package com.mzelzoghbi.zgallery;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import grand.myzone.R;

/**
 * Created by mohamedzakaria on 8/7/16.
 */
public class ImageViewHolder extends RecyclerView.ViewHolder {
    public SquareImageView image;

    public ImageViewHolder(View itemView) {
        super(itemView);
        image = (SquareImageView) itemView.findViewById(R.id.imageView);
    }
}
