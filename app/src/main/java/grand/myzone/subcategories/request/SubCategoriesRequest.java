package grand.myzone.subcategories.request;

import com.google.gson.annotations.SerializedName;

public class SubCategoriesRequest {

    @SerializedName("user_id")
    int userId;

    public SubCategoriesRequest(int userId) {
        this.userId = userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
