package grand.myzone.subcategories.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.subcategories.response.SubCategoriesItem;
import grand.myzone.subcategories.response.Tags;
import grand.myzone.subcategories.response.TagsItem;


public class TagItemViewModel extends BaseItemViewModel {

    private TagsItem tagsItem;

    public TagItemViewModel(TagsItem tagsItem) {
        this.tagsItem = tagsItem;

    }

    @Bindable
    public TagsItem getTagsItem() {
        return tagsItem;
    }


}
