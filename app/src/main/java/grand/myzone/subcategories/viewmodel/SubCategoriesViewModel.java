package grand.myzone.subcategories.viewmodel;


import android.view.View;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.home.response.CategoriesItem;
import grand.myzone.subcategories.response.SubCategoriesItem;
import grand.myzone.subcategories.response.SubCategoriesResponse;
import grand.myzone.subcategories.view.SubCategoriesAdapter;
import grand.myzone.subcategories.view.TagsAdapter;


public class SubCategoriesViewModel extends BaseViewModel {
    public SubCategoriesResponse subCategoriesResponse;
    private SubCategoriesAdapter subCategoriesAdapter;
    private List<SubCategoriesItem> subCategoriesItems;
    private TagsAdapter tagsAdapter;
    private CategoriesItem categoriesItem;

    public SubCategoriesViewModel(CategoriesItem categoriesItem) {
        this.categoriesItem=categoriesItem;
        getServices();
    }


    @BindingAdapter({"app:sub_category_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, SubCategoriesAdapter subCategoriesAdapter) {
        recyclerView.setAdapter(subCategoriesAdapter);
    }


    @BindingAdapter({"app:tags_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, TagsAdapter tagsAdapter) {
        recyclerView.setAdapter(tagsAdapter);
    }


    @Bindable
    public SubCategoriesAdapter getSubCategoriesAdapter() {
        return this.subCategoriesAdapter == null ? this.subCategoriesAdapter = new SubCategoriesAdapter() : this.subCategoriesAdapter;
    }


    @Bindable
    public TagsAdapter getTagsAdapter() {
        return this.tagsAdapter == null ? this.tagsAdapter = new TagsAdapter() : this.tagsAdapter;
    }


    public void search(String name){
        List<SubCategoriesItem> subCategoriesItems = new ArrayList<>();
            for(int i=0; i<subCategoriesResponse.getData().getSubCategoriesItems().size(); i++){
            if(subCategoriesResponse.getData().getSubCategoriesItems().get(i).getName().contains(name))
                subCategoriesItems.add(subCategoriesResponse.getData().getSubCategoriesItems().get(i));
        }

        getSubCategoriesAdapter().updateData(new ArrayList<>(subCategoriesItems));

    }
    private void getServices() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                 subCategoriesResponse = (SubCategoriesResponse)response;

                getSubCategoriesAdapter().setCategoryId(categoriesItem.getId());
                getSubCategoriesAdapter().updateData(subCategoriesResponse.getData().getSubCategoriesItems());
                getTagsAdapter().updateData(subCategoriesResponse.getData().getValues());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET,WebServices.SUB_CATEGORIES+categoriesItem.getId(), null, SubCategoriesResponse.class);

    }
}
