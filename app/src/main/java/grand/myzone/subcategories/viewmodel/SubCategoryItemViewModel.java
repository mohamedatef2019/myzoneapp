package grand.myzone.subcategories.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.subcategories.response.SubCategoriesItem;


public class SubCategoryItemViewModel extends BaseItemViewModel {

    private SubCategoriesItem subCategoriesItem;



    public SubCategoryItemViewModel(SubCategoriesItem subCategoriesItem) {
        this.subCategoriesItem = subCategoriesItem;

    }

    @Bindable
    public SubCategoriesItem getSubCategoriesItem() {
        return subCategoriesItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
