package grand.myzone.subcategories.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import grand.myzone.R;
import grand.myzone.ads.view.AdsFragment;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentSubCategoriesBinding;
import grand.myzone.home.response.CategoriesItem;
import grand.myzone.subcategories.response.SubCategoriesItem;
import grand.myzone.subcategories.response.TagsItem;
import grand.myzone.subcategories.viewmodel.SubCategoriesViewModel;

public class SubCategoriesFragment extends BaseFragment {
    private View rootView;
    private SubCategoriesViewModel subCategoriesViewModel;
    private FragmentSubCategoriesBinding fragmentSubCategoriesBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSubCategoriesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sub_categories, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSubCategoriesBinding.getRoot();
        subCategoriesViewModel = new SubCategoriesViewModel((CategoriesItem) getArguments().getSerializable(Params.BUNDLE_CATEGORY));
        fragmentSubCategoriesBinding.setSubCategoriesViewModel(subCategoriesViewModel);

    }


    private void liveDataListeners() {
        subCategoriesViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        fragmentSubCategoriesBinding.etSubCategoriesSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    subCategoriesViewModel.search(s.toString());
                }catch (Exception e){
                    e.getStackTrace();
                }

            }
        });


        subCategoriesViewModel.getTagsAdapter().getTagsItemMutableLiveData().observe(getViewLifecycleOwner(), tagsItem -> {
            Bundle bundle = new Bundle();
            SubCategoriesItem subCategoriesItem = subCategoriesViewModel.subCategoriesResponse.getData().getSubCategoriesItems().get(0);
            subCategoriesItem.setCategoryId(((CategoriesItem) getArguments().getSerializable(Params.BUNDLE_CATEGORY)).getId());
            bundle.putSerializable(Params.BUNDLE_SUB_CATEGORY,subCategoriesItem);
            bundle.putSerializable(Params.BUNDLE_TAG,tagsItem);
            MovementManager.startActivity(requireActivity(), AdsFragment.class.getName(),bundle, subCategoriesItem.getName());
        });
    }




}