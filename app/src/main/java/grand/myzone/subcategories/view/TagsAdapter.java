
package grand.myzone.subcategories.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.databinding.ItemTagBinding;
import grand.myzone.subcategories.response.TagsItem;
import grand.myzone.subcategories.viewmodel.TagItemViewModel;


public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.CategoriesViewHolder> {

    private List<TagsItem> tagsItems;
    private MutableLiveData<TagsItem>tagsItemMutableLiveData;

    public TagsAdapter() {
        tagsItemMutableLiveData = new MutableLiveData<>();
        this.tagsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        TagsItem tagsItem = tagsItems.get(position);
        TagItemViewModel tagItemViewModel = new TagItemViewModel(tagsItem);
        tagItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tagsItemMutableLiveData.setValue(tagsItem);
            }
        });
        holder.setViewModel(tagItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.tagsItems.size();
    }


    public List<TagsItem> getTagsItems() {
        return tagsItems;
    }

    public MutableLiveData<TagsItem> getTagsItemMutableLiveData() {
        return tagsItemMutableLiveData;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<TagsItem> data) {

        this.tagsItems.clear();
        assert data != null;
        this.tagsItems.addAll(data);
        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemTagBinding itemTagBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemTagBinding == null) {
                itemTagBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemTagBinding != null) {
                itemTagBinding.unbind();
            }
        }

        void setViewModel(TagItemViewModel tagItemViewModel) {
            if (itemTagBinding != null) {
                itemTagBinding.setTagItemViewModel(tagItemViewModel);
            }
        }


    }


}
