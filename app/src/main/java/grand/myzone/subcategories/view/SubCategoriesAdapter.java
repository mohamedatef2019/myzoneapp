
package grand.myzone.subcategories.view;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.ads.view.AdsFragment;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.databinding.ItemSubCategoryBinding;
import grand.myzone.subcategories.response.SubCategoriesItem;
import grand.myzone.subcategories.viewmodel.SubCategoryItemViewModel;


public class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesAdapter.CategoriesViewHolder> {

    private final List<SubCategoriesItem> subCategoriesItems;


    public SubCategoriesAdapter() {
        this.subCategoriesItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_category,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        SubCategoriesItem eventItem = subCategoriesItems.get(position);
        SubCategoryItemViewModel orderItemViewModel = new SubCategoryItemViewModel(eventItem);


        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            eventItem.setCategoryId(categoryId);
            bundle.putSerializable(Params.BUNDLE_SUB_CATEGORY,eventItem);
            MovementManager.startActivity(v.getContext(), AdsFragment.class.getName(),bundle,eventItem.getName());
        });

        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.subCategoriesItems.size();
    }


    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }


    int categoryId;

    public void setCategoryId(int categoryId){
        this.categoryId=categoryId;
    }


    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<SubCategoriesItem> data) {

            this.subCategoriesItems.clear();

            this.subCategoriesItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemSubCategoryBinding itemSubCategoryBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemSubCategoryBinding == null) {
                itemSubCategoryBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemSubCategoryBinding != null) {
                itemSubCategoryBinding.unbind();
            }
        }

        void setViewModel(SubCategoryItemViewModel notificationItemViewModel) {
            if (itemSubCategoryBinding != null) {
                itemSubCategoryBinding.setSubCategoryItemViewModel(notificationItemViewModel);
            }
        }


    }



}
