package grand.myzone.subcategories.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubCategoriesItem implements Serializable {

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("all_ads")
	private String allAds="0";

	@SerializedName("id")
	private int id;

	@Expose
	private int categoryId;

	@Expose
	@SerializedName("link")
	private String link;



	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}


	public void setAllAds(String allAds) {
		this.allAds = allAds;
	}

	public String getAllAds() {
		return allAds;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getCategoryId() {
		return categoryId;
	}


	public void setLink(String link) {
		this.link = link;
	}

	public String getLink() {
		return link;
	}

}