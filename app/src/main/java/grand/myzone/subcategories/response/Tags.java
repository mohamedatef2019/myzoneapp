package grand.myzone.subcategories.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Tags{

	@SerializedName("values")
	private List<TagsItem> values;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public void setValues(List<TagsItem> values){
		this.values = values;
	}

	public List<TagsItem> getValues(){
		return values;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}