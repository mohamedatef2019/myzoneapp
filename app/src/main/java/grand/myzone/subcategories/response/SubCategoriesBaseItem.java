package grand.myzone.subcategories.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoriesBaseItem {

	@SerializedName("tags")
	private List<TagsItem> values;


	@SerializedName("sub_categories")
	private List<SubCategoriesItem> subCategoriesItems;


	public void setSubCategoriesItems(List<SubCategoriesItem> subCategoriesItems) {
		this.subCategoriesItems = subCategoriesItems;
	}

	public void setValues(List<TagsItem> values) {
		this.values = values;
	}

	public List<SubCategoriesItem> getSubCategoriesItems() {
		return subCategoriesItems;
	}

	public List<TagsItem> getValues() {
		return values;
	}
}