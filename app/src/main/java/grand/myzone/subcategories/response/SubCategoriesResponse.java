package grand.myzone.subcategories.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoriesResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private SubCategoriesBaseItem data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(SubCategoriesBaseItem data) {
		this.data = data;
	}

	public SubCategoriesBaseItem getData() {
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}