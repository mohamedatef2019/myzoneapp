package grand.myzone.subcategories.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.myzone.events.response.ValuesItem;

public class TagsItem  implements Serializable {

	@SerializedName("name")
	private String name;


	@Expose
	@SerializedName("text")
	private String text;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("values")
	private List<ValuesItem> valuesItems;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		if(text!=null)name=text;
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public List<ValuesItem> getValuesItems() {
		return valuesItems;
	}

	public void setValuesItems(List<ValuesItem> valuesItems) {
		this.valuesItems = valuesItems;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
}