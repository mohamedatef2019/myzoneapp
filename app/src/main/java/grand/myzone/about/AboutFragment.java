package grand.myzone.about;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import grand.myzone.R;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentAboutUsBinding;
import grand.myzone.databinding.FragmentTermsAndConditionBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends BaseFragment {

    FragmentAboutUsBinding binding;
    AboutViewModel viewModel;
    private static final String TAG = "HelpFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_us, container, false);
        viewModel = new AboutViewModel();
        binding.setViewModel(viewModel);
        init();
        return binding.getRoot();
    }

    public void init() {
        viewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {



        });
    }
}
