package grand.myzone.categories.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.categories.response.CategoryItem;


public class CategoryViewModel extends BaseItemViewModel {

    private CategoryItem categoryItem;



    public CategoryViewModel(CategoryItem categoryItem) {
        this.categoryItem = categoryItem;

    }

    @Bindable
    public CategoryItem getCategoryItem() {
        return categoryItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
