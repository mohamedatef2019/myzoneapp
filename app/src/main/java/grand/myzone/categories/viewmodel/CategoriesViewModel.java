package grand.myzone.categories.viewmodel;


import android.view.View;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.categories.response.CategoryItem;
import grand.myzone.categories.response.CategoryResponse;
import grand.myzone.categories.view.CategoriesAdapter;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.subcategories.response.SubCategoriesResponse;


public class CategoriesViewModel extends BaseViewModel {

    private CategoriesAdapter categoriesAdapter;

    public CategoriesViewModel( ) {

    }


    @BindingAdapter({"app:categories_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, CategoriesAdapter categoriesAdapter) {
        recyclerView.setAdapter(categoriesAdapter);
    }



    @Bindable
    public CategoriesAdapter getCategoriesAdapter() {
        return this.categoriesAdapter == null ? this.categoriesAdapter = new CategoriesAdapter() : this.categoriesAdapter;
    }

    public void getCategories( ) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                CategoryResponse eventsResponse = (CategoryResponse) response;

                eventsResponse.getData().remove(0);
                getCategoriesAdapter().updateData(eventsResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();

            }

        }).requestJsonObject(Request.Method.GET, WebServices.GET_CATEGORIES, null, CategoryResponse.class);
    }

    public void getSubCategory(ValuesItem valuesItem) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                SubCategoriesResponse eventsResponse = (SubCategoriesResponse) response;

                List<ValuesItem> categoryItems = new ArrayList<>();
                eventsResponse.getData().getSubCategoriesItems().remove(0);
                for(int i=0; i<eventsResponse.getData().getSubCategoriesItems().size(); i++){
                    ValuesItem categoryItem = new ValuesItem();
                    categoryItem.setId(eventsResponse.getData().getSubCategoriesItems().get(i).getId());
                    categoryItem.setName(eventsResponse.getData().getSubCategoriesItems().get(i).getName());
                    categoryItems.add(categoryItem);
                }
                getCategoriesAdapter().updateData(categoryItems);
                accessLoadingBar(View.GONE);
                notifyChange();

            }

        }).requestJsonObject(Request.Method.GET, WebServices.SUB_CATEGORIES+valuesItem.getId(), null, SubCategoriesResponse.class);
    }
}
