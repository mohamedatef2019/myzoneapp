package grand.myzone.categories.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.categories.viewmodel.CategoriesViewModel;
import grand.myzone.databinding.FragmentSelectCategoryBinding;
import grand.myzone.filter.AttributesFragment;
import grand.myzone.filter.response.ValuesItem;

public class CategoriesFragment extends BaseFragment {
    private View rootView;
    private CategoriesViewModel categoriesViewModel;
    private FragmentSelectCategoryBinding fragmentSelectCategoryBinding;
    private ValuesItem valuesItem;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSelectCategoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_category, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSelectCategoryBinding.getRoot();
        categoriesViewModel = new CategoriesViewModel();

        try {
            valuesItem = (ValuesItem) getArguments().getSerializable(Params.BUNDLE_CATEGORY);
        } catch (Exception e) {
            e.getStackTrace();
        }


        if (valuesItem != null) {

            categoriesViewModel.getSubCategory(valuesItem);
        } else {
            categoriesViewModel.getCategories();
        }

        fragmentSelectCategoryBinding.setSelectCategoryViewModel(categoriesViewModel);
    }


    private void liveDataListeners() {
        categoriesViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }
        });


        categoriesViewModel.getCategoriesAdapter().getValuesItemMutableLiveData().observe(getViewLifecycleOwner(), valuesItem -> {

            if (getArguments()!=null&&getArguments().containsKey(Params.BUNDLE_FROM_EDIT)) {

                Intent intent = new Intent();
                intent.putExtra("category_name", valuesItem.getName());
                intent.putExtra("category_id", valuesItem.getId());
                requireActivity().setResult((CategoriesFragment.this.valuesItem != null)?150:180, intent);
                requireActivity().finish();

            } else {


                if (CategoriesFragment.this.valuesItem != null) {

                    Bundle bundle = new Bundle();
                    valuesItem.setCategoryId(CategoriesFragment.this.valuesItem.getId());
                    bundle.putSerializable(Params.BUNDLE_CATEGORY, valuesItem);
                    MovementManager.startActivity(requireActivity(), AttributesFragment.class.getName(), bundle, valuesItem.getName());

                } else {

                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Params.BUNDLE_CATEGORY, valuesItem);
                    MovementManager.startActivity(requireActivity(), CategoriesFragment.class.getName(), bundle, valuesItem.getName());

                }
            }
        });


    }


}