
package grand.myzone.categories.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.categories.response.CategoryItem;
import grand.myzone.categories.viewmodel.CategoryViewModel;
import grand.myzone.databinding.ItemCategoryBinding;
import grand.myzone.databinding.ItemEventBinding;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.viewmodel.FilterItemViewModel;


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    private List<ValuesItem> valuesItems;
    private MutableLiveData<ValuesItem>valuesItemMutableLiveData;

    public CategoriesAdapter() {
        valuesItemMutableLiveData = new MutableLiveData<>();
        this.valuesItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ValuesItem categoryItem = valuesItems.get(position);
        FilterItemViewModel orderItemViewModel = new FilterItemViewModel(categoryItem);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valuesItemMutableLiveData.setValue(categoryItem);
            }
        });
        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.valuesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    public MutableLiveData<ValuesItem> getValuesItemMutableLiveData() {
        return valuesItemMutableLiveData;
    }

    public void updateData(@Nullable List<ValuesItem> data) {

            this.valuesItems.clear();

            this.valuesItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemCategoryBinding itemEventBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemEventBinding == null) {
                itemEventBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemEventBinding != null) {
                itemEventBinding.unbind();
            }
        }

        void setViewModel(FilterItemViewModel filterItemViewModel) {
            if (itemEventBinding != null) {
                itemEventBinding.setFilterItemViewModel(filterItemViewModel);
            }
        }


    }



}
