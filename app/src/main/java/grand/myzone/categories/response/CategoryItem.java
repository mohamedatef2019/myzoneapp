package grand.myzone.categories.response;

import com.google.gson.annotations.SerializedName;

public class CategoryItem {

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("is_event")
	private int isEvent;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIsEvent(int isEvent){
		this.isEvent = isEvent;
	}

	public int getIsEvent(){
		return isEvent;
	}
}