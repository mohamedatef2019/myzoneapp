package grand.myzone.categories.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.myzone.filter.response.FilterDataItem;
import grand.myzone.filter.response.ValuesItem;

public class CategoryResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<ValuesItem> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<ValuesItem> data){
		this.data = data;
	}

	public List<ValuesItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}