package grand.myzone.sendmessage.viewmodel;

import android.util.Log;
import android.view.View;

import com.android.volley.Request;

import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.utils.Validate;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.login.response.UserItem;
import grand.myzone.sendmessage.request.SendMessageRequest;



public class SendMessageViewModel extends BaseViewModel {
    SendMessageRequest sendMessageRequest;


    public SendMessageViewModel() {

        sendMessageRequest = new SendMessageRequest();
    }


    public void sendMessageClick() {

        if (Validate.isEmpty(sendMessageRequest.getName())
                || Validate.isEmpty(sendMessageRequest.getEmail())
                || Validate.isEmpty(sendMessageRequest.getMessage())) {
            getClicksMutableLiveData().setValue(Codes.EMPTY_DATA);
        } else if (!Validate.isMail(sendMessageRequest.getEmail())) {
            getClicksMutableLiveData().setValue(Codes.WRONG_EMAIL);
        } else {
            sendMessage();
        }
    }

    public void sendMessageProblemClick() {

        try{
            UserItem userItem = UserPreferenceHelper.getUserDetails();

            sendMessageRequest.setName(userItem.getUsersName());

            if(userItem.getEmail()==null||userItem.getEmail().equals("")){
                userItem.setEmail("visitor@myzonne.com");
            }
            sendMessageRequest.setEmail(userItem.getEmail());

            sendMessageRequest.setPhone(userItem.getPhone());

        }catch (Exception e){
            e.getStackTrace();
        }

        if (Validate.isEmpty(sendMessageRequest.getMessage())) {

            getClicksMutableLiveData().setValue(Codes.EMPTY_DATA);
        } else {
            sendMessageProblem();
        }
    }


    public SendMessageRequest getSendMessageRequest() {
        return sendMessageRequest;
    }

    public void setSendMessageRequest(SendMessageRequest sendMessageRequest) {
        this.sendMessageRequest = sendMessageRequest;
    }


    private void sendMessageProblem() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                DefaultResponse sendCodeResponse = (DefaultResponse) response;
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                sendMessageRequest.setEmail(null);
                sendMessageRequest.setMessage(null);
                sendMessageRequest.setName(null);



                notifyChange();
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.SUGGESTIONS, getSendMessageRequest(), DefaultResponse.class);
    }

    private void sendMessage() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                DefaultResponse sendCodeResponse = (DefaultResponse) response;
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                sendMessageRequest.setEmail(null);
                sendMessageRequest.setMessage(null);
                sendMessageRequest.setName(null);



                notifyChange();
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CONTACT_US, getSendMessageRequest(), DefaultResponse.class);
    }



}
