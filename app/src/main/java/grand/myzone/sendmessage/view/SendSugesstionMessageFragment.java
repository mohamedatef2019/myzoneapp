package grand.myzone.sendmessage.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.myzone.R;
import grand.myzone.CustomDialogs;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentSendProblemBinding;
import grand.myzone.sendmessage.request.SendMessageRequest;
import grand.myzone.sendmessage.viewmodel.SendMessageViewModel;


public class SendSugesstionMessageFragment extends BaseFragment {

    private View rootView;
    private SendMessageViewModel sendMessageViewModel;
    private FragmentSendProblemBinding fragmentSuggestSendMessageBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSuggestSendMessageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_problem, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSuggestSendMessageBinding.getRoot();
        sendMessageViewModel = new SendMessageViewModel();
        fragmentSuggestSendMessageBinding.setSendMessageViewModel(sendMessageViewModel);
    }


    private void liveDataListeners() {
        sendMessageViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.EMPTY_DATA) {

                CustomDialogs.wariningDialog(getActivity(), ResourcesManager.getString(R.string.msg_empty_data));

            }else if(result == Codes.WRONG_EMAIL){

                CustomDialogs.wariningDialog(getActivity(), ResourcesManager.getString(R.string.msg_wrong_email));

            }else if(result == Codes.DATA_RECIVED){
                sendMessageViewModel.setSendMessageRequest(new SendMessageRequest());
                sendMessageViewModel.notifyChange();
                CustomDialogs.successDialog(getActivity(), ResourcesManager.getString(R.string.msg_sent_successfully));

            }

        });
    }


}