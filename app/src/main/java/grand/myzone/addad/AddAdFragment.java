package grand.myzone.addad;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;


import java.io.File;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.myzone.R;
import grand.myzone.CustomDialogs;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.addad.viewmodel.AddAdViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.filesutils.FileOperations;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentAddAdBinding;
import grand.myzone.erikagtierrez.multiple_media_picker.Gallery;
import grand.myzone.images.FilesImagesSelectInterface;
import grand.myzone.images.ImagesAdapter;
import grand.myzone.images.ImagesItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddAdFragment extends BaseFragment {

    FragmentAddAdBinding binding;
    AddAdViewModel addAdViewModel;
    static final int OPEN_MEDIA_PICKER = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_ad, container, false);


        addAdViewModel = new AddAdViewModel((AddAdRequest) getArguments().getSerializable(Params.BUNDLE_ITEM_AD));


        binding.setAddAdViewModel(addAdViewModel);

       // requireActivity().requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},1231);
        init();
        return binding.getRoot();
    }

    public void init() {
        addAdViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if(result== Constant.SELECT_IMAGE){
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                boolean permissionGranted = true;

                for(String permission : permissions) {
                    if (ContextCompat.checkSelfPermission(requireActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                        permissionGranted = false;
                    }
                }


                if (permissionGranted) {
                    Intent intent = new Intent(requireActivity(), Gallery.class);
                    intent.putExtra("title", "Select media");
                    intent.putExtra("mode", 2);
                    intent.putExtra("maxSelection", 12);
                    startActivityForResult(intent, OPEN_MEDIA_PICKER);
                } else {
                    requireActivity().requestPermissions(permissions, 1231);
                }

            }else if(result == Codes.NEXT){
                Bundle bundle = new Bundle();
                addAdViewModel.getAddAdRequest().setSelectionResult(selectionResult);
                bundle.putSerializable(Params.BUNDLE_ITEM_AD,addAdViewModel.getAddAdRequest());
                MovementManager.startActivity(requireActivity(),FragmentCompleteAdFragment.class.getName(),bundle, ResourcesManager.getString(R.string.label_add_ad));
            }else if(result == Codes.ADDING_ERROR){

                CustomDialogs.wariningDialog(requireActivity(),addAdViewModel.getReturnedMessage());
            }
        });
    }




    ArrayList<String> selectionResult;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // Check which request we're responding to
            if (requestCode == OPEN_MEDIA_PICKER) {
                // Make sure the request was successful

                if (resultCode == Activity.RESULT_OK && data != null) {


                    if (addAdViewModel.getAddAdRequest().getSelectionResult() == null) {
                        addAdViewModel.getAddAdRequest().setSelectionResult(new ArrayList<>());
                    }

                    selectionResult = addAdViewModel.getAddAdRequest().getSelectionResult();

                    selectionResult.addAll(data.getStringArrayListExtra("result"));


                    if ( selectionResult.size() <= 12) {



                        addAdViewModel.getAddAdRequest().setSelectionResult(selectionResult);

                        binding.tvAddAdSelected.setText(ResourcesManager.getString(R.string.label_image_selected) + " ( " + selectionResult.size() + " ) " + ResourcesManager.getString(R.string.label_images));

                        ArrayList<ImagesItem> imagesItems = new ArrayList<>();

                        for (int i = 0; i < selectionResult.size(); i++) {
                            imagesItems.add(new ImagesItem(i + 1, FileOperations.getVolleyFileObject(requireActivity(),selectionResult.get(i),"param "+i,Codes.FILE_TYPE_IMAGE).getCompressObject().getImage()));
                        }

                        ImagesAdapter imagesAdapter = new ImagesAdapter(imagesItems, position -> {
                            addAdViewModel.getAddAdRequest().getSelectionResult().remove(position);
                            binding.tvAddAdSelected.setText(ResourcesManager.getString(R.string.label_image_selected) + " ( " + selectionResult.size() + " ) " + ResourcesManager.getString(R.string.label_images));
                        });
                        binding.rvAddAdImages.setAdapter(imagesAdapter);
                    }else {
                        CustomDialogs.wariningDialog(requireActivity(),ResourcesManager.getString(R.string.label_image_limit));
                    }
                }
            }
        }catch (Exception e){
            e.getStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}
