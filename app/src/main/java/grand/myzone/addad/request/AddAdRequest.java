package grand.myzone.addad.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import grand.myzone.addetails.response.ImagesItem;
import grand.myzone.filter.response.AttributesItem;

public class AddAdRequest implements Serializable {


    @Expose
    @SerializedName("ad_id")
    private int adId=0;



    @SerializedName("category_id")
    private String categoryId;

    @SerializedName("sub_category_id")
    private String subCategoryId;

    @SerializedName("title")
    private String title;

    @SerializedName("price")
    private String price;

    @SerializedName("negotiable")
    private String negotiable = "0";

    @SerializedName("government_id")
    private String governmentId;

    @SerializedName("city_id")
    private String cityId;

    @SerializedName("area_id")
    private String areaId;


    @Expose
    private String government;

    @Expose
    private String city;

    @Expose
    private String area;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    @SerializedName("description")
    private String description;


    @SerializedName("attributes")
    private List<AttributesItem> attributesItems;


    @Expose
    private List<ImagesItem>imagesItems;


    @Expose
    private String categoryName;

    @Expose
    private String subCategoryName;


    ArrayList<String> selectionResult;



    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(String negotiable) {
        this.negotiable = negotiable;
    }

    public String getGovernmentId() {
        return governmentId;
    }

    public void setGovernmentId(String governmentId) {
        this.governmentId = governmentId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSelectionResult(ArrayList<String> selectionResult) {
        this.selectionResult = selectionResult;
    }

    public ArrayList<String> getSelectionResult() {
        return selectionResult;
    }

    public void setAttributesItems(List<AttributesItem> attributesItems) {
        this.attributesItems = attributesItems;
    }

    public List<AttributesItem> getAttributesItems() {
        return attributesItems;
    }


    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getGovernment() {
        return government;
    }

    public void setImagesItems(List<ImagesItem> imagesItems) {
        this.imagesItems = imagesItems;
    }

    public List<ImagesItem> getImagesItems() {
        return imagesItems;
    }

    public int getAdId() {
        return adId;
    }

    public void setAdId(int adId) {
        this.adId = adId;
    }
}
