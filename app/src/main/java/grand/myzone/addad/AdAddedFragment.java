package grand.myzone.addad;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import grand.myzone.R;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.categories.view.CategoriesFragment;
import grand.myzone.databinding.FragmentAdAddedBinding;
import grand.myzone.databinding.FragmentHelpBinding;
import grand.myzone.help.HelpViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdAddedFragment extends BaseFragment {

    FragmentAdAddedBinding binding;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ad_added, container, false);


        init();
        return binding.getRoot();
    }

    public void init() {

        binding.btnBrowser.setOnClickListener(v -> requireActivity().finish());

        binding.fabAddAddedNew.setOnClickListener(v -> {
                requireActivity().finish();
            MovementManager.startActivity(requireActivity(), CategoriesFragment.class.getName(), ResourcesManager.getString(R.string.label_add_ad));
        });



    }
}
