package grand.myzone.addad.viewmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PlacesResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<PlaceItem> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<PlaceItem> data){
		this.data = data;
	}

	public List<PlaceItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}