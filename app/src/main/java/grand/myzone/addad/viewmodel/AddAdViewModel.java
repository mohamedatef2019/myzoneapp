package grand.myzone.addad.viewmodel;


import android.util.Log;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import grand.myzone.R;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.addetails.response.AdDetailsResponse;
import grand.myzone.addetails.view.EditImagesAdapter;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.filesutils.FileOperations;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.chatlist.view.ChatListAdapter;
import grand.myzone.filter.response.AttributesItem;
import grand.myzone.filter.response.attributes.AttributiesResponse;
import grand.myzone.filter.view.AttrbutiesMainsAdapter;
import grand.myzone.filter.view.AttributesAdapter;

public class AddAdViewModel extends BaseViewModel {

    public AddAdRequest addAdRequest;
    public List<PlaceItem> governments, cities, areas;
    public ArrayList<VolleyFileObject> volleyFileObjects;
    private EditImagesAdapter editImagesAdapter;
    public String phone;
    private AttrbutiesMainsAdapter attrbutiesMainsAdapter;

    public AddAdViewModel(AddAdRequest addAdRequest) {
        phone= UserPreferenceHelper.getUserDetails().getPhone();
        volleyFileObjects = new ArrayList<>();
        this.addAdRequest = addAdRequest;

        try {
            getEditImagesAdapter().updateData(addAdRequest.getImagesItems());
        }catch (Exception e){
            e.getStackTrace();
        }
        notifyChange();

    }


    public void getCities(String id) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                PlacesResponse placesResponse = (PlacesResponse) response;
                cities = placesResponse.getData();
                getClicksMutableLiveData().setValue(Codes.CITIES);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.CITIES + id, new Object(), PlacesResponse.class);

    }

    public void getGovernments() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                PlacesResponse placesResponse = (PlacesResponse) response;
                governments = placesResponse.getData();
                getClicksMutableLiveData().setValue(Codes.GOVERNMENTS);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GOVERNMENTS, new Object(), PlacesResponse.class);

    }

    public void getAreas(String id) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                PlacesResponse placesResponse = (PlacesResponse) response;
                areas = placesResponse.getData();
                getClicksMutableLiveData().setValue(Codes.AREAS);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.AREAS + id, new Object(), PlacesResponse.class);
    }


    public void next() {
        getClicksMutableLiveData().setValue(Constant.SELECT_IMAGE);
    }



    public void selectLocationClick() {
        getClicksMutableLiveData().setValue(Codes.SELECT_LOCATION);
    }

    public void showAreasClick() {
        if (getAddAdRequest().getCityId() != null) {
            getAreas(getAddAdRequest().getCityId());
        }
    }

    public void showGovernmentsClick() {
        getGovernments();
    }

    public void isNegotiable() {
        getAddAdRequest().setNegotiable(getAddAdRequest().getNegotiable().equals("0") ? "1" : "0");
        notifyChange();
    }

    public void showCitiesClick() {
        if (getAddAdRequest().getGovernmentId() != null) {
            getCities(getAddAdRequest().getGovernmentId());
        }
    }

    public void completeAdDetails() {

        if (getAddAdRequest().getTitle() == null || getAddAdRequest().getTitle().equals("")) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_title));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else if (getAddAdRequest().getDescription() == null || getAddAdRequest().getDescription().equals("")) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_desc));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else if (getAddAdRequest().getSelectionResult() == null || getAddAdRequest().getSelectionResult().size() < 1) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_image));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else {
            getClicksMutableLiveData().setValue(Codes.NEXT);
        }


    }


    public void onCategoryClick() {
            getClicksMutableLiveData().setValue(Codes.SELECT_CATEGORY_CLICK);
    }

    public void onSubCategoryClick() {
        getClicksMutableLiveData().setValue(Codes.SELECT_SUB_CATEGORY_CLICK);
    }

    public void nextEditClick() {
         if (addAdRequest.getImagesItems().size()<1&& (getAddAdRequest().getSelectionResult() == null || getAddAdRequest().getSelectionResult().size() < 1)) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_image));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else {
            getClicksMutableLiveData().setValue(Codes.NEXT);
        }
    }

    public void confirmAdding() {

        if (getAddAdRequest().getPrice() == null || getAddAdRequest().getPrice().equals("")) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_price));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else if (getAddAdRequest().getGovernmentId() == null || getAddAdRequest().getGovernmentId().equals("")) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_governorate));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else if (getAddAdRequest().getCityId() == null || getAddAdRequest().getCityId().equals("")) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_city));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else if (getAddAdRequest().getAreaId() == null || getAddAdRequest().getAreaId().equals("")) {
            setReturnedMessage(ResourcesManager.getString(R.string.msg_add_area));
            getClicksMutableLiveData().setValue(Codes.ADDING_ERROR);
        } else {

            HashMap<String, String> params = new HashMap<>();
            params.put("category_id", getAddAdRequest().getCategoryId());
            params.put("sub_category_id", getAddAdRequest().getSubCategoryId());
            params.put("title", getAddAdRequest().getTitle());
            params.put("price", getAddAdRequest().getPrice());
            params.put("negotiable", getAddAdRequest().getNegotiable());
            params.put("government_id", getAddAdRequest().getGovernmentId());
            params.put("city_id", getAddAdRequest().getCityId());
            params.put("area_id", getAddAdRequest().getAreaId());
            params.put("latitude", getAddAdRequest().getLatitude() + "");
            params.put("longitude", getAddAdRequest().getLongitude() + "");
            params.put("description", getAddAdRequest().getDescription());

            for (int i = 0; i < getAddAdRequest().getAttributesItems().size(); i++) {

                for (int j = 0; j < getAddAdRequest().getAttributesItems().get(i).getValues().size(); j++) {
                    if (getAddAdRequest().getAttributesItems().get(i).getValues().get(j).isSelected()) {
                        params.put("attributes[" + i + "][key_id]", getAddAdRequest().getAttributesItems().get(i).getId() + "");
                        params.put("attributes[" + i + "][used_tag]", getAddAdRequest().getAttributesItems().get(i).getValues().get(j).getUsedTag() + "");
                        params.put("attributes[" + i + "][key_text]", getAddAdRequest().getAttributesItems().get(i).getValueText() + "");
                        params.put("attributes[" + i + "][values][" + j + "][value_id]", getAddAdRequest().getAttributesItems().get(i).getValues().get(j).getId() + "");
                        params.put("attributes[" + i + "][values][" + j + "][used_tag]", getAddAdRequest().getAttributesItems().get(i).getValues().get(j).getUsedTag() + "");
                    }
                }
            }


            Log.e("volleyFileObjects", volleyFileObjects.size() + " " + params);
            accessLoadingBar(View.VISIBLE);
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    accessLoadingBar(View.GONE);

                    getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                    notifyChange();
                }
            }).multiPartConnect(addAdRequest.getAdId()==0?WebServices.ADD_ADVERTISE:(WebServices.UPDATE_ADVERTISE+addAdRequest.getAdId()), params, volleyFileObjects, DefaultResponse.class, true);
        }
    }




    public void getAttributes() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AttributiesResponse filterDataResponse = (AttributiesResponse) response;

                getAttrubtiesMainsAdapter().updateData(filterDataResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.ATTRIBUTES+addAdRequest.getSubCategoryId()+"&category_id="+addAdRequest.getCategoryId(), null, AttributiesResponse.class);
    }



    public AddAdRequest getAddAdRequest() {
        return addAdRequest;
    }


    @BindingAdapter({"app:edit_image_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, EditImagesAdapter subCategoriesAdapter) {
        recyclerView.setAdapter(subCategoriesAdapter);
    }


    @BindingAdapter({"app:attributes_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, AttrbutiesMainsAdapter attrbutiesMainsAdapter) {
        recyclerView.setAdapter(attrbutiesMainsAdapter);
    }




    public AttrbutiesMainsAdapter getAttrubtiesMainsAdapter() {
        return attrbutiesMainsAdapter = attrbutiesMainsAdapter == null ? new AttrbutiesMainsAdapter() : attrbutiesMainsAdapter;
    }




    @Bindable
    public EditImagesAdapter getEditImagesAdapter() {
        return this.editImagesAdapter == null ? this.editImagesAdapter = new EditImagesAdapter() : this.editImagesAdapter;
    }
}
