package grand.myzone.addetails.request;

import com.google.gson.annotations.SerializedName;

public class DeleteAdRequest {


    @SerializedName("advertisement_id")
    private int adsId;




    public void setAdsId(int adsId) {
        this.adsId = adsId;
    }

    public int getAdsId() {
        return adsId;
    }


}
