
package grand.myzone.addetails.view;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import grand.myzone.R;
import grand.myzone.addetails.response.ImagesItem;
import grand.myzone.addetails.viewmodel.EditImageViewModel;
import grand.myzone.ads.request.ReportItem;
import grand.myzone.ads.viewmodel.ReportItemViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.databinding.ItemAdImageBinding;
import grand.myzone.databinding.ItemReportBinding;


public class EditImagesAdapter extends RecyclerView.Adapter<EditImagesAdapter.CategoriesViewHolder> {

    private List<ImagesItem> reportItems;
    private MutableLiveData<Integer> mutableLiveData;


    public EditImagesAdapter() {
        mutableLiveData = new MutableLiveData<>();
        this.reportItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad_image,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ImagesItem imagesItem = reportItems.get(position);
        EditImageViewModel editImageViewModel = new EditImageViewModel(imagesItem);


        editImageViewModel.getItemsOperationsLiveListener().observeForever(integer -> {

            deleteImageDialog(holder.itemView.getContext(),position);

        });


        holder.setViewModel(editImageViewModel);

    }


    private void deleteImageDialog(Context context,int position){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_delete_ad, null);
        Button addAd = dialogView.findViewById(R.id.btn_add_ad);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);

        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();

        addAd.setOnClickListener(v -> {

            deleteAd(reportItems.get(position).getId());
            alertDialog.cancel();
            alertDialog.dismiss();

            reportItems.remove(position);
            notifyDataSetChanged();
        });

        cancel.setOnClickListener(v -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });

        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
    }


    public void deleteAd(int id){
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

            }
        }).requestJsonObject(Request.Method.DELETE, WebServices.DELETE_IMAGE+id, null, DefaultResponse.class);
    }

    public MutableLiveData<Integer> getMutableLiveData() {
        return mutableLiveData;
    }

    @Override
    public int getItemCount() {
        return this.reportItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ImagesItem> data) {

        if(data==null){
            data = new ArrayList<>();
        }

        this.reportItems.clear();


        this.reportItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemAdImageBinding itemAdImageBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemAdImageBinding == null) {
                itemAdImageBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemAdImageBinding != null) {
                itemAdImageBinding.unbind();
            }
        }

        void setViewModel(EditImageViewModel adItemViewModel) {
            if (itemAdImageBinding != null) {
                itemAdImageBinding.setEditImageItemViewModel(adItemViewModel);
            }
        }


    }


}
