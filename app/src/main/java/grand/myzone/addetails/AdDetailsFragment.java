package grand.myzone.addetails;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import grand.myzone.CustomDialogs;
import grand.myzone.R;
import grand.myzone.SelectLocationActivity;
import grand.myzone.SliderImagesAdapter;
import grand.myzone.addetails.viewmodel.AdDetailsViewModel;
import grand.myzone.ads.request.ReportItem;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.ads.view.ReportsAdapter;
import grand.myzone.ads.view.SearchFragment;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.customviews.CustomButton;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.chatmessages.view.ChatMessagesFragment;
import grand.myzone.comments.view.CommentsFragment;
import grand.myzone.databinding.FragmentAdDetailsBinding;
import grand.myzone.userdetails.views.UserDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdDetailsFragment extends BaseFragment {

    FragmentAdDetailsBinding binding;
    AdDetailsViewModel adDetailsViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ad_details, container, false);
        adDetailsViewModel = new AdDetailsViewModel((AdsItem) getArguments().getSerializable(Params.BUNDLE_ITEM_AD));
        binding.setAdDetailsViewModel(adDetailsViewModel);
        init();

        FragmentManager fragmentManager = getChildFragmentManager();

        AdsItem adsItem = (AdsItem) getArguments().getSerializable(Params.BUNDLE_ITEM_AD);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Params.BUNDLE_ITEM_AD, adsItem);
        bundle.putBoolean(Params.BUNDLE_FULL_PAGE, true);
        CommentsFragment commentsFragment = new CommentsFragment();
        commentsFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().add(R.id.ll_ad_details, commentsFragment);
        fragmentTransaction.commit();


        return binding.getRoot();
    }

    public void init() {

        adDetailsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.ADD_COMMENT_SCREEN) {
                AdsItem adsItem = (AdsItem) getArguments().getSerializable(Params.BUNDLE_ITEM_AD);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ITEM_AD, adsItem);
                MovementManager.startActivity(requireActivity(), CommentsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_comments));
            } else if (result == Codes.USER_DETAILS_DATA) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ANY_ID, adDetailsViewModel.adDetailsItem.getUser().getId());
                MovementManager.startActivity(requireActivity(), UserDetailsFragment.class.getName(), bundle,  ResourcesManager.getString(R.string.user_details));
            } else if (result == Codes.CALL_CLICK) {
                try {
                    requireActivity().requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 234);
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + adDetailsViewModel.getAdDetailsItem().getUser().getPhone()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.getStackTrace();
                }

            } else if (result == Codes.CHAT_CLICK) {
                Bundle bundle = new Bundle();
                bundle.putInt(Params.USER_ID, adDetailsViewModel.adDetailsItem.getUser().getId());
                MovementManager.startActivity(requireActivity(), ChatMessagesFragment.class.getName(), bundle, ResourcesManager.getString(R.string.chat));
            } else if (result == Codes.SHARE_CLICK) {
                MovementManager.shareContent(requireActivity(), adDetailsViewModel.adDetailsItem.getTitle() + "  \n " + adDetailsViewModel.adDetailsItem.getDescription());
            } else if (result == Codes.MAP_CLICK) {

                  if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(requireActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},1234);
                    return;
                }else {
                      try {
                          Intent intent = new Intent(requireActivity(), SelectLocationActivity.class);
                          intent.putExtra(Params.INTENT_LAT, Double.parseDouble(adDetailsViewModel.adDetailsItem.getLatitude()));
                          intent.putExtra(Params.INTENT_LANG, Double.parseDouble(adDetailsViewModel.adDetailsItem.getLatitude()));
                          startActivity(intent);
                      } catch (Exception e) {
                          e.getStackTrace();
                      }
                  }
            } else if (result == Codes.DATA_RECIVED) {

                SliderImagesAdapter adapter = new SliderImagesAdapter(requireActivity(), adDetailsViewModel.getAdDetailsItem().getImages());
                SliderView sliderView = binding.imageSlider;
                sliderView.setSliderAdapter(adapter);
                sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                sliderView.setIndicatorSelectedColor(ResourcesManager.getColor(R.color.colorAccent));
                sliderView.setIndicatorUnselectedColor(Color.GRAY);
                sliderView.setScrollTimeInSec(4);
                sliderView.startAutoCycle();

            }else if(result == Codes.REPORT_CLICK){
                showBottomSheetDialog();
            }else if(result == Codes.REPORTED){
                adReportedDialog();
            }else if(result == Codes.LOGIN_FIRST){
                CustomDialogs.loginToContinue(requireActivity());
            }


        });

        //showBottomSheetDialog();

    }


    private void adReportedDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_report_sent, null);
        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    private void showBottomSheetDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity(), R.style.SheetDialog);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_dialog);


        CustomButton btnReport = bottomSheetDialog.findViewById(R.id.btn_report);

        RecyclerView recyclerView = bottomSheetDialog.findViewById(R.id.recycler_view);
        ReportsAdapter reportsAdapter = new ReportsAdapter();
        List<ReportItem> reportItems = new ArrayList<>();


        for(int i=0; i<adDetailsViewModel.reportItems.size(); i++){
            ReportItem reportItem = new ReportItem();
            reportItem.setId(adDetailsViewModel.reportItems.get(i).getId());
            reportItem.setName(adDetailsViewModel.reportItems.get(i).getReason());
            reportItems.add(reportItem);
        }


        btnReport.setOnClickListener(v -> {
            bottomSheetDialog.cancel();
            bottomSheetDialog.dismiss();
                adDetailsViewModel.setReport(adDetailsViewModel.reportItems.get(reportsAdapter.getSelected()).getId());
        });

        reportsAdapter.updateData(reportItems);
        recyclerView.setAdapter(reportsAdapter);

        bottomSheetDialog.show();
    }
}
