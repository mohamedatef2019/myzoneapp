package grand.myzone.addetails.viewmodel;


import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.List;

import grand.myzone.addetails.response.AdDetailsItem;
import grand.myzone.addetails.response.AdDetailsResponse;
import grand.myzone.addetails.view.EditImagesAdapter;
import grand.myzone.ads.request.FollowRequest;
import grand.myzone.ads.request.ReportRequest;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.ads.response.reasons.ReportItem;
import grand.myzone.ads.response.reasons.ReportsResponse;
import grand.myzone.ads.view.AdsAdapter;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.comments.request.AddCommentRequest;
import grand.myzone.filter.view.AttrbutiesMainsAdapter;
import grand.myzone.filter.view.ValuesAdapter;

public class AdDetailsViewModel extends BaseViewModel {

    public AdDetailsItem adDetailsItem;
    private AdsItem adsItem;
    private ValuesAdapter valuesAdapter;


    public AdDetailsViewModel(AdsItem adsItem) {
        this.adsItem=adsItem;
        getTerms();
        getReports();
    }



    public void userDetailsClick(){
        getClicksMutableLiveData().setValue(Codes.USER_DETAILS_DATA);
    }

    public void onCallClick(){
        getClicksMutableLiveData().setValue(Codes.CALL_CLICK);
    }

    public void onSoldClick(){
        removeAd();
    }


    public void onEditClick(){
        getClicksMutableLiveData().setValue(Codes.AD_EDIT_CLICK);
    }

    public void onChatClick(){
        if(UserPreferenceHelper.isLogined()) {
            getClicksMutableLiveData().setValue(Codes.CHAT_CLICK);
        }else {
            getClicksMutableLiveData().setValue(Codes.LOGIN_FIRST);
        }

    }


    public void onRemoveClick(){
        getClicksMutableLiveData().setValue(Codes.REMOVE_CLICK);
    }



    public void onMapClick(){
        getClicksMutableLiveData().setValue(Codes.MAP_CLICK);
    }


    public void removeAd() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);

                getClicksMutableLiveData().setValue(Codes.REMOVED);

                notifyChange();
            }
        }).requestJsonObject(Request.Method.DELETE,
                WebServices.DELETE_AD+adsItem.getId(),
                new Object(), DefaultResponse.class);

    }



    private void getTerms() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                adDetailsItem = ((AdDetailsResponse) response).getData();
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                try {
                    getValuesAdapter().updateData(adDetailsItem.getValuesItems());
                }catch (Exception E){
                    E.getStackTrace();
                }
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.AD_DETAILS+adsItem.getId(), new Object(), AdDetailsResponse.class);

    }

   public List<ReportItem> reportItems;

    private void getReports() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                  reportItems  = ((ReportsResponse) response).getData();

                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_REPORT, new Object(), ReportsResponse.class);

    }


    @BindingAdapter({"app:values_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, ValuesAdapter attrbutiesMainsAdapter) {
        recyclerView.setAdapter(attrbutiesMainsAdapter);
    }

    public ValuesAdapter getValuesAdapter() {
        return valuesAdapter = valuesAdapter == null ? new ValuesAdapter() : valuesAdapter;
    }




    public List<ReportItem> getReportItems() {
        return reportItems;
    }

    public void onShareClick(){
        getClicksMutableLiveData().setValue(Codes.SHARE_CLICK);
    }

    public void onFavouriteClick(){

        if(UserPreferenceHelper.isLogined()) {
        accessLoadingBar(View.VISIBLE);
        adDetailsItem.setIsFavorite(!adDetailsItem.isIsFavorite());
        notifyChange();
        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setAdsId(adDetailsItem.getId());


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.ADD_FAVOURITE, addCommentRequest, DefaultResponse.class);
    }else {
        getClicksMutableLiveData().setValue(Codes.LOGIN_FIRST);
    }


    }

    public void setReport(int reportId){
        accessLoadingBar(View.VISIBLE);
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setAdsId(adDetailsItem.getUser().getId());
        reportRequest.setReasonId(reportId);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.REPORTED);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.REPORT_AD, reportRequest, DefaultResponse.class);
    }


    public void followUser(){

        notifyChange();
        FollowRequest followRequest = new FollowRequest();
        followRequest.setUserId(adDetailsItem.getUser().getId());

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

            }
        }).requestJsonObject(Request.Method.POST, WebServices.FOLLOW, followRequest, DefaultResponse.class);
    }

    public void onFollowClick(){
        if(UserPreferenceHelper.isLogined()) {
            followUser();
            adDetailsItem.getUser().setFollow(!adDetailsItem.getUser().isFollow());
        }else {
            getClicksMutableLiveData().setValue(Codes.LOGIN_FIRST);
        }
    }

    public AdDetailsItem getAdDetailsItem() {
        return adDetailsItem;
    }


    public void onCommentsClick(){
        getClicksMutableLiveData().setValue(Codes.ADD_COMMENT_SCREEN);

    }


    public void onReportClick(){

        if(UserPreferenceHelper.isLogined()) {
            getClicksMutableLiveData().setValue(Codes.REPORT_CLICK);
        }else {
            getClicksMutableLiveData().setValue(Codes.LOGIN_FIRST);
        }



    }


}
