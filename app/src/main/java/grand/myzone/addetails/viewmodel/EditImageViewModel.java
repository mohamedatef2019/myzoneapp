package grand.myzone.addetails.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.android.volley.Request;
import com.joooonho.SelectableRoundedImageView;
import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.addetails.response.ImagesItem;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.comments.request.AddCommentRequest;


public class EditImageViewModel extends BaseItemViewModel {

    private ImagesItem imagesItem;

    public EditImageViewModel(ImagesItem imagesItem) {
        this.imagesItem = imagesItem;

    }

    @Bindable
    public ImagesItem getImagesItem() {
        return imagesItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }

    public void onDeleteClick(){
        getItemsOperationsLiveListener().setValue(Codes.DELETE_IMAGE);
    }




}
