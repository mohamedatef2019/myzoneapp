package grand.myzone.addetails;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.myzone.R;
import grand.myzone.SelectLocationActivity;
import grand.myzone.SliderImagesAdapter;
import grand.myzone.CustomDialogs;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.addetails.viewmodel.AdDetailsViewModel;
import grand.myzone.ads.request.ReportItem;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.ads.view.ReportsAdapter;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.customviews.CustomButton;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.chatmessages.view.ChatMessagesFragment;
import grand.myzone.comments.view.CommentsFragment;
import grand.myzone.databinding.FragmentMyAdDetailsBinding;
import grand.myzone.home.HomeFragment;
import grand.myzone.updatead.UpdateAdFragment;
import grand.myzone.userdetails.views.UserDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyAdDetailsFragment extends BaseFragment {

    FragmentMyAdDetailsBinding binding;
    AdDetailsViewModel adDetailsViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_ad_details, container, false);
        adDetailsViewModel = new AdDetailsViewModel((AdsItem) getArguments().getSerializable(Params.BUNDLE_ITEM_AD));
        binding.setAdDetailsViewModel(adDetailsViewModel);
        init();
        return binding.getRoot();
    }

    public void init() {

        adDetailsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.ADD_COMMENT_SCREEN) {
                AdsItem adsItem = (AdsItem) getArguments().getSerializable(Params.BUNDLE_ITEM_AD);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ITEM_AD, adsItem);
                MovementManager.startActivity(requireActivity(), CommentsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_comments));
            } else if (result == Codes.USER_DETAILS_DATA) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ANY_ID, adDetailsViewModel.adDetailsItem.getUser().getId());
                MovementManager.startActivity(requireActivity(), UserDetailsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.user_details));
            } else if (result == Codes.CALL_CLICK) {
                try {
                    requireActivity().requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 234);
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + adDetailsViewModel.getAdDetailsItem().getUser().getPhone()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.getStackTrace();
                }

            } else if (result == Codes.CHAT_CLICK) {
                Bundle bundle = new Bundle();
                bundle.putInt(Params.USER_ID, adDetailsViewModel.adDetailsItem.getUser().getId());
                MovementManager.startActivity(requireActivity(), ChatMessagesFragment.class.getName(), bundle, ResourcesManager.getString(R.string.chat));
            } else if (result == Codes.SHARE_CLICK) {
                MovementManager.shareContent(requireActivity(), adDetailsViewModel.adDetailsItem.getTitle() + "  \n " + adDetailsViewModel.adDetailsItem.getDescription());
            } else if (result == Codes.MAP_CLICK) {
                Intent intent = new Intent(requireActivity(), SelectLocationActivity.class);
                intent.putExtra(Params.INTENT_LAT, Double.parseDouble(adDetailsViewModel.adDetailsItem.getLatitude()));
                intent.putExtra(Params.INTENT_LANG, Double.parseDouble(adDetailsViewModel.adDetailsItem.getLatitude()));
                startActivity(intent);
            } else if (result == Codes.DATA_RECIVED) {

                SliderImagesAdapter adapter = new SliderImagesAdapter(requireActivity(), adDetailsViewModel.getAdDetailsItem().getImages());
                SliderView sliderView = binding.imageSlider;
                sliderView.setSliderAdapter(adapter);
                sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                sliderView.setIndicatorSelectedColor(ResourcesManager.getColor(R.color.colorAccent));
                sliderView.setIndicatorUnselectedColor(Color.GRAY);
                sliderView.setScrollTimeInSec(4);
                sliderView.startAutoCycle();

            }else if(result == Codes.REPORT_CLICK){
                showBottomSheetDialog();
            }else if(result == Codes.REMOVE_CLICK){
                deleteAdDialog();
            }else if(result == Codes.REMOVED){
                CustomDialogs.successDialog(requireActivity(), ResourcesManager.getString(R.string.msg_deleted), new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        requireActivity().finish();
                        MovementManager.startMainActivity(requireActivity(), HomeFragment.class.getName());

                    }
                });
            }else if(result == Codes.AD_EDIT_CLICK){
                Bundle bundle = new Bundle();

                AddAdRequest addAdRequest = new AddAdRequest();

                addAdRequest.setAdId(adDetailsViewModel.getAdDetailsItem().getId());
                addAdRequest.setCategoryId(adDetailsViewModel.getAdDetailsItem().getCategoryId()+"");
                addAdRequest.setSubCategoryId(adDetailsViewModel.getAdDetailsItem().getSubCategoryId()+"");
                addAdRequest.setCategoryName(adDetailsViewModel.getAdDetailsItem().getCategoryName());
                addAdRequest.setSubCategoryName(adDetailsViewModel.getAdDetailsItem().getSubCategoryName());
                addAdRequest.setNegotiable(adDetailsViewModel.getAdDetailsItem().getNegotiable()+"");
                addAdRequest.setLongitude(adDetailsViewModel.getAdDetailsItem().getLongitude());
                addAdRequest.setLatitude(adDetailsViewModel.getAdDetailsItem().getLatitude());

                addAdRequest.setCity(adDetailsViewModel.getAdDetailsItem().getCity());
                addAdRequest.setArea(adDetailsViewModel.getAdDetailsItem().getArea());
                addAdRequest.setGovernment(adDetailsViewModel.getAdDetailsItem().getGovernment());

                addAdRequest.setAreaId(adDetailsViewModel.getAdDetailsItem().getAreaId());
                addAdRequest.setGovernmentId(adDetailsViewModel.getAdDetailsItem().getGovernmentId());
                addAdRequest.setCityId(adDetailsViewModel.getAdDetailsItem().getCityId());


                addAdRequest.setDescription(adDetailsViewModel.getAdDetailsItem().getDescription());
                addAdRequest.setTitle(adDetailsViewModel.getAdDetailsItem().getTitle());
                addAdRequest.setPrice(adDetailsViewModel.getAdDetailsItem().getPrice());
                addAdRequest.setImagesItems(adDetailsViewModel.getAdDetailsItem().getImages());




                bundle.putSerializable(Params.BUNDLE_ITEM_AD,addAdRequest);
                MovementManager.startActivity(requireActivity(), UpdateAdFragment.class.getName(),bundle,ResourcesManager.getString(R.string.label_edit_ad));
            }


        });

        //showBottomSheetDialog();

    }



    private void deleteAdDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_delete_ad, null);
        Button addAd = dialogView.findViewById(R.id.btn_add_ad);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);

        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();

        addAd.setOnClickListener(v -> {

            adDetailsViewModel.removeAd();
             alertDialog.cancel();
            alertDialog.dismiss();
        });

        cancel.setOnClickListener(v -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });

        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
    }


    private void showBottomSheetDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity(), R.style.SheetDialog);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_dialog);


        CustomButton btnReport = bottomSheetDialog.findViewById(R.id.btn_report);

        RecyclerView recyclerView = bottomSheetDialog.findViewById(R.id.recycler_view);
        ReportsAdapter reportsAdapter = new ReportsAdapter();
        List<ReportItem> reportItems = new ArrayList<>();


        for(int i=0; i<adDetailsViewModel.reportItems.size(); i++){
            ReportItem reportItem = new ReportItem();
            reportItem.setId(adDetailsViewModel.reportItems.get(i).getId());
            reportItem.setName(adDetailsViewModel.reportItems.get(i).getReason());
            reportItems.add(reportItem);
        }


        btnReport.setOnClickListener(v -> {
                adDetailsViewModel.setReport(adDetailsViewModel.reportItems.get(reportsAdapter.getSelected()).getId());
        });

        reportsAdapter.updateData(reportItems);
        recyclerView.setAdapter(reportsAdapter);

        bottomSheetDialog.show();
    }
}
