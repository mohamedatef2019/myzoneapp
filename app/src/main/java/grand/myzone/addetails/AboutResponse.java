package grand.myzone.addetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.myzone.base.model.DefaultResponse;

public class AboutResponse extends DefaultResponse {

    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("term")
        @Expose
        public String term;

    }

}
