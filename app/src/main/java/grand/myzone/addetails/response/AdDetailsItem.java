package grand.myzone.addetails.response;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.myzone.filter.response.ValuesItem;
import grand.myzone.subcategories.response.Tags;

public class AdDetailsItem {

	@SerializedName("area")
	private String area;

	@SerializedName("images")
	private List<ImagesItem> images;

	@SerializedName("is_favorite")
	private boolean isFavorite;

	@SerializedName("comments")
	private List<CommentsItem> comments;


	@Expose
	@SerializedName("values")
	private List<ValuesItem> valuesItems;

	@SerializedName("city")
	private String city;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("title")
	private String title;

	@SerializedName("negotiable")
	private int negotiable;

	@SerializedName("government")
	private String government;

	@SerializedName("price")
	private String price;

	@SerializedName("comments_count")
	private int commentsCount;

	@SerializedName("id")
	private int id;


	@Expose
	@SerializedName("available")
	private int available=1;

	@SerializedName("tag")
	private Tags tag;

	@SerializedName("user")
	private User user;

	@SerializedName("longitude")
	private String longitude;


	@Expose
	@SerializedName("category_name")
	private String categoryName;

	@Expose
	@SerializedName("sub_category_name")
	private String subCategoryName;


	@Expose
	@SerializedName("category_id")
	private int categoryId=1;


	@Expose
	@SerializedName("sub_category_id")
	private int subCategoryId=1;


	@Expose
	@SerializedName("government_id")
	private String governmentId;

	@Expose
	@SerializedName("city_id")
	private String cityId;

	@Expose
	@SerializedName("area_id")
	private String areaId;

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setImages(List<ImagesItem> images){
		this.images = images;
	}

	public List<ImagesItem> getImages(){
		return images;
	}

	public void setIsFavorite(boolean isFavorite){
		this.isFavorite = isFavorite;
	}

	public boolean isIsFavorite(){
		return isFavorite;
	}

	public void setComments(List<CommentsItem> comments){
		this.comments = comments;
	}

	public List<CommentsItem> getComments(){
		return comments;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setNegotiable(int negotiable){
		this.negotiable = negotiable;
	}

	public int getNegotiable(){
		return negotiable;
	}

	public void setGovernment(String government){
		this.government = government;
	}

	public String getGovernment(){
		return government;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setCommentsCount(int commentsCount){
		this.commentsCount = commentsCount;
	}

	public int getCommentsCount(){
		return commentsCount;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTag(Tags tag){
		this.tag = tag;
	}

	public Tags getTag(){
		return tag;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public int getAvailable() {
		return available;
	}


	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setFavorite(boolean favorite) {
		isFavorite = favorite;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public int getSubCategoryId() {
		return subCategoryId;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public void setGovernmentId(String governmentId) {
		this.governmentId = governmentId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getAreaId() {
		return areaId;
	}

	public String getCityId() {
		return cityId;
	}

	public String getGovernmentId() {
		return governmentId;
	}


	public void setValuesItems(List<ValuesItem> valuesItems) {
		this.valuesItems = valuesItems;
	}

	public List<ValuesItem> getValuesItems() {
		return valuesItems;
	}
}