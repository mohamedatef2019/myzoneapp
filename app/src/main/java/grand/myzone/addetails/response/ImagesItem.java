package grand.myzone.addetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImagesItem implements Serializable {

	@Expose
	@SerializedName("image")
	private String image;

	@Expose
	@SerializedName("updated_at")
	private String updatedAt;

	@Expose
	@SerializedName("advertisement_id")
	private int advertisementId;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("id")
	private int id;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setAdvertisementId(int advertisementId){
		this.advertisementId = advertisementId;
	}

	public int getAdvertisementId(){
		return advertisementId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}