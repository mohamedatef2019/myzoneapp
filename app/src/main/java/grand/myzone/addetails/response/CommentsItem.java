package grand.myzone.addetails.response;

import com.google.gson.annotations.SerializedName;

public class CommentsItem{

	@SerializedName("id")
	private int id;

	@SerializedName("user")
	private User user;

	@SerializedName("content")
	private String content;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return content;
	}
}