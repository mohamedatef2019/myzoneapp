package grand.myzone.addetails.response;

import com.google.gson.annotations.SerializedName;

public class AdDetailsResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private AdDetailsItem data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(AdDetailsItem data){
		this.data = data;
	}

	public AdDetailsItem getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}