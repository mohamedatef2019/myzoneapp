package grand.myzone.help;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import grand.myzone.R;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentHelpBinding;


/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends BaseFragment {

    FragmentHelpBinding binding;
    HelpViewModel viewModel;
    private static final String TAG = "HelpFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_help, container, false);
        viewModel = new HelpViewModel();
        binding.setViewModel(viewModel);
        init();
        return binding.getRoot();
    }

    public void init() {
        viewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {



        });
    }
}
