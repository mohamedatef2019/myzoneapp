package grand.myzone.help;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import grand.myzone.base.model.DefaultResponse;

public class HelpResponse extends DefaultResponse {

    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("value")
        @Expose
        public String value;

    }

}
