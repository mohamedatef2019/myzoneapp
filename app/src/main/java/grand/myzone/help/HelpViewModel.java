package grand.myzone.help;


import android.view.View;

import com.android.volley.Request;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;

public class HelpViewModel extends BaseViewModel {

    public HelpResponse termResponse;

    public HelpViewModel() {
        getTerms();
    }


    private void getTerms() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                termResponse = (HelpResponse) response;
                if(termResponse != null){
                    setReturnedMessage(termResponse.getMsg());
                    getClicksMutableLiveData().setValue(Constant.MESSAGE_DISPLAY);
                    getClicksMutableLiveData().setValue(Constant.SUCCESS);
                    notifyChange();
                }else{
                    getClicksMutableLiveData().setValue(Constant.ERROR_NETWORK);
                }
            }
        }).requestJsonObject(Request.Method.GET, WebServices.HELP, new Object(), HelpResponse.class);
    }

}
