
package grand.myzone.home.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.categories.response.CategoryItem;
import grand.myzone.databinding.ItemHomeBinding;
import grand.myzone.home.response.CategoriesItem;
import grand.myzone.home.viewmodel.CategoriesItemViewModel;



public class HomeCategoriesAdapter extends RecyclerView.Adapter<HomeCategoriesAdapter.CompaniesViewHolder> {

    private List<CategoriesItem> categoriesItems;
    private MutableLiveData<CategoriesItem> itemsOperationsLiveListener;


    public HomeCategoriesAdapter() {
        this.categoriesItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CompaniesViewHolder holder, final int position) {
        CategoriesItem monagatCategoryItem = categoriesItems.get(position);
        CategoriesItemViewModel companyItemViewModel = new CategoriesItemViewModel(monagatCategoryItem);


        if(monagatCategoryItem.getSliderItem()!=null){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT
                    ,(int) ResourcesManager.getDim(R.dimen.dp160h));
            if(position==0) {
                params.setMargins(0, 0, 0, 15);
            }else {
                params.setMargins(0, 15, 0, 15);
            }
            holder.itemHomeBinding.cardView.setRadius(0);
            holder.itemHomeBinding.cardView.setLayoutParams(params);
        }else {
             LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT
                          ,(int) ResourcesManager.getDim(R.dimen.dp120h));
            params.setMargins(10,10,10,10);

            holder.itemHomeBinding.cardView.setLayoutParams(params);
        }

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               getItemsOperationsLiveListener().setValue(categoriesItems.get(position));
           }
       });


        holder.setViewModel(companyItemViewModel);
    }



    public MutableLiveData<CategoriesItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    public CategoriesItem getItem(int position) {
        return categoriesItems.get(position);
    }

    @Override
    public int getItemCount() {
        return this.categoriesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<CategoriesItem> data) {
            this.categoriesItems.clear();
            this.categoriesItems.addAll(data);
        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemHomeBinding itemHomeBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemHomeBinding == null) {
                itemHomeBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemHomeBinding != null) {
                itemHomeBinding.unbind();
            }
        }

        void setViewModel(CategoriesItemViewModel chaletsItemViewModel) {
            if (itemHomeBinding != null) {
                itemHomeBinding.setHomeItemViewModel(chaletsItemViewModel);
            }
        }


    }



}
