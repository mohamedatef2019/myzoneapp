
package grand.myzone.home.view;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.databinding.ItemHomeBinding;
import grand.myzone.databinding.ItemStoryBinding;
import grand.myzone.home.response.StoriesItem;
import grand.myzone.home.viewmodel.StoriesItemViewModel;
import omari.hamza.storyview.StoryView;
import omari.hamza.storyview.callback.StoryClickListeners;
import omari.hamza.storyview.model.MyStory;


public class HomeStoriesAdapter extends RecyclerView.Adapter<HomeStoriesAdapter.CompaniesViewHolder> {

    private List<StoriesItem> storiesItems;
    private MutableLiveData<StoriesItem> itemsOperationsLiveListener;


    public HomeStoriesAdapter() {
        this.storiesItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_story,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        StoriesItem storiesItem = storiesItems.get(position);
        StoriesItemViewModel storiesItemViewModel = new StoriesItemViewModel(storiesItem);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getItemsOperationsLiveListener().setValue(storiesItem);
            }
        });
        holder.setViewModel(storiesItemViewModel);
    }



    public MutableLiveData<StoriesItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.storiesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<StoriesItem> data) {
        Log.e("HOMERESPONSE",data.size()+" ");
            this.storiesItems.clear();
            this.storiesItems.addAll(data);
        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemStoryBinding itemStoryBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemStoryBinding == null) {
                itemStoryBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemStoryBinding != null) {
                itemStoryBinding.unbind();
            }
        }

        void setViewModel(StoriesItemViewModel storiesItemViewModel) {
            if (itemStoryBinding != null) {
                itemStoryBinding.setStoryItemViewModel(storiesItemViewModel);
            }
        }


    }



}
