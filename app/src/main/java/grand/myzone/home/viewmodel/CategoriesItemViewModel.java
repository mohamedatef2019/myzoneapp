
package grand.myzone.home.viewmodel;

import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.joooonho.SelectableRoundedImageView;
import com.makeramen.roundedimageview.RoundedImageView;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.home.response.CategoriesItem;
import grand.myzone.home.response.CategoriesItem;


public class CategoriesItemViewModel extends BaseItemViewModel {
    private CategoriesItem categoriesItem;

    public CategoriesItemViewModel(CategoriesItem categoriesItem) {
        this.categoriesItem = categoriesItem;

    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(ImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }

    @Bindable
    public CategoriesItem getCategoriesItem() {
        return categoriesItem;
    }

    public void onCategoriesItemClicked(){

    }










}
