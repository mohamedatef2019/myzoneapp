package grand.myzone.home.viewmodel;

import android.util.Log;
import android.view.View;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.home.response.CategoriesItem;
import grand.myzone.home.response.HomeResponse;
import grand.myzone.home.response.StoriesItem;
import grand.myzone.home.response.storydetails.StoriesResponse;
import grand.myzone.home.view.HomeCategoriesAdapter;
import grand.myzone.home.view.HomeStoriesAdapter;
import grand.myzone.login.response.UserItem;


public class HomeViewModel extends BaseViewModel {

    HomeCategoriesAdapter homeCategoriesAdapter;
    HomeStoriesAdapter homeStoriesAdapter;
    public ArrayList<VolleyFileObject> volleyFileObjects;

    public HomeViewModel() {
        volleyFileObjects=new ArrayList<>();
        getHomeData();
    }


    @BindingAdapter({"app:home_categories_adapter"})
    public static void getCategoriesBinding(RecyclerView recyclerView, HomeCategoriesAdapter homeCategoriesAdapter) {
        GridLayoutManager glm = new GridLayoutManager(recyclerView.getContext(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                if(homeCategoriesAdapter.getItem(position).getSliderItem()!=null){
                    return 2;
                }
                return 1;
            }

        });
        recyclerView.setLayoutManager(glm);

        recyclerView.setAdapter(homeCategoriesAdapter);
    }

    @BindingAdapter({"app:home_stories_adapter"})
    public static void getStoriesBinding(RecyclerView recyclerView, HomeStoriesAdapter storiesAdapter) {
        recyclerView.setAdapter(storiesAdapter);
    }

 private  HomeResponse homeResponse;
    public void getHomeData() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                 homeResponse = (HomeResponse)response;

                try {

                    int counter= homeResponse.getData().getCategories().size() + (homeResponse.getData().getSlider()==null?0:homeResponse.getData().getSlider().size());
                      List<CategoriesItem> categories = new ArrayList<>();
                      int size=0,size2=0;
                      int moduils=0;

                      for(int i=0; i<counter; i++){

                          if((size<homeResponse.getData().getCategories().size()&&moduils!=8)) {

                              categories.add(homeResponse.getData().getCategories().get(size));
                              size++;
                              moduils++;

                          }else {

                              if(size2<homeResponse.getData().getSlider().size()) {

                                  CategoriesItem categoriesItem = new CategoriesItem();
                                  categoriesItem.setSlider(true);
                                  categoriesItem.setImage(homeResponse.getData().getSlider().get(size2).getImage());
                                  categoriesItem.setSliderItem(homeResponse.getData().getSlider().get(size2));
                                  categories.add(categoriesItem);
                                  size2++;
                                  moduils=0;
                              }else if(size<homeResponse.getData().getCategories().size()) {
                                  categories.add(homeResponse.getData().getCategories().get(size));
                                  size++;
                              }

                          }
                      }


                    getHomeCategoriesAdapter().updateData(categories);
                      List<StoriesItem> stories = new ArrayList<>();
                    StoriesItem storiesItem = new StoriesItem();

                    try {


                        if(homeResponse.getData().isUserHasStory()) {
                            UserItem userItem = UserPreferenceHelper.getUserDetails();
                            storiesItem.setEmail(userItem.getEmail());
                            storiesItem.setName(userItem.getUsersName());
                            storiesItem.setId(userItem.getId());
                            storiesItem.setImage(userItem.getUsersImg());
                            stories.add(storiesItem);
                        }

                        stories.addAll(homeResponse.getData().getStories());

                    }catch (Exception e){
                        e.getStackTrace();
                    }
                    getHomeStoriesAdapter().updateData(stories );
                    getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                }catch (Exception e){
                    Log.e("HOMERESPONSE",e.getLocalizedMessage()+"  "+e.getStackTrace());
                }
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.HOME , null, HomeResponse.class);


    }


    public void loadStoryClick(){
        if(UserPreferenceHelper.isLogined()) {
            getClicksMutableLiveData().setValue(Codes.SELECT_STORY);
        }else {
            getClicksMutableLiveData().setValue(Codes.LOGIN_FIRST);
        }
    }

    public void confirmStoryAdding() {

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPreferenceHelper.getUserDetails().getId()+"");

        Log.e("PARAMSz",volleyFileObjects.size()+"");

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
            DefaultResponse defaultResponse = (DefaultResponse)response;

                if(defaultResponse.getCode()==WebServices.SUCCESS){
                        getHomeData();
                }else {
                    getClicksMutableLiveData().setValue(Codes.NO_ADS_BEFORE);
                }

                notifyChange();
            }
        }).multiPartConnect(WebServices.ADD_STORY, params, volleyFileObjects, DefaultResponse.class,true);
    }
    public StoriesResponse storiesResponse;

    public void getStoryDetails(int userId) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                accessLoadingBar(View.GONE);
                storiesResponse = (StoriesResponse)response;

                if(storiesResponse.getCode()==WebServices.SUCCESS){
                    getClicksMutableLiveData().setValue(Codes.ADD_STORY_DATA);
                }


            }
        }).requestJsonObject(Request.Method.GET,WebServices.GET_STORIES+userId,null, StoriesResponse.class);
    }

    public HomeCategoriesAdapter getHomeCategoriesAdapter() {
        return homeCategoriesAdapter = homeCategoriesAdapter==null?new HomeCategoriesAdapter():homeCategoriesAdapter;
    }


    public HomeStoriesAdapter getHomeStoriesAdapter() {
        return  homeStoriesAdapter = homeStoriesAdapter==null?new HomeStoriesAdapter():homeStoriesAdapter;
    }

    public HomeResponse getHomeResponse() {
        return homeResponse;
    }
}
