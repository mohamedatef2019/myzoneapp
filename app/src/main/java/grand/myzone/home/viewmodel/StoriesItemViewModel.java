
package grand.myzone.home.viewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.home.response.StoriesItem;
import grand.myzone.home.response.StoriesItem;


public class StoriesItemViewModel extends BaseItemViewModel {
    private StoriesItem storiesItem;

    public StoriesItemViewModel(StoriesItem storiesItem) {
        this.storiesItem = storiesItem;

    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }

    @Bindable
    public StoriesItem getStoriesItem() {
        return storiesItem;
    }

    public void onStoriesItemClicked(){

    }



}
