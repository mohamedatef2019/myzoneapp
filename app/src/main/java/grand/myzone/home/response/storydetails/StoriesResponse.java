package grand.myzone.home.response.storydetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import omari.hamza.storyview.model.MyStory;

public class StoriesResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<MyStory> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<MyStory> data){
		this.data = data;
	}

	public List<MyStory> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}