package grand.myzone.home.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoriesItem implements Serializable {

	@Expose
	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;




	@Expose
	private boolean isSlider=false;

	@SerializedName("is_event")
	private int isEvent;

	@SerializedName("id")
	private int id;

	@Expose
	SliderItem sliderItem;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}


	public void setIsEvent(int isEvent) {
		this.isEvent = isEvent;
	}

	public int getIsEvent() {
		return isEvent;
	}

	public void setSliderItem(SliderItem sliderItem) {
		this.sliderItem = sliderItem;
	}

	public SliderItem getSliderItem() {
		return sliderItem;
	}


	public void setSlider(boolean slider) {
		isSlider = slider;
	}

	public boolean isSlider() {
		return isSlider;
	}



}