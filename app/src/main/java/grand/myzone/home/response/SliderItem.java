package grand.myzone.home.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SliderItem implements Serializable {

	@SerializedName("link_type")
	private String linkType;

	@SerializedName("image")
	private String image;

	@SerializedName("id")
	private int id;

	@SerializedName("link_id")
	private int linkId;

	public void setLinkType(String linkType){
		this.linkType = linkType;
	}

	public String getLinkType(){
		return linkType;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLinkId(int linkId){
		this.linkId = linkId;
	}

	public int getLinkId(){
		return linkId;
	}
}