package grand.myzone.home.response;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.myzone.addetails.response.ImagesItem;

public class HomeItem {

	@SerializedName("images")
	private List<SliderItem> slider;


	@SerializedName("slider")
	private List<ImagesItem> images;

	@SerializedName("stories")
	private List<StoriesItem> stories;

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	@Expose
	@SerializedName("user_has_story")
	private boolean userHasStory;



	public List<ImagesItem> getImages() {
		return images;
	}

	public void setImages(List<ImagesItem> images) {
		this.images = images;
	}

	public void setSlider(List<SliderItem> slider){
		this.slider = slider;
	}

	public List<SliderItem> getSlider(){
		return slider;
	}

	public void setStories(List<StoriesItem> stories){
		this.stories = stories;
	}

	public List<StoriesItem> getStories(){
		return stories;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public boolean isUserHasStory() {
		return userHasStory;
	}

	public void setUserHasStory(boolean userHasStory) {
		this.userHasStory = userHasStory;
	}
}