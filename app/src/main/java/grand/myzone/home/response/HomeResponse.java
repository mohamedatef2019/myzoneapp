package grand.myzone.home.response;

import com.google.gson.annotations.SerializedName;

public class HomeResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private HomeItem data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(HomeItem data){
		this.data = data;
	}

	public HomeItem getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}