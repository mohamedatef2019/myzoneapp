package grand.myzone.home;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import app.videocompressor.VideoCompress;
import burhanrashid52.photoediting.EditImageActivity;
import grand.myzone.CustomDialogs;
import grand.myzone.R;
import grand.myzone.SliderImagesAdapter;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.filesutils.FileOperations;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.activities.MainActivity;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.categories.view.CategoriesFragment;
import grand.myzone.comments.response.User;
import grand.myzone.databinding.FragmentHomeBinding;
import grand.myzone.erikagtierrez.multiple_media_picker.Gallery;
import grand.myzone.events.view.EventsFragment;
import grand.myzone.home.response.CategoriesItem;
import grand.myzone.home.response.StoriesItem;
import grand.myzone.home.viewmodel.HomeViewModel;
import grand.myzone.subcategories.view.SubCategoriesFragment;

import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import omari.hamza.storyview.StoryView;
import omari.hamza.storyview.callback.StoryClickListeners;
import omari.hamza.storyview.model.MyStory;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    FragmentHomeBinding binding;
    HomeViewModel viewModel;
    StoriesItem storiesItem;

    private final int PHOTO_EDITOR_REQUEST_CODE = 231;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        viewModel = new HomeViewModel();
        binding.setHomeViewModel(viewModel);


        if (UserPreferenceHelper.getUserDetails().getUsersImg() == null || UserPreferenceHelper.getUserDetails().getUsersImg().equals("")) {
            binding.ivHomeUser.setImageResource(R.drawable.img_avatar);
        } else {
            ConnectionHelper.loadImage(binding.ivHomeUser, UserPreferenceHelper.getUserDetails().getUsersImg());
        }

        init();
        return binding.getRoot();
    }

    String recordedVidePath = "";

    public void init() {

        viewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SELECT_STORY) {
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                boolean permissionGranted = true;

                for (String permission : permissions) {
                    if (ContextCompat.checkSelfPermission(requireActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                        permissionGranted = false;
                    }
                }


                if (permissionGranted) {

                    String[] colors = {getString(R.string.pick_image), getString(R.string.record_video), getString(R.string.select_from_gallery)};

                    AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
                    builder.setTitle("Pick a color");
                    builder.setItems(colors, (dialog, which) -> {
                        if (which == 0) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(Intent.createChooser(intent, ResourcesManager.getString(R.string.label_select_image_from)), ACTION_TAKE_IMAGE);
                        }else if(which == 1){

                            recordedVidePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"  + new Date().getTime() + "_" + UserPreferenceHelper.getUserDetails().getId() + ".mp4";
                            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
                            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, recordedVidePath);
                            startActivityForResult(takeVideoIntent, ACTION_TAKE_VIDEO);
                        }else {
                            Intent intent = new Intent(requireActivity(), Gallery.class);
                            intent.putExtra("title", "Select media");
                            intent.putExtra("mode", 1);
                            intent.putExtra("maxSelection", 1);
                            startActivityForResult(intent, OPEN_MEDIA_PICKER);
                        }
                    });
                    builder.show();

                } else {
                    requireActivity().requestPermissions(permissions, ACTION_TAKE_IMAGE);
                }
            }
            if (result == Codes.NO_ADS_BEFORE) {
                noAdDialog();
            } else if (result == Codes.ADD_STORY_DATA) {
                showStories(new ArrayList<>(viewModel.storiesResponse.getData()));
            } else if (result == Codes.LOGIN_FIRST) {
                CustomDialogs.loginToContinue(requireActivity());
            } else if (result == Codes.DATA_RECIVED) {
                SliderImagesAdapter adapter = new SliderImagesAdapter(requireActivity(), viewModel.getHomeResponse().getData().getImages());
                SliderView sliderView = binding.imageSlider;
                sliderView.setSliderAdapter(adapter);
                sliderView.setIndicatorAnimation(IndicatorAnimationType.FILL); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                sliderView.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION);
                sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                sliderView.setIndicatorSelectedColor(ResourcesManager.getColor(R.color.colorAccent));
                sliderView.setIndicatorUnselectedColor(Color.GRAY);
                sliderView.setScrollTimeInSec(4);
                sliderView.startAutoCycle();

            }
        });

        viewModel.getHomeCategoriesAdapter().getItemsOperationsLiveListener().observe(getViewLifecycleOwner(), categoriesItem -> {

            if (categoriesItem.isSlider()) {
                MovementManager.startWebPage(requireActivity(), "https://www.google.com/");
            } else {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_CATEGORY, categoriesItem);
                if (categoriesItem.getIsEvent() == 1) {
                    MovementManager.startActivity(requireActivity(), EventsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_events));
                } else {
                    MovementManager.startActivity(requireActivity(), SubCategoriesFragment.class.getName(), bundle, categoriesItem.getName());
                }
            }


        });

        viewModel.getHomeCategoriesAdapter().getItemsOperationsLiveListener().observe(getViewLifecycleOwner(), categoriesItem -> {

        });

        viewModel.getHomeStoriesAdapter().getItemsOperationsLiveListener().observe(getViewLifecycleOwner(), storiesItem -> {
            HomeFragment.this.storiesItem = storiesItem;
            viewModel.getStoryDetails(storiesItem.getId());
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showStories(ArrayList<MyStory> myStories) {

        if (myStories == null || myStories.size() < 1) return;

        new StoryView.Builder(requireActivity().getSupportFragmentManager())
                .setStoriesList(myStories)
                .setStoryDuration(5000 * myStories.size())
                .setTitleText(storiesItem.getName())
                .setSubtitleText("")
                .setTitleLogoUrl(storiesItem.getImage())
                .setStoryClickListeners(new StoryClickListeners() {
                    @Override
                    public void onDescriptionClickListener(int position) {

                    }

                    @Override
                    public void onTitleIconClickListener(int position) {

                    }
                })
                .build()
                .show();
    }

    static final int OPEN_MEDIA_PICKER = 1;
    static final int ACTION_TAKE_VIDEO = 2;
    static final int ACTION_TAKE_IMAGE = 3;
    static final int ACTION_IMAGE_GOT=40;
    int counter = 0;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("SSDS",requestCode+"  "+resultCode);

        if (requestCode == OPEN_MEDIA_PICKER) {

            if (resultCode == Activity.RESULT_OK && data != null) {

                ArrayList<String> selectionResult = data.getStringArrayListExtra("result");

                for (int i = 0; i < selectionResult.size(); i++) {
                    counter++;
                    VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(requireActivity(),
                            selectionResult.get(i), "files[" + i + "]",
                            Codes.FILE_TYPE_VIDEO);


                    if (volleyFileObject.getFilePath().contains("mp4") || volleyFileObject.getFilePath().contains("mov")) {
                        String dest = volleyFileObject.getFilePath();

                        String stt = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + new Date().getTime() + "_"
                                + UserPreferenceHelper.getUserDetails().getId() + ".mp4";

                        VideoCompress.compressVideoLow(dest, stt
                                , new VideoCompress.CompressListener() {
                                    @Override
                                    public void onStart() {

                                    }

                                    @Override
                                    public void onSuccess() {

                                        volleyFileObject.setFilePath(stt);

                                        viewModel.volleyFileObjects.add(volleyFileObject);

                                    }

                                    @Override
                                    public void onFail() {
                                        //Failed
                                    }

                                    @Override
                                    public void onProgress(float percent) {
                                        //Progress
                                    }
                                });
                            } else {

                        try {
                            // This could throw if either `sourcePath` or `outputPath` is blank or Null

                            editPhotoDialog(volleyFileObject.getFilePath());

                        } catch (Exception e) {
                            Log.e("DASA", e.getMessage() +" "+e.getStackTrace()); // This could throw if either `sourcePath` or `outputPath` is blank or Null
                        }


                        }
                }

            }
            //viewModel.confirmStoryAdding();
        } else if (requestCode == ACTION_TAKE_VIDEO) {


            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(requireActivity(),
                   data, "files[0]",
                    Codes.FILE_TYPE_VIDEO);

            String stt=Environment.getExternalStorageDirectory().getAbsolutePath() + "/"  + new Date().getTime() + "_" + UserPreferenceHelper.getUserDetails().getId()+".mp4";

            VideoCompress.compressVideoLow(volleyFileObject.getFilePath(),stt
                    ,               new VideoCompress.CompressListener() {
                        @Override
                        public void onStart() {
                            accessLoadingBar(View.VISIBLE);
                        }

                        @Override
                        public void onSuccess() {

                                volleyFileObject.setFilePath(stt);
                            viewModel.volleyFileObjects.add(volleyFileObject);

                            viewModel.confirmStoryAdding();
                        }

                        @Override
                        public void onFail() {
                            //Failed
                        }

                        @Override
                        public void onProgress(float percent) {
                            //Progress
                        }
                    });



        } else if (requestCode == ACTION_TAKE_IMAGE) {

            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(requireActivity(), data, "files[0]", Codes.FILE_TYPE_IMAGE);
            editPhotoDialog(volleyFileObject.getFilePath());

        }else if(requestCode == ACTION_IMAGE_GOT){
            Log.e("SSSSSSSS",data.getStringExtra("image"));
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(requireActivity(), data.getStringExtra("image"), "files[0]", Codes.FILE_TYPE_IMAGE);
            viewModel.volleyFileObjects.add(volleyFileObject);
             viewModel.confirmStoryAdding();
        }
    }

    private void editPhotoDialog(String image){
        Intent intent = new Intent(requireActivity(), EditImageActivity.class);
        intent.putExtra("image",image);
        startActivityForResult(intent,ACTION_IMAGE_GOT);
    }


    private void noAdDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_no_ads, null);
        Button addAd = dialogView.findViewById(R.id.btn_add_ad);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);

        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();

        addAd.setOnClickListener(v -> {
            MovementManager.startActivity(requireActivity(), CategoriesFragment.class.getName(), ResourcesManager.getString(R.string.label_add_ad));
            alertDialog.cancel();
            alertDialog.dismiss();
        });

        cancel.setOnClickListener(v -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });

        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
    }


}
