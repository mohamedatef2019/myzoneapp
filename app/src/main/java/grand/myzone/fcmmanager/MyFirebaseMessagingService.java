package grand.myzone.fcmmanager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import grand.myzone.R;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.activities.MainActivity;



public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyMessagingService";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {

        try {
            if (remoteMessage.getNotification() != null) {
                sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), "1" );
            }
        }catch (Exception E){
            E.getStackTrace();
        }


        String details = remoteMessage.getData().get("message");
        try {
            JSONObject data = new JSONObject(details);
            sendNotification(data.getString("title"), data.getString("body"),
                    data.has("notification_type")?data.getString("notification_type"):""+"");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onNewToken(@NonNull String token) {
        UserPreferenceHelper.saveGoogleToken( token);
        System.out.println("Refreshed token:" + token);
    }

    private void sendNotification(String title, String body,String type) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        if (type.equals("3")) {

            Intent dialogIntent = new Intent("messageIntent");
            getApplicationContext().sendBroadcast(dialogIntent);
        }

        String channelId = "channelId";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }
}