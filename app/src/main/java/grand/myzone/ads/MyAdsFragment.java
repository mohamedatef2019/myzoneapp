package grand.myzone.ads;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.myzone.R;
import grand.myzone.ads.viewmodel.AdsViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.activities.BaseActivity;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentAdsBinding;
import grand.myzone.databinding.FragmentMyAdsBinding;
import grand.myzone.filter.FilterFragment;
import grand.myzone.filter.FilterRequest;
import grand.myzone.subcategories.response.SubCategoriesItem;


public class MyAdsFragment extends BaseFragment {
    private View rootView;
    private AdsViewModel adsViewModel;
    private FragmentMyAdsBinding fragmentEventsBinding;
    SubCategoriesItem subCategoriesItem;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentEventsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_ads, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentEventsBinding.getRoot();
        adsViewModel = new AdsViewModel();
        adsViewModel.getMyAds();
        fragmentEventsBinding.setAdsViewModel(adsViewModel);
    }


    private void liveDataListeners() {
        adsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if(result == Codes.FILTER_SCREEN){

                Bundle bundle = new Bundle();

                bundle.putInt(Params.BUNDLE_CATEGORY,subCategoriesItem.getCategoryId());

                bundle.putInt(Params.BUNDLE_SUB_CATEGORY,subCategoriesItem.getId());


                Log.e("tagsTZIE",subCategoriesItem.getId()+"");

                Intent intent = new Intent(context, BaseActivity.class);

                intent.putExtra(Params.INTENT_PAGE, FilterFragment.class.getName());
                intent.putExtra(Params.INTENT_PAGE_TITLE,"");
                intent.putExtra(Params.INTENT_BUNDLE,bundle);
                startActivityForResult(intent,1693);

            }else if(result == Codes.MY_SELECTED_ADS){
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_SELECTED_ADS,adsViewModel.advertisements);
                MovementManager.startActivity(requireActivity(),MySelectedAdsFragment.class.getName(),bundle, ResourcesManager.getString(R.string.my_ads));
            }else if(result == Codes.SHOW_MESSAGE){
                Toast.makeText(requireActivity(), ""+adsViewModel.getReturnedMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        adsViewModel.getFilteredData((FilterRequest) data.getSerializableExtra(Params.INTENT_FILTER));
    }
}