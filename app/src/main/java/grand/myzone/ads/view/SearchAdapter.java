package grand.myzone.ads.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.ads.response.searchresult.SearchItem;
import grand.myzone.ads.viewmodel.SearchItemViewModel;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.databinding.ItemSearchBinding;
import grand.myzone.databinding.ItemTagBinding;
import grand.myzone.subcategories.response.SubCategoriesItem;
import grand.myzone.subcategories.response.TagsItem;
import grand.myzone.subcategories.viewmodel.TagItemViewModel;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SpecsAdaptersViewHolder> {

    private List<SearchItem> searchItems;


    public SearchAdapter() {
        this.searchItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public SearchAdapter.SpecsAdaptersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search,
                new FrameLayout(parent.getContext()), false);
        return new SearchAdapter.SpecsAdaptersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchAdapter.SpecsAdaptersViewHolder holder, final int position) {
        SearchItem searchItem = searchItems.get(position);
        SearchItemViewModel searchItemViewModel = new SearchItemViewModel(searchItem);
        searchItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));

        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            SubCategoriesItem eventItem = new SubCategoriesItem();
            eventItem.setId(searchItem.getSubCategoryId());
            eventItem.setCategoryId(searchItem.getCategoryId());
            bundle.putString(Params.SEARCH,searchItems.get(position).getTitle());
            bundle.putSerializable(Params.BUNDLE_SUB_CATEGORY,eventItem);

            MovementManager.startActivity(v.getContext(), AdsFragment.class.getName(),bundle, ResourcesManager.getString(R.string.label_ads));
        });
        holder.setViewModel(searchItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.searchItems.size();
    }


    @Override
    public void onViewAttachedToWindow(SearchAdapter.SpecsAdaptersViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(SearchAdapter.SpecsAdaptersViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<SearchItem> data) {

        this.searchItems.clear();

        this.searchItems.addAll(data);

        notifyDataSetChanged();
    }

    static class SpecsAdaptersViewHolder extends RecyclerView.ViewHolder {
        ItemSearchBinding itemSearchBinding;

        SpecsAdaptersViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemSearchBinding == null) {
                itemSearchBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemSearchBinding != null) {
                itemSearchBinding.unbind();
            }
        }

        void setViewModel(SearchItemViewModel searchItemViewModel) {
            if (itemSearchBinding != null) {
                itemSearchBinding.setSearchItemViewModel(searchItemViewModel);
            }
        }


    }



}
