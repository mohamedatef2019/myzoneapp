package grand.myzone.ads.view;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.databinding.ItemTagBinding;
import grand.myzone.subcategories.response.TagsItem;
import grand.myzone.subcategories.viewmodel.TagItemViewModel;


public class SpecsAdapter extends RecyclerView.Adapter<SpecsAdapter.SpecsAdaptersViewHolder> {

    private List<TagsItem> tagsItems;


    public SpecsAdapter() {
        this.tagsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public SpecsAdapter.SpecsAdaptersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag,
                new FrameLayout(parent.getContext()), false);
        return new SpecsAdapter.SpecsAdaptersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SpecsAdapter.SpecsAdaptersViewHolder holder, final int position) {
        TagsItem tagsItem = tagsItems.get(position);
        TagItemViewModel tagItemViewModel = new TagItemViewModel(tagsItem);
        tagItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(tagItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.tagsItems.size();
    }


    @Override
    public void onViewAttachedToWindow(SpecsAdapter.SpecsAdaptersViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(SpecsAdapter.SpecsAdaptersViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<TagsItem> data) {

        this.tagsItems.clear();

        this.tagsItems.addAll(data);

        notifyDataSetChanged();
    }

    static class SpecsAdaptersViewHolder extends RecyclerView.ViewHolder {
        ItemTagBinding itemTagBinding;

        SpecsAdaptersViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemTagBinding == null) {
                itemTagBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemTagBinding != null) {
                itemTagBinding.unbind();
            }
        }

        void setViewModel(TagItemViewModel tagItemViewModel) {
            if (itemTagBinding != null) {
                itemTagBinding.setTagItemViewModel(tagItemViewModel);
            }
        }


    }



}
