package grand.myzone.ads.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.ads.viewmodel.Tag2ItemViewModel;
import grand.myzone.databinding.ItemTag2Binding;
import grand.myzone.databinding.ItemTagBinding;
import grand.myzone.events.response.ValuesItem;
import grand.myzone.subcategories.response.TagsItem;



public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.SpecsAdaptersViewHolder> {

    private List<ValuesItem> tagsItems;
    private MutableLiveData<ValuesItem> valuesItemMutableLiveData;
    private int selected = -1;
    public TagsAdapter() {
        valuesItemMutableLiveData = new MutableLiveData<>();
        this.tagsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public TagsAdapter.SpecsAdaptersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag2,
                new FrameLayout(parent.getContext()), false);
        return new TagsAdapter.SpecsAdaptersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TagsAdapter.SpecsAdaptersViewHolder holder, final int position) {
        ValuesItem tagsItem = tagsItems.get(position);
        Tag2ItemViewModel tagItemViewModel = new Tag2ItemViewModel(tagsItem);

        holder.itemView.setOnClickListener(v -> {


            if (selected != -1) {
                tagsItems.get(selected).setSelected(false);
            }
            tagsItems.get(position).setSelected(true);
            selected = position;

            notifyDataSetChanged();

            valuesItemMutableLiveData.setValue(tagsItem);

        });


        holder.setViewModel(tagItemViewModel);
    }

    public MutableLiveData<ValuesItem> getValuesItemMutableLiveData() {
        return valuesItemMutableLiveData;
    }

    @Override
    public int getItemCount() {
        return this.tagsItems.size();
    }


    @Override
    public void onViewAttachedToWindow(TagsAdapter.SpecsAdaptersViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(TagsAdapter.SpecsAdaptersViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ValuesItem> data) {

        this.tagsItems.clear();

        this.tagsItems.addAll(data);

        notifyDataSetChanged();
    }

    static class SpecsAdaptersViewHolder extends RecyclerView.ViewHolder {
        ItemTag2Binding itemTagBinding;

        SpecsAdaptersViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemTagBinding == null) {
                itemTagBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemTagBinding != null) {
                itemTagBinding.unbind();
            }
        }

        void setViewModel(Tag2ItemViewModel tagItemViewModel) {
            if (itemTagBinding != null) {
                itemTagBinding.setTagItemViewModel(tagItemViewModel);
            }
        }


    }



}
