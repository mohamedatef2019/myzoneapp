package grand.myzone.ads.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.CustomDialogs;
import grand.myzone.ads.request.ReportItem;
import grand.myzone.ads.viewmodel.AdsViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.activities.BaseActivity;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentAdsBinding;
import grand.myzone.filter.FilterFragment;
import grand.myzone.filter.FilterRequest;
import grand.myzone.subcategories.response.SubCategoriesItem;
import grand.myzone.subcategories.response.TagsItem;


public class AdsFragment extends BaseFragment {

    private View rootView;
    private AdsViewModel adsViewModel;
    private FragmentAdsBinding fragmentEventsBinding;
    SubCategoriesItem subCategoriesItem;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentEventsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ads, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentEventsBinding.getRoot();

        if(getArguments().containsKey(Params.BUNDLE_TAG)){
            adsViewModel = new AdsViewModel();
            adsViewModel.getSearchByTag((SubCategoriesItem) getArguments().getSerializable(Params.BUNDLE_SUB_CATEGORY), (TagsItem) getArguments().getSerializable(Params.BUNDLE_TAG));
        }


        else if(getArguments().containsKey(Params.SEARCH)){
            adsViewModel = new AdsViewModel();
            adsViewModel.getSearchAds((SubCategoriesItem) getArguments().getSerializable(Params.BUNDLE_SUB_CATEGORY),getArguments().getString(Params.SEARCH));
        }else {
            subCategoriesItem = (SubCategoriesItem) getArguments().getSerializable(Params.BUNDLE_SUB_CATEGORY);
            adsViewModel = new AdsViewModel(subCategoriesItem);
        }


        fragmentEventsBinding.setAdsViewModel(adsViewModel);
    }


    private void liveDataListeners() {
        adsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.FILTER_SCREEN) {

                if(adsViewModel.getAdsAdapter()==null||adsViewModel.getAdsAdapter().getItemCount()==0){
                    CustomDialogs.wariningDialog(requireActivity(),ResourcesManager.getString(R.string.no_data));
                }else {

                    try {
                        Bundle bundle = new Bundle();
                        if (subCategoriesItem != null) {
                            bundle.putInt(Params.BUNDLE_CATEGORY, subCategoriesItem.getCategoryId());
                            bundle.putInt(Params.BUNDLE_SUB_CATEGORY, subCategoriesItem.getId());
                        }
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Params.INTENT_PAGE, FilterFragment.class.getName());
                        intent.putExtra(Params.INTENT_PAGE_TITLE, "");
                        intent.putExtra(Params.INTENT_BUNDLE, bundle);
                        startActivityForResult(intent, 1693);

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }
            } else if (result == Codes.SEARCH) {
                MovementManager.startActivity(requireActivity(), SearchFragment.class.getName(), ResourcesManager.getString(R.string.label_search));
            }else if(result == Codes.SORT_CLICK){
                if(adsViewModel.getAdsAdapter()==null||adsViewModel.getAdsAdapter().getItemCount()==0){
                    CustomDialogs.wariningDialog(requireActivity(),ResourcesManager.getString(R.string.no_data));
                }else {
                    showBottomSheetDialog();
                }
            }else if(result == Codes.EMPTY_DATA_2){
                CustomDialogs.wariningDialog(requireActivity(),ResourcesManager.getString(R.string.no_data));
            }

        });

        adsViewModel.getTagsAdapter2().getValuesItemMutableLiveData().observe(getViewLifecycleOwner(), tagsItem -> {
            adsViewModel.filterTag(tagsItem.getId());
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            FilterRequest filterRequest = (FilterRequest) data.getSerializableExtra(Params.INTENT_FILTER);

            if(subCategoriesItem!=null) {
                filterRequest.setCategoryId(subCategoriesItem.getCategoryId());
                filterRequest.setSubCategoryId(subCategoriesItem.getId());
            }

            adsViewModel.getFilteredData(filterRequest);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


    private void showBottomSheetDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity(),R.style.SheetDialog);

        bottomSheetDialog.setContentView(R.layout.bottom_sheet_dialog);


        RecyclerView recyclerView = bottomSheetDialog.findViewById(R.id.recycler_view);
        ReportsAdapter reportsAdapter = new ReportsAdapter();
        List<ReportItem> reportItems = new ArrayList<>();

        ReportItem reportItem = new ReportItem();
        reportItem.setId(1);
        reportItem.setName(ResourcesManager.getString(R.string.label_newest));
        reportItem.setKey("newest");
        reportItems.add(reportItem);

        ReportItem reportItem2 = new ReportItem();
        reportItem2.setId(2);
        reportItem2.setName(ResourcesManager.getString(R.string.label_oldest));
        reportItem2.setKey("oldest");
        reportItems.add(reportItem2);

        ReportItem reportItem3 = new ReportItem();
        reportItem3.setId(3);
        reportItem3.setName(ResourcesManager.getString(R.string.label_lowest));
        reportItem3.setKey("lowest");
        reportItems.add(reportItem3);


        ReportItem reportItem4 = new ReportItem();
        reportItem4.setId(4);
        reportItem4.setName(ResourcesManager.getString(R.string.label_highest));
        reportItem4.setKey("highest");
        reportItems.add(reportItem4);



        reportsAdapter.updateData(reportItems);
        recyclerView.setAdapter(reportsAdapter);


        reportsAdapter.getMutableLiveData().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                bottomSheetDialog.cancel();
                bottomSheetDialog.dismiss();
                    adsViewModel.getOrderBy(reportItems.get(integer).getKey());
            }
        });

        bottomSheetDialog.show();
    }
}