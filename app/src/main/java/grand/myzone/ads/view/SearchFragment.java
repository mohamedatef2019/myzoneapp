package grand.myzone.ads.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.myzone.R;
import grand.myzone.ads.viewmodel.AdsViewModel;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentSearchBinding;


public class SearchFragment extends BaseFragment {
    private View rootView;
    private AdsViewModel adsViewModel;
    private FragmentSearchBinding fragmentSearchBinding;



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSearchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);



        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

        fragmentSearchBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                    adsViewModel.getSearch(s.toString());

            }
        });
    }

    private void binding() {
        rootView = fragmentSearchBinding.getRoot();
        adsViewModel = new AdsViewModel();
        fragmentSearchBinding.setAdsViewModel(adsViewModel);
    }


    private void liveDataListeners() {
        adsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });
    }


}