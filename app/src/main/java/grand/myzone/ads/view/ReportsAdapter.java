
package grand.myzone.ads.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.ads.request.ReportItem;
import grand.myzone.ads.viewmodel.ReportItemViewModel;
import grand.myzone.databinding.ItemReportBinding;


public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.CategoriesViewHolder> {

    private List<ReportItem> reportItems;
    private int selected = -1;
    private MutableLiveData<Integer> mutableLiveData;

    public ReportsAdapter() {
        mutableLiveData = new MutableLiveData<>();
        this.reportItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ReportItem adsItem = reportItems.get(position);
        ReportItemViewModel orderItemViewModel = new ReportItemViewModel(adsItem);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(orderItemViewModel);

        holder.itemView.setOnClickListener(v -> {

            if (selected != -1) {
                reportItems.get(selected).setSelected(false);
            }
            reportItems.get(position).setSelected(true);
            selected = position;
            mutableLiveData.setValue(position);
            notifyDataSetChanged();
        });

    }

    public int getSelected() {
        return selected;
    }

    public MutableLiveData<Integer> getMutableLiveData() {
        return mutableLiveData;
    }

    @Override
    public int getItemCount() {
        return this.reportItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ReportItem> data) {

        this.reportItems.clear();

        assert data != null;
        this.reportItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemReportBinding itemReportBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemReportBinding == null) {
                itemReportBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemReportBinding != null) {
                itemReportBinding.unbind();
            }
        }

        void setViewModel(ReportItemViewModel adItemViewModel) {
            if (itemReportBinding != null) {
                itemReportBinding.setReportViewModel(adItemViewModel);
            }
        }


    }


}
