
package grand.myzone.ads.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.addetails.AdDetailsFragment;
import grand.myzone.addetails.MyAdDetailsFragment;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.ads.viewmodel.AdItemViewModel;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.databinding.ItemAdBinding;
import grand.myzone.databinding.ItemMyAdBinding;


public class MyAdsAdapter extends RecyclerView.Adapter<MyAdsAdapter.CategoriesViewHolder> {

    private List<AdsItem> adsItems;


    public MyAdsAdapter() {
        this.adsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ad,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        AdsItem adsItem = adsItems.get(position);
        AdItemViewModel orderItemViewModel = new AdItemViewModel(adsItem);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(orderItemViewModel);

        holder.itemView.setOnClickListener(v -> {

            if(!adsItems.get(position).isVisiable()) {

                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ITEM_AD, adsItem);
                MovementManager.startActivity(v.getContext(), MyAdDetailsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_ad_details));


            }else {

                adsItems.get(position).setChecked(!adsItems.get(position).isChecked());
                notifyDataSetChanged();

            }
        });
    }

    public List<AdsItem> getAdsItems() {
        return adsItems;
    }

    @Override
    public int getItemCount() {
        return this.adsItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<AdsItem> data) {

            this.adsItems.clear();

            this.adsItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemMyAdBinding itemAdBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemAdBinding == null) {
                itemAdBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemAdBinding != null) {
                itemAdBinding.unbind();
            }
        }

        void setViewModel(AdItemViewModel adItemViewModel) {
            if (itemAdBinding != null) {
                itemAdBinding.setAdItemViewModel(adItemViewModel);
            }
        }


    }



}
