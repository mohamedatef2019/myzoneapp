package grand.myzone.ads.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.myzone.R;
import grand.myzone.ads.viewmodel.AdsViewModel;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentFavouriteAdsBinding;



public class FavouritesFragment extends BaseFragment {
    private View rootView;
    private AdsViewModel adsViewModel;
    private FragmentFavouriteAdsBinding fragmentEventsBinding;



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentEventsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favourite_ads, container, false);



        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentEventsBinding.getRoot();
        adsViewModel = new AdsViewModel();
        adsViewModel.getFavourites();
        fragmentEventsBinding.setAdsViewModel(adsViewModel);
    }


    private void liveDataListeners() {
        adsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });
    }


}