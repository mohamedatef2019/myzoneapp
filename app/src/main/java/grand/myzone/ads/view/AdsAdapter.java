
package grand.myzone.ads.view;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.CustomDialogs;
import grand.myzone.R;
import grand.myzone.addetails.AdDetailsFragment;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.ads.viewmodel.AdItemViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.chatmessages.view.ChatMessagesFragment;
import grand.myzone.databinding.ItemAdBinding;
import grand.myzone.databinding.ItemEventBinding;


public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.CategoriesViewHolder> {

    private List<AdsItem> adsItems;


    public AdsAdapter() {
        this.adsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        AdsItem adsItem = adsItems.get(position);
        AdItemViewModel orderItemViewModel = new AdItemViewModel(adsItem);

        orderItemViewModel.getItemsOperationsLiveListener().observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(UserPreferenceHelper.isLogined()&&integer != Codes.LOGIN_FIRST) {
                if (integer == Codes.CALL_CLICK) {
                    try {
                        ((Activity)holder.itemView.getContext()).requestPermissions(new String[]{Manifest.permission.CALL_PHONE},234);
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + adsItem.getUser().getPhone()));
                        holder.itemView.getContext().startActivity(intent);
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                } else if (integer == Codes.CHAT_CLICK) {
                    Bundle bundle = new Bundle();

                    bundle.putInt(Params.USER_ID,adsItem.getUser().getId());
                    MovementManager.startActivity(holder.itemView.getContext(), ChatMessagesFragment.class.getName(),bundle,ResourcesManager.getString(R.string.chat));
                }
                }else {
                    CustomDialogs.loginToContinue(holder.itemView.getContext());
                }
            }
        });
        holder.setViewModel(orderItemViewModel);

        holder.itemView.setOnClickListener(v -> {

            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_ITEM_AD, adsItem);
            MovementManager.startActivity(v.getContext(), AdDetailsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_ad_details));


        });
    }

    @Override
    public int getItemCount() {
        return this.adsItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<AdsItem> data) {

        this.adsItems.clear();

        this.adsItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemAdBinding itemAdBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemAdBinding == null) {
                itemAdBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemAdBinding != null) {
                itemAdBinding.unbind();
            }
        }

        void setViewModel(AdItemViewModel adItemViewModel) {
            if (itemAdBinding != null) {
                itemAdBinding.setAdItemViewModel(adItemViewModel);
            }
        }


    }


}
