package grand.myzone.ads.request;

import com.google.gson.annotations.SerializedName;

public class FollowRequest {

    @SerializedName("user_id")
    int userId;

    public FollowRequest() {
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
