package grand.myzone.ads.request;

import com.google.gson.annotations.SerializedName;

public class EventsRequest {

    @SerializedName("user_id")
    int userId;

    public EventsRequest(int userId) {
        this.userId = userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
