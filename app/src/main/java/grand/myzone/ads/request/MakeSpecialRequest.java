package grand.myzone.ads.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MakeSpecialRequest {

    @SerializedName("advertisement_ids")
    ArrayList<Integer>adsId;


    public void setAdsId(ArrayList<Integer> adsId) {
        this.adsId = adsId;
    }

    public ArrayList<Integer> getAdsId() {
        return adsId;
    }
}
