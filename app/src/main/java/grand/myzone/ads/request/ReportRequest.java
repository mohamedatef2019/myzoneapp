package grand.myzone.ads.request;

import com.google.gson.annotations.SerializedName;

public class ReportRequest {

    @SerializedName("advertisement_id")
    int adsId;

    @SerializedName("reason_id")
    int reasonId;

    public ReportRequest() {
    }

    public void setAdsId(int adsId) {
        this.adsId = adsId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public int getAdsId() {
        return adsId;
    }

    public int getReasonId() {
        return reasonId;
    }


}
