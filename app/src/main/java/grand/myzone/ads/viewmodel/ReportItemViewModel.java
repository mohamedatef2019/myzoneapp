package grand.myzone.ads.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.android.volley.Request;
import com.joooonho.SelectableRoundedImageView;
import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.ads.request.ReportItem;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.comments.request.AddCommentRequest;


public class ReportItemViewModel extends BaseItemViewModel {

    private ReportItem reportItem;



    public ReportItemViewModel(ReportItem reportItem) {
        this.reportItem = reportItem;

    }

    @Bindable
    public ReportItem getReportItem() {
        return reportItem;
    }





    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
