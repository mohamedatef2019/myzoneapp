package grand.myzone.ads.viewmodel;



import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import com.android.volley.Request;
import com.joooonho.SelectableRoundedImageView;
import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.ads.response.AdsItem;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.comments.request.AddCommentRequest;


public class AdItemViewModel extends BaseItemViewModel {

    private AdsItem adsItem;

    public AdItemViewModel(AdsItem adsItem) {
        this.adsItem = adsItem;

    }

    @Bindable
    public AdsItem getAdsItem() {
        return adsItem;
    }


    public void onFavouriteClick(){

        if(UserPreferenceHelper.isLogined()) {
            adsItem.setIsFavorite(!adsItem.isIsFavorite());
            notifyChange();
            AddCommentRequest addCommentRequest = new AddCommentRequest();
            addCommentRequest.setAdsId(adsItem.getId());


            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {

                }
            }).requestJsonObject(Request.Method.POST, WebServices.ADD_FAVOURITE, addCommentRequest, DefaultResponse.class);
        }else {

            getItemsOperationsLiveListener().setValue(Codes.LOGIN_FIRST);

        }
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }

    public void onCallClick(){
        getItemsOperationsLiveListener().setValue(Codes.CALL_CLICK);
    }

    public void onChatClick(){
        getItemsOperationsLiveListener().setValue(Codes.CHAT_CLICK);
    }
}
