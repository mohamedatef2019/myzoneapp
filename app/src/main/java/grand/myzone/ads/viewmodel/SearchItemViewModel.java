package grand.myzone.ads.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.joooonho.SelectableRoundedImageView;

import grand.myzone.ads.response.AdsItem;
import grand.myzone.ads.response.searchresult.SearchItem;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;


public class SearchItemViewModel extends BaseItemViewModel {

    private SearchItem searchItem;



    public SearchItemViewModel(SearchItem searchItem) {
        this.searchItem = searchItem;

    }

    @Bindable
    public SearchItem getSearchItem() {
        return searchItem;
    }


}
