package grand.myzone.ads.viewmodel;


import androidx.databinding.Bindable;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.events.response.ValuesItem;


public class Tag2ItemViewModel extends BaseItemViewModel {

    private ValuesItem tagsItem;

    public Tag2ItemViewModel(ValuesItem tagsItem) {
        this.tagsItem = tagsItem;

    }

    @Bindable
    public ValuesItem getTagsItem() {
        return tagsItem;
    }


}
