package grand.myzone.ads.viewmodel;


import android.util.Log;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.ads.request.MakeSpecialRequest;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.ads.response.AdsResponse;
import grand.myzone.ads.response.Advertisements;
import grand.myzone.ads.response.searchresult.SearchResponse;
import grand.myzone.ads.view.AdsAdapter;
import grand.myzone.ads.view.MyAdsAdapter;
import grand.myzone.ads.view.SearchAdapter;
import grand.myzone.ads.view.SpecsAdapter;
import grand.myzone.ads.view.TagsAdapter;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.events.response.ValuesItem;
import grand.myzone.filter.FilterRequest;
import grand.myzone.subcategories.response.SubCategoriesItem;
import grand.myzone.subcategories.response.TagsItem;


public class AdsViewModel extends BaseViewModel {

    private AdsAdapter adsAdapter;
    private SpecsAdapter tagsAdapter;
    private TagsAdapter tagsAdapter2;
    private SearchAdapter searchAdapter;
    private MyAdsAdapter myAdsAdapter;
    private SubCategoriesItem subCategoriesItem;
    private List<AdsItem> adsItems;
    public String phone;

    public int total = 0;

    public AdsViewModel(SubCategoriesItem subCategoriesItem) {
        this.subCategoriesItem = subCategoriesItem;
        phone= UserPreferenceHelper.getUserDetails().getPhone();
        getAds();
    }

    public AdsViewModel() {

    }


    @BindingAdapter({"app:ads_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, AdsAdapter subCategoriesAdapter) {
        recyclerView.setAdapter(subCategoriesAdapter);
    }

    @BindingAdapter({"app:my_ads_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, MyAdsAdapter myAdsAdapter) {
        recyclerView.setAdapter(myAdsAdapter);
    }


    @BindingAdapter({"app:tags_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, SpecsAdapter tagsAdapter) {
        recyclerView.setAdapter(tagsAdapter);
    }

    @BindingAdapter({"app:tags_adapter2"})
    public static void getOrdersBinding(RecyclerView recyclerView, TagsAdapter tagsAdapter2) {
        recyclerView.setAdapter(tagsAdapter2);
    }


    @BindingAdapter({"app:search_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, SearchAdapter searchAdapter) {
        recyclerView.setAdapter(searchAdapter);
    }


    @Bindable
    public AdsAdapter getAdsAdapter() {
        return this.adsAdapter == null ? this.adsAdapter = new AdsAdapter() : this.adsAdapter;
    }

    public MyAdsAdapter getMyAdsAdapter() {
        return this.myAdsAdapter == null ? this.myAdsAdapter = new MyAdsAdapter() : this.myAdsAdapter;
    }

    @Bindable
    public SpecsAdapter getTagsAdapter() {
        return this.tagsAdapter == null ? this.tagsAdapter = new SpecsAdapter() : this.tagsAdapter;
    }

    @Bindable
    public TagsAdapter getTagsAdapter2() {
        return this.tagsAdapter2 == null ? this.tagsAdapter2 = new TagsAdapter() : this.tagsAdapter2;
    }


    public SearchAdapter getSearchAdapter() {
        return this.searchAdapter == null ? this.searchAdapter = new SearchAdapter() : this.searchAdapter;
    }

    public void filterClick() {
        getClicksMutableLiveData().setValue(Codes.FILTER_SCREEN);
    }

    public void sortClick() {
        getClicksMutableLiveData().setValue(Codes.SORT_CLICK);
    }

    public void onSearchClick() {
        getClicksMutableLiveData().setValue(Codes.SEARCH);
    }

    private void getAds() {
        String link = WebServices.ADS + subCategoriesItem.getId() + "&all_ads=" + subCategoriesItem.getAllAds();

        if(subCategoriesItem.getAllAds().equals("1")){
            link = WebServices.ALL_ADS + subCategoriesItem.getCategoryId() ;
        }



        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {



                try {
                    accessLoadingBar(View.GONE);

                    AdsResponse adsResponse = (AdsResponse) response;


                    if (adsResponse.getData() == null || adsResponse.getData().getAdvertisements().getAdsItems() == null || adsResponse.getData().getAdvertisements().getAdsItems().size() < 1) {
                        getClicksMutableLiveData().setValue(Codes.EMPTY_DATA_2);
                    }

                    adsItems = adsResponse.getData().getAdvertisements().getAdsItems();
                    getAdsAdapter().updateData(adsResponse.getData().getAdvertisements().getAdsItems());
                    ArrayList<ValuesItem> valuesItems = new ArrayList<>();

                    for (int i = 0; i < adsResponse.getData().getSpecifications().size(); i++) {
                        valuesItems.addAll(adsResponse.getData().getSpecifications().get(i).getValuesItems());
                    }
                    getTagsAdapter2().updateData(valuesItems);

                    notifyChange();
                }catch (Exception e){
                    e.getStackTrace();
                }
            }

            @Override
            public void onRequestError(Object error) {
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.EMPTY_DATA_2);
                super.onRequestError(error);
            }

        }).requestJsonObject(Request.Method.GET, link, null, AdsResponse.class);
    }



    public void getSearchByTag(SubCategoriesItem subCategoriesItem , TagsItem tagsItem) {

        this.subCategoriesItem = subCategoriesItem;

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                accessLoadingBar(View.GONE);
                AdsResponse adsResponse = (AdsResponse) response;
                adsItems = adsResponse.getData().getAdvertisements().getAdsItems();
                getAdsAdapter().updateData(adsResponse.getData().getAdvertisements().getAdsItems());
                ArrayList<ValuesItem> valuesItems = new ArrayList<>();

                for (int i = 0; i < adsResponse.getData().getSpecifications().size(); i++) {
                    valuesItems.addAll(adsResponse.getData().getSpecifications().get(i).getValuesItems());
                }
                getTagsAdapter2().updateData(valuesItems);



                //filterTag(tagsItem.getId());

                notifyChange();
            }

            @Override
            public void onRequestError(Object error) {

                super.onRequestError(error);
            }

        }).requestJsonObject(Request.Method.GET, WebServices.ALL_ADS+subCategoriesItem.getCategoryId()+"&tag_id=" + tagsItem.getId() , null, AdsResponse.class);
    }


    public void getSearchAds(SubCategoriesItem subCategoriesItem , String search) {

        this.subCategoriesItem = subCategoriesItem;

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);

                AdsResponse adsResponse = (AdsResponse) response;
                adsItems = adsResponse.getData().getAdvertisements().getAdsItems();
                getAdsAdapter().updateData(adsResponse.getData().getAdvertisements().getAdsItems());
                ArrayList<ValuesItem> valuesItems = new ArrayList<>();

                for (int i = 0; i < adsResponse.getData().getSpecifications().size(); i++) {
                    valuesItems.addAll(adsResponse.getData().getSpecifications().get(i).getValuesItems());
                }
                getTagsAdapter2().updateData(valuesItems);

                notifyChange();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }

        }).requestJsonObject(Request.Method.GET, WebServices.ADS + subCategoriesItem.getId() + "&search=" +search, null, AdsResponse.class);
    }


    public void getOrderBy(String orderBy) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                AdsResponse adsResponse = (AdsResponse) response;
                adsItems = adsResponse.getData().getAdvertisements().getAdsItems();
                getAdsAdapter().updateData(adsResponse.getData().getAdvertisements().getAdsItems());

                ArrayList<ValuesItem> valuesItems = new ArrayList<>();

                for (int i = 0; i < adsResponse.getData().getSpecifications().size(); i++) {
                    valuesItems.addAll(adsResponse.getData().getSpecifications().get(i).getValuesItems());
                }
                getTagsAdapter2().updateData(valuesItems);
                accessLoadingBar(View.GONE);
                notifyChange();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }

        }).requestJsonObject(Request.Method.GET, WebServices.ORDER_BY + subCategoriesItem.getId() + "&all_ads=" + subCategoriesItem.getAllAds() + "&order_by=" + orderBy, null, AdsResponse.class);
    }


    public void getFavourites() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AdsResponse adsResponse = (AdsResponse) response;
                getAdsAdapter().updateData(adsResponse.getData().getAdvertisements().getAdsItems());
                accessLoadingBar(View.GONE);
                notifyChange();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.FAVOURITE_ADS, null, AdsResponse.class);
    }


    public void getMySelectedAds(Advertisements advertisements) {
        total = advertisements.getAdsItems().size() * 200;
        getMyAdsAdapter().updateData(advertisements.getAdsItems());
        notifyChange();
    }

    public void getMyAds() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);

                AdsResponse adsResponse = (AdsResponse) response;
                getMyAdsAdapter().updateData(adsResponse.getData().getAdvertisements().getAdsItems());

                notifyChange();
            }

            @Override
            public void onRequestError(Object error) {

                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.MY_ADS, null, AdsResponse.class);


    }

    public boolean isVisable = false;
    public Advertisements advertisements;

    public void confirmMakeAdSpecialClick() {
        accessLoadingBar(View.VISIBLE);
        ArrayList<Integer> adsItems = new ArrayList<>();
        for (int i = 0; i < getMyAdsAdapter().getAdsItems().size(); i++) {
            if (getMyAdsAdapter().getAdsItems().get(i).isChecked()) {
                adsItems.add(getMyAdsAdapter().getAdsItems().get(i).getId());
            }
        }
        MakeSpecialRequest makeSpecialRequest = new MakeSpecialRequest();
        makeSpecialRequest.setAdsId(adsItems);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.AD_IS_SPECIAL);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.MAKE_SPECIAL , makeSpecialRequest, SearchResponse.class);
    }

    public void makeAdSpecialClick() {

        if( getMyAdsAdapter().getAdsItems()==null|| getMyAdsAdapter().getAdsItems().size()<1){

            setReturnedMessage(ResourcesManager.getString(R.string.msg_at_lease_one_ad));
            getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
        }else {

            if (isVisable) {
                advertisements = new Advertisements();
                ArrayList<AdsItem> adsItems = new ArrayList<>();
                for (int i = 0; i < getMyAdsAdapter().getAdsItems().size(); i++) {
                    if (getMyAdsAdapter().getAdsItems().get(i).isChecked()) {
                        adsItems.add(getMyAdsAdapter().getAdsItems().get(i));
                    }
                }
                advertisements.setAdsItems(adsItems);
                getClicksMutableLiveData().setValue(Codes.MY_SELECTED_ADS);
            }

            isVisable = !isVisable;

            for (int i = 0; i < getMyAdsAdapter().getAdsItems().size(); i++) {
                getMyAdsAdapter().getAdsItems().get(i).setVisiable(isVisable);
            }

            getMyAdsAdapter().notifyDataSetChanged();
            notifyChange();
        }

    }

    public boolean goNow = true;

    public void getSearch(String search) {

        if (goNow) {
            // accessLoadingBar(View.VISIBLE);
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    SearchResponse searchResponse = (SearchResponse) response;
                    getSearchAdapter().updateData(searchResponse.getData());
                    notifyChange();
                    goNow = true;
                }
            }).requestJsonObject(Request.Method.GET, WebServices.SEARCH + search, null, SearchResponse.class);
        }

        goNow = false;
    }




    public void filterTag(int usedTag) {

        List<AdsItem> searchResultItems = new ArrayList<>();

        for (int i = 0; i < adsItems.size(); i++) {

            if (adsItems.get(i).getTag().getId() == usedTag) {
                searchResultItems.add(adsItems.get(i));
            }
        }
        getAdsAdapter().updateData(new ArrayList<>(searchResultItems));

    }

    public void getFilteredData(FilterRequest filterRequest) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AdsResponse adsResponse = (AdsResponse) response;
                adsItems = adsResponse.getData().getAdvertisements().getAdsItems();
                getAdsAdapter().updateData(adsResponse.getData().getAdvertisements().getAdsItems());
                ArrayList<ValuesItem> valuesItems = new ArrayList<>();

                for (int i = 0; i < adsResponse.getData().getSpecifications().size(); i++) {
                    valuesItems.addAll(adsResponse.getData().getSpecifications().get(i).getValuesItems());
                }
                getTagsAdapter2().updateData(valuesItems);
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.FILTER_RESULT, filterRequest, AdsResponse.class);

    }
}
