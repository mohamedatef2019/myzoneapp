package grand.myzone.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.util.Objects;

import grand.myzone.R;
import grand.myzone.ads.response.Advertisements;
import grand.myzone.ads.viewmodel.AdsViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentMySelectedAdsBinding;
import grand.myzone.filter.FilterRequest;
import grand.myzone.home.HomeFragment;


public class MySelectedAdsFragment extends BaseFragment {
    private View rootView;
    private AdsViewModel adsViewModel;
    private FragmentMySelectedAdsBinding fragmentEventsBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentEventsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_selected_ads, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentEventsBinding.getRoot();
        adsViewModel = new AdsViewModel();
        adsViewModel.getMySelectedAds((Advertisements) getArguments().getSerializable(Params.BUNDLE_SELECTED_ADS));
        fragmentEventsBinding.setAdsViewModel(adsViewModel);
    }


    private void liveDataListeners() {
        adsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if(result == Codes.AD_IS_SPECIAL){
                adIsSpecialDialog();
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        adsViewModel.getFilteredData((FilterRequest) data.getSerializableExtra(Params.INTENT_FILTER));
    }


    private void adIsSpecialDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_ad_special, null);
         dialogView.findViewById(R.id.btn_add_ad).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 MovementManager.startMainActivity(requireActivity(), HomeFragment.class.getName());
             }
         });
        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }
}