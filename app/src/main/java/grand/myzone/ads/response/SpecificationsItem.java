package grand.myzone.ads.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.myzone.filter.response.ValuesItem;

public class SpecificationsItem{

	@SerializedName("values")
	private List<ValuesItem> values;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public void setValues(List<ValuesItem> values){
		this.values = values;
	}

	public List<ValuesItem> getValues(){
		return values;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}