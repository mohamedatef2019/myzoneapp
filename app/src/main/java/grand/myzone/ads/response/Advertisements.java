package grand.myzone.ads.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Advertisements implements Serializable {

	@SerializedName("data")
	private List<AdsItem> adsItems;

	@SerializedName("meta")
	private Meta meta;

	@SerializedName("links")
	private Links links;

	public void setAdsItems(List<AdsItem> adsItems) {
		this.adsItems = adsItems;
	}

	public List<AdsItem> getAdsItems() {
		return adsItems;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	public void setLinks(Links links){
		this.links = links;
	}

	public Links getLinks(){
		return links;
	}
}