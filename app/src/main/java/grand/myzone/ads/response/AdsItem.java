package grand.myzone.ads.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.myzone.comments.response.User;
import grand.myzone.subcategories.response.TagsItem;

public class AdsItem implements Serializable {

	@Expose
	@SerializedName("special")
	private int special;

	@Expose
	@SerializedName("image")
	private String image;

	@Expose
	@SerializedName("is_favorite")
	private boolean isFavorite;

	@Expose
	@SerializedName("price")
	private String price;





	@Expose
	@SerializedName("user")
	private User user;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	@SerializedName("count_images")
	private String countImages;

	@Expose
	@SerializedName("ended_at")
	private String endedAt;

	@Expose
	@SerializedName("tag")
	private TagsItem tag;

	@Expose
	@SerializedName("title")
	private String title;

	@Expose
	private boolean checked=false;



	@Expose
	@SerializedName("visible")
	private int visible = 0;

	@Expose
	private boolean visiable=false;

	public void setSpecial(int special){
		this.special = special;
	}

	public int getSpecial(){
		return special;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setIsFavorite(boolean isFavorite){
		this.isFavorite = isFavorite;
	}

	public boolean isIsFavorite(){
		return isFavorite;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTag(TagsItem tag){
		this.tag = tag;
	}

	public TagsItem getTag(){
		return tag;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isChecked() {
		return checked;
	}

	public boolean isVisiable() {
		return visiable;
	}

	public void setVisiable(boolean visiable) {
		this.visiable = visiable;
	}


	public void setCountImages(String countImages) {
		this.countImages = countImages;
	}

	public void setEndedAt(String endedAt) {
		this.endedAt = endedAt;
	}

	public String getCountImages() {
		return countImages;
	}

	public String getEndedAt() {
		return endedAt;
	}


	public void setFavorite(boolean favorite) {
		isFavorite = favorite;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getVisible() {
		return visible;
	}

	public void setVisible(int visible) {
		this.visible = visible;
	}

	public User getUser() {
		return user;
	}
}