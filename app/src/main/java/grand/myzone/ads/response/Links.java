package grand.myzone.ads.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Links implements Serializable {

	@SerializedName("next")
	private String next;

	@SerializedName("last")
	private String last;

	@SerializedName("prev")
	private String prev;

	@SerializedName("first")
	private String first;

	public void setNext(String next){
		this.next = next;
	}

	public String getNext(){
		return next;
	}

	public void setLast(String last){
		this.last = last;
	}

	public String getLast(){
		return last;
	}

	public void setPrev(String prev){
		this.prev = prev;
	}

	public String getPrev(){
		return prev;
	}

	public void setFirst(String first){
		this.first = first;
	}

	public String getFirst(){
		return first;
	}
}