package grand.myzone.ads.response;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.myzone.subcategories.response.TagsItem;

public class Data{

	@SerializedName("advertisements")
	private Advertisements advertisements;

	@Expose
	@SerializedName("specifications")
	private List<TagsItem> specifications;

	public void setAdvertisements(Advertisements advertisements){
		this.advertisements = advertisements;
	}

	public Advertisements getAdvertisements(){
		return advertisements;
	}

	public void setSpecifications(List<TagsItem> specifications){
		this.specifications = specifications;
	}

	public List<TagsItem> getSpecifications(){
		return specifications;
	}
}