package grand.myzone.ads.response.reasons;

import com.google.gson.annotations.SerializedName;

public class ReportItem {

	@SerializedName("reason")
	private String reason;

	@SerializedName("id")
	private int id;

	public void setReason(String reason){
		this.reason = reason;
	}

	public String getReason(){
		return reason;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}