package grand.myzone.ads.response.searchresult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchItem {

	@SerializedName("sub_category_id")
	private int subCategoryId;

	@Expose
	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	public void setSubCategoryId(int subCategoryId){
		this.subCategoryId = subCategoryId;
	}

	public int getSubCategoryId(){
		return subCategoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}
}