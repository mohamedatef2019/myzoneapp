package grand.myzone.ads.response.reasons;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReportsResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<ReportItem> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<ReportItem> data){
		this.data = data;
	}

	public List<ReportItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}