package grand.myzone.ads.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdsResponse{

	@SerializedName("code")
	private int code;

	@Expose
	@SerializedName("data")
	private Data data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}