package grand.myzone.notifications.viewmodel;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.events.response.EventsResponse;
import grand.myzone.notifications.response.NotificationsResponse;
import grand.myzone.notifications.view.NotificationsAdapter;


public class NotificationsViewModel extends BaseViewModel {

    private NotificationsAdapter notificationsAdapter;

    public NotificationsViewModel( ) {
        getNotifications();
    }


    @BindingAdapter({"app:notifications_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, NotificationsAdapter notificationsAdapter) {
        recyclerView.setAdapter(notificationsAdapter);
    }



    @Bindable
    public NotificationsAdapter getNotificationsAdapter() {
        return this.notificationsAdapter == null ? this.notificationsAdapter = new NotificationsAdapter() : this.notificationsAdapter;
    }

    private void getNotifications( ) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                NotificationsResponse eventsResponse = (NotificationsResponse)response;

                getNotificationsAdapter().updateData(eventsResponse.getData().getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.NOTIFICATIONS, null, NotificationsResponse.class);
    }
}
