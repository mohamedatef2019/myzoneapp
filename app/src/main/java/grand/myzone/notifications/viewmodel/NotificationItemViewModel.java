package grand.myzone.notifications.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import com.makeramen.roundedimageview.RoundedImageView;
import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.notifications.response.NotificationItem;


public class NotificationItemViewModel extends BaseItemViewModel {

    private NotificationItem notificationItem;

    public NotificationItemViewModel(NotificationItem notificationItem) {
        this.notificationItem = notificationItem;

    }

    @Bindable
    public NotificationItem getNotificationItem() {
        return notificationItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }else {
            view.setImageDrawable(ResourcesManager.getDrawable(R.drawable.img_logo));
        }
    }
}
