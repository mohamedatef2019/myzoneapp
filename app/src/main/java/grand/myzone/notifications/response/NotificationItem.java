package grand.myzone.notifications.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.myzone.comments.response.User;

public class NotificationItem {

	@SerializedName("follower_id")
	private String followerId;

	@SerializedName("user_id")
	private String userId;

	@Expose
	@SerializedName("advertisement_id")
	private int advertisementId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("message_id")
	private String messageId;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("image")
	private String image;

	@Expose
	@SerializedName("user")
	private User user;

	@SerializedName("comment_id")
	private String commentId;

	public void setFollowerId(String followerId){
		this.followerId = followerId;
	}

	public String getFollowerId(){
		return followerId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setAdvertisementId(int advertisementId){
		this.advertisementId = advertisementId;
	}

	public int getAdvertisementId(){
		return advertisementId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setMessageId(String messageId){
		this.messageId = messageId;
	}

	public String getMessageId(){
		return messageId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setCommentId(String commentId){
		this.commentId = commentId;
	}

	public String getCommentId(){
		return commentId;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}