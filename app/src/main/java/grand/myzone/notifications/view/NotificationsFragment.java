package grand.myzone.notifications.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.myzone.R;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentEventsBinding;
import grand.myzone.databinding.FragmentNotificaitonsBinding;
import grand.myzone.notifications.viewmodel.NotificationsViewModel;

public class NotificationsFragment extends BaseFragment {
    private View rootView;
    private NotificationsViewModel notificationsViewModel;
    private FragmentNotificaitonsBinding fragmentNotificaitonsBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentNotificaitonsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notificaitons, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentNotificaitonsBinding.getRoot();
        notificationsViewModel = new NotificationsViewModel();
        fragmentNotificaitonsBinding.setNotificationsViewModel(notificationsViewModel);
    }


    private void liveDataListeners() {
        notificationsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });
    }




}