
package grand.myzone.notifications.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.chatmessages.view.ChatMessagesFragment;
import grand.myzone.comments.view.CommentsFragment;
import grand.myzone.databinding.ItemEventBinding;
import grand.myzone.databinding.ItemNotificationBinding;
import grand.myzone.events.response.EventItem;
import grand.myzone.notifications.response.NotificationItem;
import grand.myzone.notifications.viewmodel.NotificationItemViewModel;
import grand.myzone.userdetails.views.UserDetailsFragment;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.CategoriesViewHolder> {

    private List<NotificationItem> notificationItems;


    public NotificationsAdapter() {
        this.notificationItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        NotificationItem notificationItem = notificationItems.get(position);
        NotificationItemViewModel orderItemViewModel = new NotificationItemViewModel(notificationItem);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));


        holder.itemView.setOnClickListener(view -> {

            if (notificationItem.getType().equals("follow")) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ANY_ID, notificationItem.getUser().getId());
                MovementManager.startActivity(view.getContext(), UserDetailsFragment.class.getName(), bundle,  ResourcesManager.getString(R.string.user_details));
            } else if (notificationItem.getType().equals("comment")) {
                Bundle bundle = new Bundle();
                AdsItem adsItem = new AdsItem();
                adsItem.setId(notificationItem.getAdvertisementId());
                bundle.putSerializable(Params.BUNDLE_ITEM_AD, adsItem);
                MovementManager.startActivity(view.getContext(), CommentsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_comments));
            } else if (notificationItem.getType().equals("message")) {
                Bundle bundle = new Bundle();
                bundle.putInt(Params.USER_ID,notificationItem.getUser().getId());
                MovementManager.startActivity(view.getContext(), ChatMessagesFragment.class.getName(),bundle, ResourcesManager.getString(R.string.chat));
            } else if (notificationItem.getType().equals("admin")) {

            }

        });

        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.notificationItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<NotificationItem> data) {

        this.notificationItems.clear();

        this.notificationItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemNotificationBinding itemNotificationBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemNotificationBinding == null) {
                itemNotificationBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemNotificationBinding != null) {
                itemNotificationBinding.unbind();
            }
        }

        void setViewModel(NotificationItemViewModel notificationItemViewModel) {
            if (itemNotificationBinding != null) {
                itemNotificationBinding.setNotificationItemViewModel(notificationItemViewModel);
            }
        }


    }


}
