package grand.myzone.chatmessages.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatMessageItem {


    @Expose
    @SerializedName("image")
    private String image;

    @Expose
    @SerializedName("unread_count")
    private int unreadCount;

    @Expose
    @SerializedName("created_at")
    private String createdAt;

    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("user")
    private User user;

    @Expose
    @SerializedName("time")
    private String time;

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }
}