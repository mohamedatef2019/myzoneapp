package grand.myzone.chatmessages.view;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.Objects;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.filesutils.FileOperations;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.utils.Validate;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.chatmessages.viewmodel.ChatMessagesViewModel;
import grand.myzone.databinding.FragmentChatBinding;


public class ChatMessagesFragment extends BaseFragment {
    View rootView;
    ChatMessagesViewModel chatMessagesViewModel;
    FragmentChatBinding fragmentChatMessagesBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentChatMessagesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentChatMessagesBinding.getRoot();
        chatMessagesViewModel = new ChatMessagesViewModel(getArguments().getInt(Params.USER_ID));
        fragmentChatMessagesBinding.setChatViewModel(chatMessagesViewModel);
    }


    private void liveDataListeners() {
        chatMessagesViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), o -> {
            int result = (int) o;
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if(result == Codes.ON_SELECT_IMAGE_CLICK){

                if (Validate.isPermissionDone(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                        && Validate.isPermissionDone(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && Validate.isPermissionDone(context, Manifest.permission.CAMERA)) {

                    CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 199);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();
                VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), resultUri, "image", Codes.FILE_TYPE_IMAGE);
                chatMessagesViewModel.getVolleyFileObjects().add(volleyFileObject);
                chatMessagesViewModel.sendMessageImage();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

            }
        }
    }


    @Override
    public void onResume() {
        Objects.requireNonNull(requireActivity()).registerReceiver(onBroadcast,
                new IntentFilter("messageIntent"));
        super.onResume();
    }


    private BroadcastReceiver onBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, final Intent i) {
            try {
                chatMessagesViewModel.getChatMessages(false);
            } catch (Exception e) {
                e.getStackTrace();
            }
        }
    };

}