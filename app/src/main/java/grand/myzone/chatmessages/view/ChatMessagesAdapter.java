
package grand.myzone.chatmessages.view;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.entities.ZColor;

import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.chatmessages.response.ChatMessageItem;
import grand.myzone.chatmessages.viewmodel.ChatMessageItemViewModel;
import grand.myzone.databinding.ItemChatBinding;


public class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter.CategoriesViewHolder> {

    private List<ChatMessageItem> orderItems;
    private int userId;

    public ChatMessagesAdapter() {
        userId= UserPreferenceHelper.getUserDetails().getId();
        this.orderItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ChatMessageItem dataModel = orderItems.get(position);
        ChatMessageItemViewModel orderItemViewModel = new ChatMessageItemViewModel(dataModel,userId);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));

        holder.itemChatBinding.ivChatUser.setOnClickListener(v -> {
            ArrayList<String>images = new ArrayList<>();
                images.add(orderItems.get(position).getImage());
            ZGallery.with((Activity) v.getContext(), images)
                    .setToolbarTitleColor(ZColor.WHITE)
                    .setGalleryBackgroundColor(ZColor.WHITE)
                    .setToolbarColorResId(R.color.colorPrimary)
                    .setTitle(ResourcesManager.getString(R.string.label_gallery))
                    .show();
        });

        holder.itemChatBinding.ivChatUser2.setOnClickListener(v -> {
            ArrayList<String>images = new ArrayList<>();
            images.add(orderItems.get(position).getImage());
            ZGallery.with((Activity) v.getContext(), images)
                    .setToolbarTitleColor(ZColor.WHITE)
                    .setGalleryBackgroundColor(ZColor.WHITE)
                    .setToolbarColorResId(R.color.colorPrimary)
                    .setTitle(ResourcesManager.getString(R.string.label_gallery))
                    .show();
        });
        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.orderItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ChatMessageItem> data) {
        Log.e("RESSS","DATA"+data.size());
        this.orderItems.clear();

        this.orderItems.addAll(data);
        notifyDataSetChanged();
    }


    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemChatBinding itemChatBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemChatBinding == null) {
                itemChatBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemChatBinding != null) {
                itemChatBinding.unbind();
            }
        }

        void setViewModel(ChatMessageItemViewModel chatMessageItemViewModel) {
            if (itemChatBinding != null) {
                itemChatBinding.setChatMessageItemViewModel(chatMessageItemViewModel);
            }
        }


    }


}
