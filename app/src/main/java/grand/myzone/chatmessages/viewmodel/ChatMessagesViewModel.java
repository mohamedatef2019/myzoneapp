package grand.myzone.chatmessages.viewmodel;

import android.util.Log;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;

import java.util.ArrayList;

import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.chatmessages.request.ChatMessagesRequest;
import grand.myzone.chatmessages.response.ChatMessagesResponse;
import grand.myzone.chatmessages.view.ChatMessagesAdapter;

public class ChatMessagesViewModel extends BaseViewModel {

    private ChatMessagesAdapter chatMessagesAdapter;
    private ChatMessagesRequest chatMessagesRequest;

    int messagesId;

    public ChatMessagesViewModel(int userId) {
        volleyFileObjects = new ArrayList<>();
        this.messagesId=userId;
        chatMessagesRequest = new ChatMessagesRequest();
        chatMessagesRequest.setUserId(userId);
        getChatMessages(true);

    }


    @BindingAdapter({"app:messages_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, ChatMessagesAdapter chatMessagesAdapter) {
        recyclerView.setAdapter(chatMessagesAdapter);
        try {
            recyclerView.smoothScrollToPosition(chatMessagesAdapter.getItemCount() - 1);
        }catch (Exception e){
            e.getStackTrace();
        }
    }


    @Bindable
    public ChatMessagesAdapter getChatMessagesAdapter() {
        return this.chatMessagesAdapter == null ? this.chatMessagesAdapter = new ChatMessagesAdapter() : this.chatMessagesAdapter;
    }




    public ChatMessagesRequest getChatMessagesRequest() {
        return chatMessagesRequest;
    }

    public void getChatMessages(boolean showLoading) {

        if(showLoading) {
            accessLoadingBar(View.VISIBLE);
        }
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {


                ChatMessagesResponse chatMessagesResponse = (ChatMessagesResponse) response;

                Log.e("RESSS","DATA"+chatMessagesResponse);

                try {
                    getChatMessagesAdapter().updateData(chatMessagesResponse.getData().getData());
                }catch (Exception e){
                    Log.e("RESSS","DATA"+e.getStackTrace()+"  "+e.getMessage());
                }
                notifyChange();
                accessLoadingBar(View.GONE);

            }
        }).requestJsonObject(Request.Method.GET, WebServices.CHAT_MESSAGES+messagesId,
                null,
                ChatMessagesResponse.class);
    }

    public void onSelectImageClick(){
        getClicksMutableLiveData().setValue(Codes.ON_SELECT_IMAGE_CLICK);
    }

    public void sendMessage() {
        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                getChatMessages(false);
                chatMessagesRequest.setMessage(null);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.SEND_MESSAGE, chatMessagesRequest, DefaultResponse.class);
    }

    public void sendMessageImage() {
        accessLoadingBar(View.VISIBLE);
        chatMessagesRequest.setMessage("Image");
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                getChatMessages(false);
                chatMessagesRequest.setMessage(null);
            }
        }).multiPartConnect(WebServices.SEND_MESSAGE, chatMessagesRequest,getVolleyFileObjects(), DefaultResponse.class);
    }


    private ArrayList<VolleyFileObject> volleyFileObjects;


    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }

}
