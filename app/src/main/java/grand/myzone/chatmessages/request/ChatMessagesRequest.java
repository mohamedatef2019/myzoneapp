package grand.myzone.chatmessages.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ChatMessagesRequest {

	@SerializedName("message")
	private String message;

	@Expose
	@SerializedName("user_id")
	private int userId;


	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserId() {
		return userId;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
 	public String toString(){
		return
			"GetMessagesResponse{" +
			"userId = '" + userId + '\'' +
			",message = '" + message + '\'' +
			"}";
		}
}