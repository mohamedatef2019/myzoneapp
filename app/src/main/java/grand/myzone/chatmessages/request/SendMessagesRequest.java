package grand.myzone.chatmessages.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SendMessagesRequest {

    @SerializedName("user_id")
    private int userId;

    @SerializedName("worker_id")
    private int workerId;

    @SerializedName("message")
    private String body;

    @SerializedName("send")
    private String send;

    @Expose
    @SerializedName("order_id")
    private int orderId;


    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setSend(String send) {
        this.send = send;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSend() {
        return send;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return
                "GetMessagesResponse{" +
                        "user_id = '" + userId + '\'' +
                        ",worker_id = '" + workerId + '\'' +
                        "}";
    }
}