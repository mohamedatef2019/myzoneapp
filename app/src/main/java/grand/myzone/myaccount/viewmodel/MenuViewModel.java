package grand.myzone.myaccount.viewmodel;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.ArrayList;

import grand.myzone.R;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.events.response.EventsResponse;
import grand.myzone.myaccount.MenuItem;
import grand.myzone.myaccount.view.MenuAdapter;


public class MenuViewModel extends BaseViewModel {

    private MenuAdapter menuAdapter;

    public MenuViewModel( ) {

    }


    @BindingAdapter({"app:menu_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, MenuAdapter eventsAdapter) {
        recyclerView.setAdapter(eventsAdapter);
    }



    @Bindable
    public MenuAdapter getMenuAdapter() {
        return this.menuAdapter == null ? this.menuAdapter = new MenuAdapter() : this.menuAdapter;
    }

    public void setupProfileMenu() {


        ArrayList<MenuItem> menuItems = new ArrayList<>();

        menuItems.add(new MenuItem(1, ResourcesManager.getString(R.string.my_profile),R.drawable.ic_my_profile));
        menuItems.add(new MenuItem(2, ResourcesManager.getString(R.string.favourites),R.drawable.ic_favorite));
        menuItems.add(new MenuItem(3, ResourcesManager.getString(R.string.followers),R.drawable.ic_following));
        menuItems.add(new MenuItem(4, ResourcesManager.getString(R.string.my_ads),R.drawable.ic_my_ads));
        menuItems.add(new MenuItem(5, ResourcesManager.getString(R.string.log_out),R.drawable.ic_log_out));

        getMenuAdapter().updateData(menuItems);
        notifyChange();

    }

    public void setupSettingMenu() {


        ArrayList<MenuItem> menuItems = new ArrayList<>();

        menuItems.add(new MenuItem(0, ResourcesManager.getString(R.string.change_language),R.drawable.ic_language));
        menuItems.add(new MenuItem(1, ResourcesManager.getString(R.string.about_app),R.drawable.ic_menu_about));
        menuItems.add(new MenuItem(2, ResourcesManager.getString(R.string.call_us),R.drawable.ic_menu_call));
        menuItems.add(new MenuItem(3, ResourcesManager.getString(R.string.help),R.drawable.ic_menu_help));
        menuItems.add(new MenuItem(4, ResourcesManager.getString(R.string.terms_conditions),R.drawable.ic_menu_shield));
        menuItems.add(new MenuItem(5, ResourcesManager.getString(R.string.problems_suggestions),R.drawable.ic_menu_light));
        menuItems.add(new MenuItem(6, ResourcesManager.getString(R.string.privacy),R.drawable.ic_menu_checklist));
        menuItems.add(new MenuItem(7, ResourcesManager.getString(R.string.share_app),R.drawable.ic_menu_share));
        menuItems.add(new MenuItem(8, ResourcesManager.getString(R.string.rate_app),R.drawable.ic_rate));

        getMenuAdapter().updateData(menuItems);
        notifyChange();

    }
}
