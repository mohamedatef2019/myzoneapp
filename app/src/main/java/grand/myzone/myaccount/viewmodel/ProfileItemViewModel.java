package grand.myzone.myaccount.viewmodel;


import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.myaccount.MenuItem;


public class ProfileItemViewModel extends BaseItemViewModel {

    private MenuItem menuItem;



    public ProfileItemViewModel(MenuItem menuItem) {
        this.menuItem = menuItem;

    }

    @Bindable
    public MenuItem getMenuItem() {
        return menuItem;
    }


    @BindingAdapter({"app:imageSrc"})
    public static void setItemImage(ImageView view, int s){

       view.setImageResource(s);

    }
}
