package grand.myzone.myaccount;


public class MenuItem {

	 private int id,icon;
	 private String title;


	 public MenuItem(int id, String title, int icon){
	 	this.id = id;
	 	this.title=title;
	 	this.icon=icon;
	 }

	public void setId(int id) {
		this.id = id;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public int getIcon() {
		return icon;
	}

}