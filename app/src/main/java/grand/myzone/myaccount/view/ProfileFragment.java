package grand.myzone.myaccount.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import java.util.Objects;

import grand.myzone.R;
import grand.myzone.about.AboutFragment;
import grand.myzone.ads.MyAdsFragment;
import grand.myzone.ads.view.FavouritesFragment;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.activities.SplashScreenActivity;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentMenuBinding;
import grand.myzone.followers.view.FragmentMainFollowers;
import grand.myzone.help.HelpFragment;
import grand.myzone.myaccount.MenuItem;
import grand.myzone.myaccount.viewmodel.MenuViewModel;
import grand.myzone.privacy.PrivacyFragment;
import grand.myzone.register.view.MyProfileFragment;
import grand.myzone.sendmessage.view.SendMessageFragment;
import grand.myzone.sendmessage.view.SendSugesstionMessageFragment;
import grand.myzone.terms.TermFragments;
import grand.myzone.welcome.IntroScreenActivity;

public class ProfileFragment extends BaseFragment {
    private View rootView;
    private MenuViewModel notificationsViewModel;
    private FragmentMenuBinding fragmentMenuBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMenuBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentMenuBinding.getRoot();
        notificationsViewModel = new MenuViewModel();
        notificationsViewModel.setupProfileMenu();
        fragmentMenuBinding.setMenuViewModel(notificationsViewModel);
    }


    private void liveDataListeners() {
        notificationsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        notificationsViewModel.getMenuAdapter().getItemsOperationsLiveListener().observe(getViewLifecycleOwner(), menuItem -> {
            if(menuItem.getId()==1){
             MovementManager.startActivity(requireActivity(), MyProfileFragment.class.getName(), ResourcesManager.getString(R.string.my_profile));
            }else if(menuItem.getId()==2){
                MovementManager.startActivity(requireActivity(), FavouritesFragment.class.getName(), ResourcesManager.getString(R.string.favourites));
            }else if(menuItem.getId()==3){
                MovementManager.startActivity(requireActivity(), FragmentMainFollowers.class.getName(), ResourcesManager.getString(R.string.followers));
            }else if(menuItem.getId()==4){
                MovementManager.startActivity(requireActivity(), MyAdsFragment.class.getName(), ResourcesManager.getString(R.string.my_ads));
            }else if(menuItem.getId()==5){
                logOutDialog();
//                UserPreferenceHelper.clearUserDetails();
//                requireActivity().finishAffinity();
//                MovementManager.startActivity(requireActivity(), IntroScreenActivity.class);
            }
        });
    }

    private void logOutDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_log_out, null);
        Button addAd = dialogView.findViewById(R.id.btn_log_out);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);

        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();

        addAd.setOnClickListener(v -> {

            UserPreferenceHelper.clearUserDetails();
            requireActivity().finishAffinity();
            MovementManager.startActivity(requireActivity(), IntroScreenActivity.class);

            alertDialog.cancel();
            alertDialog.dismiss();
        });

        cancel.setOnClickListener(v -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });

        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
    }




}