
package grand.myzone.myaccount.view;


import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.databinding.ItemMenuBinding;
import grand.myzone.myaccount.MenuItem;
import grand.myzone.myaccount.viewmodel.ProfileItemViewModel;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.CategoriesViewHolder> {

    private List<MenuItem> menuItems;
    private MutableLiveData<MenuItem> itemsOperationsLiveListener;

    public MenuAdapter() {
        itemsOperationsLiveListener = new MutableLiveData<>();
        this.menuItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        MenuItem eventItem = menuItems.get(position);
        ProfileItemViewModel orderItemViewModel = new ProfileItemViewModel(eventItem);

        holder.itemView.setOnClickListener(v -> itemsOperationsLiveListener.setValue(menuItems.get(position)));


        holder.setViewModel(orderItemViewModel);
    }

    public MutableLiveData<MenuItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener;
    }

    @Override
    public int getItemCount() {
        return this.menuItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<MenuItem> data) {
        this.menuItems.clear();
        this.menuItems.addAll(data);
        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemMenuBinding itemEventBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemEventBinding == null) {
                itemEventBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemEventBinding != null) {
                itemEventBinding.unbind();
            }
        }

        void setViewModel(ProfileItemViewModel notificationItemViewModel) {
            if (itemEventBinding != null) {
                itemEventBinding.setMenItemViewModel(notificationItemViewModel);
            }
        }


    }


}
