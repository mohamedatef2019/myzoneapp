package grand.myzone.myaccount.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import java.util.Objects;

import grand.myzone.R;
import grand.myzone.about.AboutFragment;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.activities.SplashScreenActivity;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentMenuBinding;
import grand.myzone.help.HelpFragment;
import grand.myzone.myaccount.MenuItem;
import grand.myzone.myaccount.viewmodel.MenuViewModel;
import grand.myzone.privacy.PrivacyFragment;
import grand.myzone.sendmessage.view.SendMessageFragment;
import grand.myzone.sendmessage.view.SendSugesstionMessageFragment;
import grand.myzone.terms.TermFragments;
import grand.myzone.welcome.IntroScreenActivity;

public class SettingsFragment extends BaseFragment {
    private View rootView;
    private MenuViewModel notificationsViewModel;
    private FragmentMenuBinding fragmentMenuBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMenuBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentMenuBinding.getRoot();
        notificationsViewModel = new MenuViewModel();
        notificationsViewModel.setupSettingMenu();
        fragmentMenuBinding.setMenuViewModel(notificationsViewModel);
    }


    private void liveDataListeners() {
        notificationsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        notificationsViewModel.getMenuAdapter().getItemsOperationsLiveListener().observe(getViewLifecycleOwner(), new Observer<MenuItem>() {
            @Override
            public void onChanged(MenuItem menuItem) {

                if(menuItem.getId()==0){

                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(getActivity());
                    } else {
                        builder = new AlertDialog.Builder(getActivity());
                    }
                    builder.setTitle(getResources().getString(R.string.change_language))

                            .setMessage(getResources().getString(R.string.choose_change_language))

                            .setPositiveButton(getResources().getString(R.string.arabic), (dialog, which) -> {
                                SplashScreenActivity.changeLanguage(context, "ar");
                                getActivity().finishAffinity();
                                startActivity(new Intent(getActivity(), IntroScreenActivity.class));
                            })
                            .setNegativeButton(getResources().getString(R.string.english), (dialog, which) -> {
                                SplashScreenActivity.changeLanguage(context, "en");
                                getActivity().finishAffinity();
                                startActivity(new Intent(getActivity(), IntroScreenActivity.class));
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                else if(menuItem.getId()==1){
                    //MovementManager.startActivity(requireActivity(), MyProfileFragment.class.getName(), ResourcesManager.getString(R.string.about_app));
                  MovementManager.startActivity(requireActivity(), AboutFragment.class.getName(), ResourcesManager.getString(R.string.about_app));
                }else if(menuItem.getId()==2){
                    MovementManager.startActivity(requireActivity(), SendMessageFragment.class.getName(), ResourcesManager.getString(R.string.call_us));
                }else if(menuItem.getId()==3){
                    MovementManager.startActivity(requireActivity(), HelpFragment.class.getName(), ResourcesManager.getString(R.string.help));
                }else if(menuItem.getId()==4){
                    MovementManager.startActivity(requireActivity(), TermFragments.class.getName(), ResourcesManager.getString(R.string.terms_conditions));
                }else if(menuItem.getId()==5){
                    MovementManager.startActivity(requireActivity(), SendSugesstionMessageFragment.class.getName(), ResourcesManager.getString(R.string.problems_suggestions));
                }else if(menuItem.getId()==6){
                    MovementManager.startActivity(requireActivity(), PrivacyFragment.class.getName(), ResourcesManager.getString(R.string.privacy));
                }else if(menuItem.getId()==7){
                    MovementManager.shareContent(context,"");
                }else  if(menuItem.getId()==8){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + requireActivity().getPackageName())));

                }
            }
        });
    }




}