package grand.myzone.fortesting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import grand.myzone.R;
import grand.myzone.base.volleyutils.ConnectionHelper;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private List<String> mData;
    private LayoutInflater mInflater;


    // data is passed into the constructor
    NotificationAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_ad, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        if(position%2==0){
            ConnectionHelper.loadImage(holder.image,"https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-bmw-x6-mmp-1-1605135898.jpg");

        }else {
            ConnectionHelper.loadImage(holder.image,"https://www.autoguide.com/blog/wp-content/gallery/mercedes-g-class-vs-jeep-wrangler-comparison/Mercedes-G-Class-vs-Jeep-Wrangler-25.jpg");

        }




    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        ViewHolder(View itemView) {
            super(itemView);

            image= itemView.findViewById(R.id.iv_chat_user);;

        }


    }


}