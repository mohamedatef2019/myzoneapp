package grand.myzone.fortesting;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;

public class ChatterActivity extends AppCompatActivity {

    RecyclerView chatList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notificaitons);
        chatList = findViewById(R.id.rv_chat_list);

        chatList.setLayoutManager(new LinearLayoutManager(this));
        List<String> data = new ArrayList<>();
        for(int i=0; i<10; i++){
            data.add("Mohamed Atef");
        }

        NotificationAdapter chatterAdapter = new NotificationAdapter(this,data);
        chatList.setAdapter(chatterAdapter);
    }
}