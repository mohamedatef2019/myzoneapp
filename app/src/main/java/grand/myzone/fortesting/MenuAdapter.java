package grand.myzone.fortesting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.myaccount.MenuItem;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private List<MenuItem> mData;
    private LayoutInflater mInflater;


    // data is passed into the constructor
    MenuAdapter(Context context, List<MenuItem> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            holder.icon.setImageDrawable(ResourcesManager.getDrawable(mData.get(position).getIcon()));
            holder.title.setText(mData.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView title;


        ViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.iv_menu_icon);
            title = itemView.findViewById(R.id.tv_menu_title);
        }


    }


}