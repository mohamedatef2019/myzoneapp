package grand.myzone.fortesting;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.myaccount.MenuItem;

public class MenuActivity extends AppCompatActivity {

    RecyclerView chatList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_menu);
        chatList = findViewById(R.id.rv_chat_list);

        ArrayList<MenuItem> menuItems = new ArrayList<>();
//        menuItems.add(new MenuItem(1, ResourcesManager.getString(R.string.about_app),R.drawable.ic_menu_about));
//        menuItems.add(new MenuItem(2, ResourcesManager.getString(R.string.call_us),R.drawable.ic_menu_call));
//        menuItems.add(new MenuItem(3, ResourcesManager.getString(R.string.help),R.drawable.ic_menu_help));
//        menuItems.add(new MenuItem(4, ResourcesManager.getString(R.string.terms_conditions),R.drawable.ic_menu_shield));
//        menuItems.add(new MenuItem(5, ResourcesManager.getString(R.string.problems_suggestions),R.drawable.ic_menu_light));
//        menuItems.add(new MenuItem(6, ResourcesManager.getString(R.string.privacy),R.drawable.ic_menu_checklist));
//        menuItems.add(new MenuItem(7, ResourcesManager.getString(R.string.share_app),R.drawable.ic_menu_share));


        menuItems.add(new MenuItem(1, ResourcesManager.getString(R.string.my_profile),R.drawable.ic_menu_about));
        menuItems.add(new MenuItem(2, ResourcesManager.getString(R.string.favourites),R.drawable.ic_menu_call));
        menuItems.add(new MenuItem(3, ResourcesManager.getString(R.string.followers),R.drawable.ic_menu_help));
        menuItems.add(new MenuItem(4, ResourcesManager.getString(R.string.my_ads),R.drawable.ic_menu_shield));
        menuItems.add(new MenuItem(5, ResourcesManager.getString(R.string.log_out),R.drawable.ic_menu_light));



        chatList.setLayoutManager(new LinearLayoutManager(this));
        MenuAdapter menuAdapter = new MenuAdapter(this,menuItems);
        chatList.setAdapter(menuAdapter);

    }
}