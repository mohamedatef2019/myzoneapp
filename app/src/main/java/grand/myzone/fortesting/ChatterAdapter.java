package grand.myzone.fortesting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import grand.myzone.R;
import grand.myzone.base.volleyutils.ConnectionHelper;

public class ChatterAdapter extends RecyclerView.Adapter<ChatterAdapter.ViewHolder> {

    private List<String> mData;
    private LayoutInflater mInflater;


    // data is passed into the constructor
    ChatterAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_chat, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        if (position % 2 == 0) {
            holder.left.setVisibility(View.VISIBLE);
            holder.right.setVisibility(View.GONE);

        } else {
            holder.right.setVisibility(View.VISIBLE);
            holder.left.setVisibility(View.GONE);
        }


        if(position==5){
            holder.tv_chat_message.setVisibility(View.GONE);
            holder.chatImage1.setVisibility(View.VISIBLE);
        }else {

            holder.tv_chat_message.setVisibility(View.VISIBLE);
            holder.chatImage1.setVisibility(View.GONE);
        }


        ConnectionHelper.loadImage(holder.chatImage1,"https://i1.wp.com/whatsappimages.in/wp-content/uploads/2020/07/Cute-Baby-Boy-Whatsapp-Images-5-1-1024x816.jpg");
        ConnectionHelper.loadImage(holder.chatImage2,"https://dorar.at/imup2/2014-04/nice-wallpapers-desktop-84.jpg");

        if(position==2){
            holder.tv_chat_message2.setVisibility(View.GONE);
            holder.chatImage2.setVisibility(View.VISIBLE);
        }else {

            holder.tv_chat_message2.setVisibility(View.VISIBLE);
            holder.chatImage2.setVisibility(View.GONE);
        }



    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout left, right;
        TextView tv_chat_message,tv_chat_message2;
        ImageView chatImage1,chatImage2;

        ViewHolder(View itemView) {
            super(itemView);
            left = itemView.findViewById(R.id.rl_chat_left);
            right = itemView.findViewById(R.id.rl_chat_right);
            chatImage1 = itemView.findViewById(R.id.iv_chat_user);
            chatImage2 = itemView.findViewById(R.id.iv_chat_user2);
            tv_chat_message= itemView.findViewById(R.id.tv_chat_message);
            tv_chat_message2= itemView.findViewById(R.id.tv_chat_message2);;

        }


    }


}