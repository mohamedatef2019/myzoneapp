package grand.myzone.images;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImagesItem implements Serializable {

    @SerializedName("image")
    private String image;

    @SerializedName("id")
    private int id;

    @Expose
    private Bitmap bitmap;


    public ImagesItem(int id,String image){
        this.id=id;
        this.image=image;
    }


    public ImagesItem(int id,Bitmap bitmap){
        this.id=id;
        this.bitmap=bitmap;
    }


    public void setImage(String image){
        this.image = image;
    }

    public String getImage(){
        return image;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }


    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}