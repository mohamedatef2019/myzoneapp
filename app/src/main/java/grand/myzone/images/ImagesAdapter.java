package grand.myzone.images;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;


import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.databinding.DynamicItemImageBinding;


public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.CompaniesViewHolder> {

    private final List<ImagesItem> imagesItems;
    FilesImagesSelectInterface filesImagesSelectInterface;

    public ImagesAdapter(List<ImagesItem> imagesItems,FilesImagesSelectInterface filesImagesSelectInterface) {
        this.filesImagesSelectInterface=filesImagesSelectInterface;
        this.imagesItems = imagesItems;

    }

    @Override
    public   CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dynamic_item_image,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        ImagesItem imagesItem = imagesItems.get(position);
        ImageItemViewModel imageItemViewModel = new ImageItemViewModel(imagesItem);



        holder.itemImageBinding.ivImageDelete.setOnClickListener(view -> {

            new SweetAlertDialog(view.getContext(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(ResourcesManager.getString(R.string.delete))
                    .setContentText(ResourcesManager.getString(R.string.do_you_want_delete_item))
                    .setConfirmButton(ResourcesManager.getString(R.string.ok), sweetAlertDialog -> {
                        imagesItems.remove(position);
                        notifyDataSetChanged();
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                        filesImagesSelectInterface.onFilesImagesSelectInterface(position);

                    }).setCancelButton(ResourcesManager.getString(R.string.cancel),
                    sweetAlertDialog -> {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }).show();
        });




            holder.itemView.setOnClickListener(view -> {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(imagesItems.get(position).getImage()));
                    view.getContext().startActivity(intent);
                }catch (Exception e){
                    e.getStackTrace();
                }
            });

            holder.itemImageBinding.ivDynamicPhoto.setImageBitmap(imagesItem.getBitmap());


        holder.setViewModel(imageItemViewModel);
    }



    @Override
    public int getItemCount() {
        return this.imagesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        DynamicItemImageBinding itemImageBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemImageBinding == null) {
                itemImageBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemImageBinding != null) {
                itemImageBinding.unbind();
            }
        }

        void setViewModel(ImageItemViewModel imageItemViewModel) {
            if (itemImageBinding != null) {
                itemImageBinding.setImageItemViewModel(imageItemViewModel);
            }
        }


    }


}