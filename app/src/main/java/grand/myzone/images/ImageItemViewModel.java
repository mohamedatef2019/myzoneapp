
package grand.myzone.images;

import android.util.Log;
import android.widget.ImageView;

import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

import grand.myzone.base.volleyutils.ConnectionHelper;


public class ImageItemViewModel extends BaseObservable {

    public ImagesItem imagesItem;


    public ImageItemViewModel(ImagesItem imagesItem) {
        this.imagesItem = imagesItem;
    }


    @BindingAdapter({"app:imageUrlDynamic"})
    public static void setItemImage(ImageView view, String imageItem){


        if(imageItem!=null) {
            Log.e("ImageUrl", imageItem);
        }

                ConnectionHelper.loadImage(view, imageItem);



    }





}
