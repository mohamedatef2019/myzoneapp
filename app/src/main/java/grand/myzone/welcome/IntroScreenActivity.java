package grand.myzone.welcome;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.nineoldandroids.view.ViewHelper;

import java.util.List;

import grand.myzone.R;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.activities.SplashScreenActivity;
import grand.myzone.base.views.navigationdrawer.MenuViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.fcmmanager.MyFirebaseMessagingService;
import grand.myzone.help.HelpResponse;
import grand.myzone.home.HomeFragment;
import grand.myzone.login.view.LoginFragment;
import grand.myzone.welcome.response.WelcomeItem;
import grand.myzone.welcome.response.WelcomeResponse;


@SuppressLint("InlinedApi")
public class IntroScreenActivity extends AppCompatActivity {

    int TOTAL_PAGES = 1;
    TextView btnDone;
    ImageView btnNext;
    ViewPager viewpager;
    ScreenSlideAdapter pagerAdapter;
    LinearLayout circles;
    RelativeLayout welcomeSplash;
    TextView skipTextView;
    boolean isOpaque = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MenuViewModel.changeLanguage(this,  UserPreferenceHelper.getCurrentLanguage(this));
        SplashScreenActivity.changeLanguage(this,  UserPreferenceHelper.getCurrentLanguage(this));

        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.welcome_layout);


        try{
            Intent intent = new Intent(this, MyFirebaseMessagingService.class);
            startService(intent);
        }catch (Exception e){
            e.getStackTrace();
        }

        welcomeSplash = findViewById(R.id.iv_welcome_splash);
        skipTextView = findViewById(R.id.iv_add_post_base_back);

        int secondsDelayed = 1;

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(UserPreferenceHelper.isFirstTime()){
                    welcomeSplash.setVisibility(View.GONE);


                    skipTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startApp();
                        }
                    });


                }else {

                    startApp();

                }
            }
        }, secondsDelayed * 1000);



        btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewpager.getCurrentItem()+1 == TOTAL_PAGES){
                    startApp();
                }else {
                    viewpager.setCurrentItem(viewpager.getCurrentItem() + 1, true);
                }
            }
        });

        btnDone = findViewById(R.id.done);
        btnDone.setOnClickListener(v -> viewpager.setCurrentItem(viewpager.getCurrentItem() - 1, true));
        
        viewpager = findViewById(R.id.pager);

        getIntroData();




    }


    private void setupWelcomeData(List<WelcomeItem> welcomeItems) {
        pagerAdapter = new ScreenSlideAdapter(getSupportFragmentManager(), welcomeItems);
        viewpager.setAdapter(pagerAdapter);
        viewpager.setPageTransformer(true, new CrossfadePageTransformer());
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == TOTAL_PAGES - 2 && positionOffset > 0) {
                    if (isOpaque) {
                        viewpager.setBackgroundColor(Color.TRANSPARENT);
                        isOpaque = false;
                    }
                } else {
                    if (!isOpaque) {
                        viewpager.setBackgroundColor(getResources().getColor(R.color.colorGreyLight));
                        isOpaque = true;
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                setIndicator(position);
                if (position > 0 ) {
                  //  btnNext.setVisibility(View.GONE);
                    btnDone.setVisibility(View.VISIBLE);
                } else if (position == 0 ) {
                    btnNext.setVisibility(View.VISIBLE);
                    btnDone.setVisibility(View.GONE);
                } else if (position == TOTAL_PAGES - 1) {
                    endIntroduction();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        buildCircles();
    }

    private void getIntroData() {

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                WelcomeResponse welcomeResponse = (WelcomeResponse) response;
                TOTAL_PAGES=welcomeResponse.getData().size();
                setupWelcomeData(welcomeResponse.getData());
            }
        }).requestJsonObject(Request.Method.GET, WebServices.WELCOME, new Object(), WelcomeResponse.class);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (viewpager != null) {
            viewpager.clearOnPageChangeListeners();
        }
    }

    private void buildCircles() {
        circles = findViewById(R.id.circles);

        float scale = getResources().getDisplayMetrics().density;
        int padding = (int) (5 * scale + 0.5f);

        for (int i = 0; i < TOTAL_PAGES ; i++) {
            ImageView circle = new ImageView(this);
            circle.setImageResource(R.drawable.ic_radio_un_selected);
            circle.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            circle.setAdjustViewBounds(true);
            circle.setPadding(padding, 0, padding, 0);
            circles.addView(circle);
        }

        setIndicator(0);
    }

    private void setIndicator(int index) {
        if (index < TOTAL_PAGES) {
            for (int i = 0; i < TOTAL_PAGES; i++) {
                ImageView circle = (ImageView) circles.getChildAt(i);
                if (i == index) {
                    circle.setImageResource(R.drawable.ic_radio_selected);
                } else {
                    circle.setImageResource(R.drawable.ic_radio_un_selected);
                }
            }
        }
    }

    @SuppressLint("PrivateResource")
    private void endIntroduction() {
        //finish();
      //  overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onBackPressed() {
        if (viewpager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            viewpager.setCurrentItem(viewpager.getCurrentItem() - 1);
        }
    }

    private static class ScreenSlideAdapter extends FragmentStatePagerAdapter {

        List<WelcomeItem> welcomeItems;

        public ScreenSlideAdapter(FragmentManager fm, List<WelcomeItem> welcomeItems) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

            this.welcomeItems = welcomeItems;
        }


        @NonNull
        @Override
        public Fragment getItem(int position) {
            IntroScreenFragment introScreenFragment = null;
            introScreenFragment = IntroScreenFragment.newInstance(R.layout.fragment_welcome_screen, welcomeItems.get(position));
            return introScreenFragment;
        }

        @Override
        public int getCount() {
            return welcomeItems.size();
        }
    }

    public static class CrossfadePageTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View page, float position) {
            int pageWidth = page.getWidth();

            View backgroundView = page.findViewById(R.id.welcome_fragment);
            View centerImg = page.findViewById(R.id.img);
            View text_head = page.findViewById(R.id.screen_heading);
            View text_content = page.findViewById(R.id.screen_desc);

            if (0 <= position && position < 1) {
                ViewHelper.setTranslationX(page, pageWidth * -position);
            }
            if (-1 < position && position < 0) {
                ViewHelper.setTranslationX(page, pageWidth * -position);
            }

            if (position != 0.0f && position >= -1.0f || position <= 1.0f) {
                if (backgroundView != null) {

                    ViewHelper.setAlpha(backgroundView, 1.0f - Math.abs(position));
                }

                if (centerImg != null) {
                    ViewHelper.setTranslationX(centerImg, pageWidth * position);
                    ViewHelper.setAlpha(centerImg, 1.0f - Math.abs(position));
                }

                if (text_head != null) {
                    ViewHelper.setTranslationX(text_head, pageWidth * position);
                    ViewHelper.setAlpha(text_head, 1.0f - Math.abs(position));
                }

                if (text_content != null) {
                    ViewHelper.setTranslationX(text_content, pageWidth * position);
                    ViewHelper.setAlpha(text_content, 1.0f - Math.abs(position));
                }
            }
        }
    }


    private void startApp(){

        if(UserPreferenceHelper.isLogined()){
            MovementManager.startMainActivity(this, HomeFragment.class.getName());
        }else {
            MovementManager.startActivity(this, LoginFragment.class.getName(),getString(R.string.label_login));
        }

    }
}