package grand.myzone.welcome;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import grand.myzone.R;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.welcome.response.WelcomeItem;

public class IntroScreenFragment extends Fragment {
 
    final static String LAYOUT_ID = "layoutId";
 
    public static IntroScreenFragment newInstance(int layoutId, WelcomeItem welcomeItem) {
        IntroScreenFragment pane = new IntroScreenFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(LAYOUT_ID, layoutId);
        bundle.putString("name",welcomeItem.getTitle());
        bundle.putString("desc",welcomeItem.getContent());
        bundle.putString("image",welcomeItem.getImage());
        pane.setArguments(bundle);
        return pane;
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(getArguments().getInt(LAYOUT_ID, -1), container, false);
        ImageView image = root.findViewById(R.id.img);
        TextView title = root.findViewById(R.id.screen_heading);
        TextView desc = root.findViewById(R.id.screen_desc);

        ConnectionHelper.loadImage(image,getArguments().getString("image"));
        title.setText(getArguments().getString("name"));
        desc.setText(getArguments().getString("desc"));

        return root;
    }
}