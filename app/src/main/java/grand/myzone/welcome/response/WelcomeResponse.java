package grand.myzone.welcome.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class WelcomeResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<WelcomeItem> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<WelcomeItem> data){
		this.data = data;
	}

	public List<WelcomeItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}