package grand.myzone.welcome.response;

import com.google.gson.annotations.SerializedName;

public class WelcomeItem {

	@SerializedName("image")
	private String image;

	@SerializedName("title")
	private String title;

	@SerializedName("content")
	private String content;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return content;
	}
}