package grand.myzone.filter.viewmodel;


import android.util.Log;
import android.view.View;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import com.makeramen.roundedimageview.RoundedImageView;
import java.util.ArrayList;
import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.filter.response.AttributesItem;
import grand.myzone.filter.response.FilterDataResponse;
import grand.myzone.filter.view.FilterMainsAdapter;



public class FilterViewModel extends BaseViewModel {

    private FilterMainsAdapter filterMainsAdapter;

    public FilterViewModel(int id,int sub) {
        getFilter(id,sub);
    }

    @BindingAdapter({"app:filter_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, FilterMainsAdapter filterMainsAdapter) {
        recyclerView.setAdapter(filterMainsAdapter);
    }

    public FilterMainsAdapter getFilterMainsAdapter() {
        return filterMainsAdapter = filterMainsAdapter == null ? new FilterMainsAdapter() : filterMainsAdapter;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }

    private void getFilter(int id,int sub) {


        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                FilterDataResponse filterDataResponse = (FilterDataResponse)response;

                ArrayList<AttributesItem>attributesItems = new ArrayList<>();

                AttributesItem governmentItem = new AttributesItem();
                governmentItem.setName(ResourcesManager.getString(R.string.label_governmate));
                governmentItem.setValues(filterDataResponse.getData().getGovernments());
                attributesItems.add(governmentItem);


                AttributesItem cityItem = new AttributesItem();
                cityItem.setName(ResourcesManager.getString(R.string.label_city));
                cityItem.setValues(filterDataResponse.getData().getCities());
                attributesItems.add(cityItem);


                AttributesItem areaItem = new AttributesItem();
                areaItem.setName(ResourcesManager.getString(R.string.label_area));
                areaItem.setValues(filterDataResponse.getData().getAreas());
                attributesItems.add(areaItem);

                attributesItems.addAll(filterDataResponse.getData().getAttributes());


                getFilterMainsAdapter().updateData(attributesItems);

                accessLoadingBar(View.GONE);

                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.FILTER_DATA+id+"&sub_category_id="+sub, null, FilterDataResponse.class);
    }

    public void searchClick(){
        getClicksMutableLiveData().setValue(Codes.SEARCH_USING_FILTER);
    }
}
