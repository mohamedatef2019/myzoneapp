package grand.myzone.filter.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.filter.response.AttributesItem;
import grand.myzone.filter.view.AttributesAdapter;
import grand.myzone.filter.view.FilterAdapter;


public class AttributeMainItemViewModel extends BaseItemViewModel {

    private AttributesItem attributesItem;
    private AttributesAdapter attributesAdapter;



    public AttributeMainItemViewModel(AttributesItem attributesItem) {
        this.attributesItem = attributesItem;
        getAttributesAdapter().updateData(attributesItem.getValues());


    }

    @BindingAdapter({"app:attribute_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, AttributesAdapter attributesAdapter) {
        recyclerView.setAdapter(attributesAdapter);
    }



    @Bindable
    public AttributesAdapter getAttributesAdapter() {
        return this.attributesAdapter == null ? this.attributesAdapter = new AttributesAdapter() : this.attributesAdapter;
    }


    @Bindable
    public AttributesItem getAttributesItem() {
        return attributesItem;
    }

    public void onArrowClick(){
        attributesItem.isOpened=!attributesItem.isOpened;

        getItemsOperationsLiveListener().setValue(Codes.OPEN_CLOSE_TAB);
        notifyChange();
    }

    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
