package grand.myzone.filter.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.followers.view.FollowersAdapter;


public class AttributeItemViewModel extends BaseItemViewModel {

    private ValuesItem valuesItem;


    public AttributeItemViewModel(ValuesItem valuesItem) {
        this.valuesItem = valuesItem;

    }

    @Bindable
    public ValuesItem getValuesItem() {
        return valuesItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
