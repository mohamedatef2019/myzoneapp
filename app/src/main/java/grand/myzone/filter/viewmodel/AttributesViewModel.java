package grand.myzone.filter.viewmodel;


import android.view.View;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import com.makeramen.roundedimageview.RoundedImageView;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.response.attributes.AttributiesResponse;
import grand.myzone.filter.view.AttrbutiesMainsAdapter;


public class AttributesViewModel extends BaseViewModel {

    private AttrbutiesMainsAdapter attrbutiesMainsAdapter;
    ValuesItem valuesItem;

    public AttributesViewModel(ValuesItem valuesItem) {
        this.valuesItem=valuesItem;
        getAttributes();
    }

    @BindingAdapter({"app:attributes_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, AttrbutiesMainsAdapter attrbutiesMainsAdapter) {
        recyclerView.setAdapter(attrbutiesMainsAdapter);
    }

    public AttrbutiesMainsAdapter getAttrubtiesMainsAdapter() {
        return attrbutiesMainsAdapter = attrbutiesMainsAdapter == null ? new AttrbutiesMainsAdapter() : attrbutiesMainsAdapter;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem) {
        if (imageItem != null && imageItem.length() > 0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }

    public void next(){
        getClicksMutableLiveData().setValue(Constant.ADD_AD);
    }

    private void getAttributes() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AttributiesResponse filterDataResponse = (AttributiesResponse) response;

                getAttrubtiesMainsAdapter().updateData(filterDataResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.ATTRIBUTES+valuesItem.getId()+"&category_id="+valuesItem.getCategoryId(), null, AttributiesResponse.class);
    }
}
