package grand.myzone.filter.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.filter.response.AttributesItem;
import grand.myzone.filter.view.FilterAdapter;
import grand.myzone.followers.view.FollowersAdapter;


public class FilterMainItemViewModel extends BaseItemViewModel {

    private AttributesItem attributesItem;
    private FilterAdapter filterAdapter;

    public FilterMainItemViewModel(AttributesItem attributesItem) {
        this.attributesItem = attributesItem;
        getFilterAdapter().updateData(attributesItem.getValues());

    }

    @BindingAdapter({"app:filter_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, FilterAdapter filterAdapter) {
        recyclerView.setAdapter(filterAdapter);
    }



    @Bindable
    public FilterAdapter getFilterAdapter() {
        return this.filterAdapter == null ? this.filterAdapter = new FilterAdapter() : this.filterAdapter;
    }


    @Bindable
    public AttributesItem getAttributesItem() {
        return attributesItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
