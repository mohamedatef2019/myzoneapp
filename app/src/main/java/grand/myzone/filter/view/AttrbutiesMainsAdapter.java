
package grand.myzone.filter.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.databinding.ItemMainAttributeBinding;
import grand.myzone.databinding.ItemMainFilterBinding;
import grand.myzone.filter.response.AttributesItem;
import grand.myzone.filter.viewmodel.AttributeMainItemViewModel;
import grand.myzone.filter.viewmodel.FilterMainItemViewModel;


public class AttrbutiesMainsAdapter extends RecyclerView.Adapter<AttrbutiesMainsAdapter.CategoriesViewHolder> {

    private List<AttributesItem> attributesItems;


    public AttrbutiesMainsAdapter( ) {
     
        this.attributesItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_attribute,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        AttributesItem attributesItem = attributesItems.get(position);
        AttributeMainItemViewModel attributeMainItemViewModel = new AttributeMainItemViewModel(attributesItem);
        attributeMainItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(attributeMainItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.attributesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<AttributesItem> data) {

            this.attributesItems.clear();

            this.attributesItems.addAll(data);

        notifyDataSetChanged();
    }

    public List<AttributesItem> getAttributesItems() {
        return attributesItems;
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemMainAttributeBinding itemMainAttributeBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMainAttributeBinding == null) {
                itemMainAttributeBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMainAttributeBinding != null) {
                itemMainAttributeBinding.unbind();
            }
        }

        void setViewModel(AttributeMainItemViewModel attributeMainItemViewModel) {
            if (itemMainAttributeBinding != null) {
                itemMainAttributeBinding.setAttributeMainItemViewModel(attributeMainItemViewModel);
            }
        }


    }



}
