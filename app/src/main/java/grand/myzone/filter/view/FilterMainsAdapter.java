
package grand.myzone.filter.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.databinding.ItemMainFilterBinding;
import grand.myzone.filter.response.AttributesItem;
import grand.myzone.filter.viewmodel.FilterMainItemViewModel;



public class FilterMainsAdapter extends RecyclerView.Adapter<FilterMainsAdapter.CategoriesViewHolder> {

    private List<AttributesItem> attributesItems;
 

    public FilterMainsAdapter( ) {
     
        this.attributesItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_filter,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        AttributesItem followersItem = attributesItems.get(position);

        FilterMainItemViewModel filterMainItemViewModel = new FilterMainItemViewModel(followersItem);
        filterMainItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(filterMainItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.attributesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public List<AttributesItem> getAttributesItems() {
        return attributesItems;
    }

    public void updateData(@Nullable List<AttributesItem> data) {

            this.attributesItems.clear();

            this.attributesItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemMainFilterBinding itemMainFilterBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMainFilterBinding == null) {
                itemMainFilterBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMainFilterBinding != null) {
                itemMainFilterBinding.unbind();
            }
        }

        void setViewModel(FilterMainItemViewModel filterMainItemViewModel) {
            if (itemMainFilterBinding != null) {
                itemMainFilterBinding.setFilterItemViewModel(filterMainItemViewModel);
            }
        }


    }



}
