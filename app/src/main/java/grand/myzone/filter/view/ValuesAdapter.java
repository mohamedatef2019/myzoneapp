
package grand.myzone.filter.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.databinding.ItemAdValueBinding;
import grand.myzone.databinding.ItemFilterBinding;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.viewmodel.AttributeItemViewModel;
import grand.myzone.filter.viewmodel.AttributesViewModel;
import grand.myzone.filter.viewmodel.FilterItemViewModel;


public class ValuesAdapter extends RecyclerView.Adapter<ValuesAdapter.CategoriesViewHolder> {

    private List<ValuesItem> valuesItems;


    public ValuesAdapter( ) {
     
        this.valuesItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad_value,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ValuesItem followersItem = valuesItems.get(position);

        AttributeItemViewModel filterItemViewModel = new AttributeItemViewModel(followersItem);
        filterItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(filterItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.valuesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ValuesItem> data) {

            this.valuesItems.clear();

            this.valuesItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemAdValueBinding itemFilterBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemFilterBinding == null) {
                itemFilterBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemFilterBinding != null) {
                itemFilterBinding.unbind();
            }
        }

        void setViewModel(AttributeItemViewModel filterItemViewModel) {
            if (itemFilterBinding != null) {
                itemFilterBinding.setAttributesViewModel(filterItemViewModel);
            }
        }


    }



}
