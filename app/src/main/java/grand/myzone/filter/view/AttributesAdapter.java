
package grand.myzone.filter.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.databinding.ItemAttributeBinding;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.viewmodel.AttributeItemViewModel;
import grand.myzone.filter.viewmodel.FilterItemViewModel;


public class AttributesAdapter extends RecyclerView.Adapter<AttributesAdapter.CategoriesViewHolder> {

    private List<ValuesItem> valuesItems;
    private int selected=-1;

    public AttributesAdapter( ) {
     
        this.valuesItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_attribute,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ValuesItem valuesItem = valuesItems.get(position);
        AttributeItemViewModel attributeItemViewModel = new AttributeItemViewModel(valuesItem);
        holder.itemView.setOnClickListener(v -> {
            if (selected != -1) {
                valuesItems.get(selected).setSelected(false);
            }
            valuesItems.get(position).setSelected(true);
            selected = position;
            notifyDataSetChanged();
        });
        holder.setViewModel(attributeItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.valuesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ValuesItem> data) {

            this.valuesItems.clear();

            this.valuesItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemAttributeBinding itemAttributeBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemAttributeBinding == null) {
                itemAttributeBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemAttributeBinding != null) {
                itemAttributeBinding.unbind();
            }
        }

        void setViewModel(AttributeItemViewModel attributeItemViewModel) {
            if (itemAttributeBinding != null) {
                itemAttributeBinding.setAttributesViewModel(attributeItemViewModel);
            }
        }


    }



}
