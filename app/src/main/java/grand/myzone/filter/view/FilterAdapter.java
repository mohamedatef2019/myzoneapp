
package grand.myzone.filter.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.databinding.ItemFilterBinding;
import grand.myzone.databinding.ItemMainFilterBinding;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.viewmodel.FilterItemViewModel;
import grand.myzone.filter.viewmodel.FilterMainItemViewModel;


public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.CategoriesViewHolder> {

    private List<ValuesItem> valuesItems;
    private int selected=-1;

    public FilterAdapter( ) {
     
        this.valuesItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ValuesItem followersItem = valuesItems.get(position);

        FilterItemViewModel filterItemViewModel = new FilterItemViewModel(followersItem);
        filterItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.itemView.setOnClickListener(v -> {
            if (selected != -1) {
                valuesItems.get(selected).setSelected(false);
            }
            valuesItems.get(position).setSelected(true);
            selected = position;
            notifyDataSetChanged();
        });

        holder.setViewModel(filterItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.valuesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ValuesItem> data) {

            this.valuesItems.clear();

            this.valuesItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemFilterBinding itemFilterBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemFilterBinding == null) {
                itemFilterBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemFilterBinding != null) {
                itemFilterBinding.unbind();
            }
        }

        void setViewModel(FilterItemViewModel filterItemViewModel) {
            if (itemFilterBinding != null) {
                itemFilterBinding.setFilterItemViewModel(filterItemViewModel);
            }
        }


    }



}
