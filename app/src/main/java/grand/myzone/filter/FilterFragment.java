package grand.myzone.filter;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.mohammedalaa.seekbar.DoubleValueSeekBarView;
import com.mohammedalaa.seekbar.OnDoubleValueSeekBarChangeListener;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.addad.AddAdFragment;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentAttributesBinding;
import grand.myzone.databinding.FragmentFilterBinding;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.viewmodel.AttributesViewModel;
import grand.myzone.filter.viewmodel.FilterViewModel;

public class FilterFragment extends BaseFragment {
    private View rootView;
    private FilterViewModel filterViewModel;
    private FragmentFilterBinding fragmentFilterBinding;
    private FilterRequest filterRequest;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false);
        filterRequest = new FilterRequest();
        init();
        setPriceRange();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentFilterBinding.getRoot();
        filterViewModel = new FilterViewModel(getArguments().getInt(Params.BUNDLE_CATEGORY, 0),getArguments().getInt(Params.BUNDLE_SUB_CATEGORY, 0));

        fragmentFilterBinding.setFilterViewModel(filterViewModel);
    }


    private void liveDataListeners() {
        filterViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Constant.ADD_AD) {

            }else if( result == Codes.SEARCH_USING_FILTER){
                filterRequest.setMinPrice(doubleValueSeekBarView.getLeft());
                filterRequest.setMaxPrice(doubleValueSeekBarView.getRight());
                filterRequest.setCityIds(new ArrayList<>());
                filterRequest.setAreaIds(new ArrayList<>());
                filterRequest.setGovernmentIds(new ArrayList<>());
                filterRequest.setValueIds(new ArrayList<>());

                for(int i=0; i<filterViewModel.getFilterMainsAdapter().getAttributesItems().size(); i++){

                List<ValuesItem> values = filterViewModel.getFilterMainsAdapter().getAttributesItems().get(i).getValues();

                    for(int j=0; j<values.size(); j++) {

                        if(values.get(j).isSelected()) {
                            if (i == 0) {
                                filterRequest.getGovernmentIds().add(values.get(j).getId());
                            } else if (i == 1) {
                                filterRequest.getCityIds().add(values.get(j).getId());
                            } else if (i == 2) {
                                filterRequest.getAreaIds().add(values.get(j).getId());
                            } else {
                                filterRequest.getValueIds().add(values.get(j).getId());
                            }
                        }
                    }
                }

                Intent intent = new Intent();
                intent.putExtra(Params.INTENT_FILTER,filterRequest);
                requireActivity().setResult(1693,intent);
                requireActivity().finish();

            }
        });
    }
    RangeSeekBar doubleValueSeekBarView;
    private void setPriceRange() {
        try {

            doubleValueSeekBarView = fragmentFilterBinding.rangeSeekbar;

            fragmentFilterBinding.tvFilterStart.setText( "50 " + ResourcesManager.getString(R.string.label_egp));
            fragmentFilterBinding.tvFilterEnd.setText( "10000 " + ResourcesManager.getString(R.string.label_egp));

            doubleValueSeekBarView.setRight(8000);
            doubleValueSeekBarView.setLeft(50);
            doubleValueSeekBarView.setValue(1000,80000);
            doubleValueSeekBarView.setOnRangeChangedListener(new OnRangeChangedListener() {
                @Override
                public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {

                    fragmentFilterBinding.tvFilterStart.setText((int)leftValue + " " + ResourcesManager.getString(R.string.label_egp));
                    fragmentFilterBinding.tvFilterEnd.setText((int)rightValue + " " + ResourcesManager.getString(R.string.label_egp));

                }

                @Override
                public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

                }

                @Override
                public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

                }
            });

        } catch (Exception e) {
            e.getStackTrace();
        }
    }


}