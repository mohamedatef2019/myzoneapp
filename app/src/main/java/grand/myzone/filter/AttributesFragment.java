package grand.myzone.filter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.addad.AddAdFragment;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentAttributesBinding;
import grand.myzone.filter.response.AttributesItem;
import grand.myzone.filter.response.ValuesItem;
import grand.myzone.filter.viewmodel.AttributesViewModel;

public class AttributesFragment extends BaseFragment {
    private View rootView;
    private AttributesViewModel attributesViewModel;
    private FragmentAttributesBinding fragmentAttributesBinding;
    private ValuesItem valuesItem;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAttributesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_attributes, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentAttributesBinding.getRoot();
        valuesItem = (ValuesItem) getArguments().getSerializable(Params.BUNDLE_CATEGORY);
        attributesViewModel = new AttributesViewModel(valuesItem);
        fragmentAttributesBinding.setAttributesViewModel(attributesViewModel);
    }


    private void liveDataListeners() {
        attributesViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if(result== Constant.ADD_AD) {
                Bundle bundle = new Bundle();
                AddAdRequest addAdRequest = new AddAdRequest();
                addAdRequest.setSubCategoryId(valuesItem.getId()+"");
                addAdRequest.setCategoryId(valuesItem.getCategoryId()+"");
                addAdRequest.setAttributesItems(attributesViewModel.getAttrubtiesMainsAdapter().getAttributesItems());
                bundle.putSerializable(Params.BUNDLE_ITEM_AD,addAdRequest);

                MovementManager.startActivity(requireActivity(), AddAdFragment.class.getName(),bundle, ResourcesManager.getString(R.string.label_ad_details));
            }
        });

//
//        attributesViewModel.getAttrubtiesMainsAdapter().getValuesItemMutableLiveData().observe(getViewLifecycleOwner(), new Observer<ValuesItem>() {
//            @Override
//            public void onChanged(ValuesItem valuesItem) {
//                if (AttributesFragment.this.valuesItem != null) {
//
//                } else {
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable(Params.BUNDLE_CATEGORY, valuesItem);
//                    MovementManager.startActivity(requireActivity(), AttributesFragment.class.getName(), bundle, valuesItem.getName());
//                }
//            }
//        });


    }


}