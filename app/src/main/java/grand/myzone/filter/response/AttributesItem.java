package grand.myzone.filter.response;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttributesItem implements Serializable {

	@Expose
	@SerializedName("values")
	private List<ValuesItem> values;

	@Expose
	@SerializedName("name")
	private String name;



	@Expose
	private String valueText;

	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	public boolean isOpened=false;

	@Expose
	@SerializedName("used_tag")
	private int usedTag;


	public void setUsedTag(int usedTag){
		this.usedTag = usedTag;
	}

	public int getUsedTag(){
		return usedTag;
	}



	public void setValues(List<ValuesItem> values){
		this.values = values;
	}

	public List<ValuesItem> getValues(){
		return values;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setOpened(boolean opened) {
		isOpened = opened;
	}

	public String getValueText() {
		return valueText;
	}

	public void setValueText(String valueText) {
		this.valueText = valueText;
	}


}