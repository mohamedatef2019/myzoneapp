package grand.myzone.filter.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class FilterDataItem {

	@SerializedName("cities")
	private List<ValuesItem> cities;

	@SerializedName("governments")
	private List<ValuesItem> governments;

	@SerializedName("areas")
	private List<ValuesItem> areas;

	@SerializedName("attributes")
	private List<AttributesItem> attributes;

	public void setCities(List<ValuesItem> cities){
		this.cities = cities;
	}

	public List<ValuesItem> getCities(){
		return cities;
	}

	public void setGovernments(List<ValuesItem> governments){
		this.governments = governments;
	}

	public List<ValuesItem> getGovernments(){
		return governments;
	}

	public void setAreas(List<ValuesItem> areas){
		this.areas = areas;
	}

	public List<ValuesItem> getAreas(){
		return areas;
	}

	public void setAttributes(List<AttributesItem> attributes){
		this.attributes = attributes;
	}

	public List<AttributesItem> getAttributes(){
		return attributes;
	}
}