package grand.myzone.filter.response;

import com.google.gson.annotations.SerializedName;

public class FilterDataResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private FilterDataItem data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(FilterDataItem data){
		this.data = data;
	}

	public FilterDataItem getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}