package grand.myzone.filter.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ValuesItem implements Serializable {

	@Expose
	@SerializedName("used_tag")
	private int usedTag;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("key_name")
	private String keyName;

	@Expose
	@SerializedName("value_name")
	private String valueName;


	@Expose
	@SerializedName("text")
	private String text;



	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	@SerializedName("category_id")
	private int categoryId;

	@Expose
	@SerializedName("type")
	private String type;


	@Expose
	private boolean isSelected=false;

	public void setUsedTag(int usedTag){
		this.usedTag = usedTag;
	}

	public int getUsedTag(){
		return usedTag;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		this.isSelected = selected;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getValueName() {

		if(this.text!=null)return valueName=this.text;
		return valueName;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
}