package grand.myzone.filter.response.attributes;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.myzone.filter.response.AttributesItem;

public class AttributiesResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<AttributesItem> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<AttributesItem> data){
		this.data = data;
	}

	public List<AttributesItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}