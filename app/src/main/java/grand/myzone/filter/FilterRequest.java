package grand.myzone.filter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class FilterRequest implements Serializable {

    @SerializedName("min_price")
    double minPrice;

    @SerializedName("category_id")
    double categoryId;

    @SerializedName("sub_category_id")
    double subCategoryId;

    @SerializedName("max_price")
    double maxPrice;

    @SerializedName("government_ids")
    ArrayList<Integer>governmentIds;

    @SerializedName("city_ids")
    ArrayList<Integer>cityIds;

    @SerializedName("area_ids")
    ArrayList<Integer>areaIds;

    @SerializedName("value_ids")
    ArrayList<Integer>valueIds;

    public void setAreaIds(ArrayList<Integer> areaIds) {
        this.areaIds = areaIds;
    }

    public void setCityIds(ArrayList<Integer> cityIds) {
        this.cityIds = cityIds;
    }

    public void setGovernmentIds(ArrayList<Integer> governmentIds) {
        this.governmentIds = governmentIds;
    }

    public void setValueIds(ArrayList<Integer> valueIds) {
        this.valueIds = valueIds;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public ArrayList<Integer> getAreaIds() {
        return areaIds;
    }

    public ArrayList<Integer> getCityIds() {
        return cityIds;
    }

    public ArrayList<Integer> getGovernmentIds() {
        return governmentIds;
    }

    public ArrayList<Integer> getValueIds() {
        return valueIds;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setCategoryId(double categoryId) {
        this.categoryId = categoryId;
    }

    public double getCategoryId() {
        return categoryId;
    }

    public void setSubCategoryId(double subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public double getSubCategoryId() {
        return subCategoryId;
    }


}
