package grand.myzone.userdetails.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentPrivacyBinding;
import grand.myzone.databinding.FragmentUserDetailsBinding;
import grand.myzone.privacy.PrivacyViewModel;
import grand.myzone.userdetails.viewmodel.UserDetailsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDetailsFragment extends BaseFragment {

    FragmentUserDetailsBinding binding;
    UserDetailsViewModel viewModel;
    private static final String TAG = "HelpFragment";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_details, container, false);
        viewModel = new UserDetailsViewModel(getArguments().getInt(Params.BUNDLE_ANY_ID));
        binding.setUserDetailsViewModel(viewModel);
        init();
        return binding.getRoot();
    }

    public void init() {
        viewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {

        });
    }
}
