package grand.myzone.userdetails.response;

import com.google.gson.annotations.SerializedName;

import grand.myzone.ads.response.Advertisements;
import grand.myzone.login.response.UserItem;

public class Data{

	@SerializedName("advertisements")
	private Advertisements advertisements;

	@SerializedName("user")
	private UserItem user;

	public void setAdvertisements(Advertisements advertisements){
		this.advertisements = advertisements;
	}

	public Advertisements getAdvertisements(){
		return advertisements;
	}

	public void setUser(UserItem user){
		this.user = user;
	}

	public UserItem getUser(){
		return user;
	}
}