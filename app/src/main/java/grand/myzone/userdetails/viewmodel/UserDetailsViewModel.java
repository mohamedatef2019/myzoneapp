package grand.myzone.userdetails.viewmodel;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.myzone.ads.request.FollowRequest;
import grand.myzone.ads.response.AdsResponse;
import grand.myzone.ads.view.AdsAdapter;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.userdetails.response.UserProfileResponse;


public class UserDetailsViewModel extends BaseViewModel {

    private AdsAdapter adsAdapter;


    public UserDetailsViewModel(int userId) {
        getProfileDetails(userId);
    }


    @BindingAdapter({"app:ads_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, AdsAdapter subCategoriesAdapter) {
        recyclerView.setAdapter(subCategoriesAdapter);
    }




    @Bindable
    public AdsAdapter getAdsAdapter() {
        return this.adsAdapter == null ? this.adsAdapter = new AdsAdapter() : this.adsAdapter;
    }


    UserProfileResponse userProfileResponse;

    public void getProfileDetails(int userId) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                userProfileResponse = (UserProfileResponse) response;
                getAdsAdapter().updateData(userProfileResponse.getData().getAdvertisements().getAdsItems());
                accessLoadingBar(View.GONE);
                notifyChange();
            }

            @Override
            public void onRequestError(Object error) {

                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.USER_PROFILE+userId, null, UserProfileResponse.class);


    }

    public void followUser(){

        notifyChange();
        FollowRequest followRequest = new FollowRequest();
        followRequest.setUserId(userProfileResponse.getData().getUser().getId());

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

            }
        }).requestJsonObject(Request.Method.POST, WebServices.FOLLOW, followRequest, DefaultResponse.class);
    }

    public void onFollowClick(){
        followUser();
        userProfileResponse.getData().getUser().setFollow(!userProfileResponse.getData().getUser().isFollow());
    }

    public UserProfileResponse getUserProfileResponse() {
        return userProfileResponse;
    }
}
