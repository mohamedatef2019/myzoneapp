package grand.myzone.updatead;

import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import grand.myzone.R;
import grand.myzone.CustomDialogs;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.addad.viewmodel.AddAdViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentEditAd2Binding;



public class UpdateAd2Fragment extends BaseFragment {

    FragmentEditAd2Binding binding;
    AddAdViewModel addAdViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_ad_2, container, false);


        addAdViewModel = new AddAdViewModel((AddAdRequest) getArguments().getSerializable(Params.BUNDLE_ITEM_AD));


        binding.setAddAdViewModel(addAdViewModel);

        requireActivity().requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},1231);
        init();
        return binding.getRoot();
    }

    public void init() {
        addAdViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
              if(result == Codes.NEXT){
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ITEM_AD,addAdViewModel.getAddAdRequest());
                MovementManager.startActivity(requireActivity(), UpdateAd3Fragment.class.getName(),bundle, ResourcesManager.getString(R.string.label_add_ad));
            }else if(result == Codes.ADDING_ERROR){
                CustomDialogs.wariningDialog(requireActivity(),addAdViewModel.getReturnedMessage());
            }
        });
    }





}
