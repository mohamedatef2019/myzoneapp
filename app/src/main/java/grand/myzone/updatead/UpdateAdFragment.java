package grand.myzone.updatead;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import grand.myzone.R;
import grand.myzone.CustomDialogs;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.addad.viewmodel.AddAdViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.activities.BaseActivity;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.categories.view.CategoriesFragment;
import grand.myzone.databinding.FragmentEditAdBinding;
import grand.myzone.erikagtierrez.multiple_media_picker.Gallery;
import grand.myzone.filter.response.ValuesItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateAdFragment extends BaseFragment {

    FragmentEditAdBinding binding;
    AddAdViewModel addAdViewModel;
    static final int OPEN_MEDIA_PICKER = 1;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_ad, container, false);


        addAdViewModel = new AddAdViewModel((AddAdRequest) getArguments().getSerializable(Params.BUNDLE_ITEM_AD));


        binding.setAddAdViewModel(addAdViewModel);

        init();
        return binding.getRoot();
    }

    public void init() {
        addAdViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == Constant.SELECT_IMAGE) {

                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                boolean permissionGranted = true;

                for(String permission : permissions) {
                    if (ContextCompat.checkSelfPermission(requireActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                        permissionGranted = false;
                    }
                }


                if (permissionGranted) {
                    Intent intent = new Intent(requireActivity(), Gallery.class);
                    intent.putExtra("title", "Select media");
                    intent.putExtra("mode", 2);
                    intent.putExtra("maxSelection", 12);
                    startActivityForResult(intent, OPEN_MEDIA_PICKER);
                } else {
                    requireActivity().requestPermissions(permissions, 1231);
                }

            } else if (result == Codes.NEXT) {
                Bundle bundle = new Bundle();
                addAdViewModel.getAddAdRequest().setSelectionResult(selectionResult);
                bundle.putSerializable(Params.BUNDLE_ITEM_AD, addAdViewModel.getAddAdRequest());
                MovementManager.startActivity(requireActivity(), UpdateAd2Fragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_add_ad));
            } else if (result == Codes.ADDING_ERROR) {

                CustomDialogs.wariningDialog(requireActivity(), addAdViewModel.getReturnedMessage());
            } else if (result == Codes.SELECT_CATEGORY_CLICK) {

                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Params.INTENT_PAGE, CategoriesFragment.class.getName());
                intent.putExtra(Params.INTENT_PAGE_TITLE, ResourcesManager.getString(R.string.label_edit_ad));
                Bundle bundle = new Bundle();
                bundle.putBoolean(Params.BUNDLE_FROM_EDIT, true);
                intent.putExtra(Params.INTENT_BUNDLE, bundle);
                startActivityForResult(intent, 123);


            } else if (result == Codes.SELECT_SUB_CATEGORY_CLICK) {


                Intent intent = new Intent(context, BaseActivity.class);
                ValuesItem valuesItem = new ValuesItem();
                valuesItem.setId(Integer.parseInt(addAdViewModel.addAdRequest.getCategoryId()));
                valuesItem.setCategoryId(Integer.parseInt(addAdViewModel.addAdRequest.getCategoryId()));

                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_CATEGORY, valuesItem);
                bundle.putBoolean(Params.BUNDLE_FROM_EDIT, true);
                intent.putExtra(Params.INTENT_BUNDLE, bundle);

                intent.putExtra(Params.INTENT_PAGE, CategoriesFragment.class.getName());
                intent.putExtra(Params.INTENT_PAGE_TITLE, ResourcesManager.getString(R.string.label_edit_ad));
                startActivityForResult(intent, 123);

            }
        });
    }


    ArrayList<String> selectionResult;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == OPEN_MEDIA_PICKER) {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    selectionResult = data.getStringArrayListExtra("result");
                    addAdViewModel.getAddAdRequest().setSelectionResult(selectionResult);
                }
            } else {

                if (resultCode == 180) {
                    addAdViewModel.addAdRequest.setCategoryName(data.getStringExtra("category_name"));
                    addAdViewModel.addAdRequest.setCategoryId(data.getIntExtra("category_id", 0) + "");
                } else {
                    addAdViewModel.addAdRequest.setSubCategoryName(data.getStringExtra("category_name"));
                    addAdViewModel.addAdRequest.setSubCategoryId(data.getIntExtra("category_id", 0) + "");
                }
                addAdViewModel.notifyChange();
            }
        } catch (Exception e) {
            Log.e("WHTY", "TEST" + e.getStackTrace() + " " + e.getMessage());
            e.getStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
