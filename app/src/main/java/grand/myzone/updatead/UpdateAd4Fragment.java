package grand.myzone.updatead;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.SelectLocationActivity;
import grand.myzone.CustomDialogs;
import grand.myzone.addad.AdAddedFragment;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.addad.viewmodel.AddAdViewModel;
import grand.myzone.addad.viewmodel.PlaceItem;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.filesutils.FileOperations;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.customviews.spinner.PopUpItem;
import grand.myzone.base.views.customviews.spinner.PopUpMenus;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentEditAd4Binding;
import grand.myzone.home.HomeFragment;


public class UpdateAd4Fragment extends BaseFragment {

    FragmentEditAd4Binding binding;
    AddAdViewModel addAdViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_ad_4, container, false);

        init();
        return binding.getRoot();
    }

    public void init() {

        addAdViewModel = new AddAdViewModel((AddAdRequest) getArguments().getSerializable(Params.BUNDLE_ITEM_AD));

        if(addAdViewModel.getAddAdRequest().getSelectionResult()!=null) {
            for (int i = 0; i < addAdViewModel.getAddAdRequest().getSelectionResult().size(); i++) {
                VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(requireActivity(),
                        addAdViewModel.getAddAdRequest().getSelectionResult().get(i), "images[" + i + "]",
                        Codes.FILE_TYPE_IMAGE);
                addAdViewModel.volleyFileObjects.add(volleyFileObject);
            }
        }


        binding.setAddAdViewModel(addAdViewModel);

        addAdViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }
            else if(result== Codes.CITIES){
            setPlaces(addAdViewModel.cities,1,binding.etCompleteAdCity);
            }else if(result == Codes.AREAS){
                setPlaces(addAdViewModel.areas,2,binding.etCompleteAdArea);
            }else if(result == Codes.GOVERNMENTS){
                setPlaces(addAdViewModel.governments,0,binding.etCompleteAdGovernorate);
            }else if(result== Codes.ADDING_ERROR){
                CustomDialogs.wariningDialog(requireActivity(),addAdViewModel.getReturnedMessage());
            }else if(result== Codes.SELECT_PROFILE_IMAGE){
                CustomDialogs.wariningDialog(requireActivity(),addAdViewModel.getReturnedMessage());
            }else if(result==Codes.SELECT_LOCATION){
                if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(requireActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},1234);
                    return;
                }else {
                    startActivityForResult(new Intent(requireActivity(), SelectLocationActivity.class), 343);
                }
            }else if(result == Codes.DATA_RECIVED){
                requireActivity().finishAffinity();
                MovementManager.startMainActivity(requireActivity(), HomeFragment.class.getName());
                MovementManager.startActivity(requireActivity(), AdAddedFragment.class.getName(), ResourcesManager.getString(R.string.label_add_ad));
            }

        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            addAdViewModel.getAddAdRequest().setLatitude(data.getStringExtra("lat"));
            addAdViewModel.getAddAdRequest().setLongitude(data.getStringExtra("lng"));
        }catch (Exception e){
            e.getStackTrace();
        }
    }

    private void setPlaces(List<PlaceItem> placeItems, int type, EditText placeEditText) {

        PopupMenu popupMenu;

        ArrayList<PopUpItem> popUpItems = new ArrayList<>();
        for (int i = 0; i < placeItems.size(); i++) {
            popUpItems.add(new PopUpItem(placeItems.get(i).getId() + "", placeItems.get(i).getName()));
        }

        popupMenu = PopUpMenus.showPopUp(getActivity(), placeEditText, popUpItems);

        popupMenu.setOnMenuItemClickListener(item -> {

            placeEditText.setText(popUpItems.get(item.getItemId()).getName());
            if (type == 1) {
                addAdViewModel.getAddAdRequest().setCityId(popUpItems.get(item.getItemId()).getId());
                addAdViewModel.getAddAdRequest().setCity(popUpItems.get(item.getItemId()).getName());
            } else if (type == 2) {
                addAdViewModel.getAddAdRequest().setAreaId(popUpItems.get(item.getItemId()).getId());
                addAdViewModel.getAddAdRequest().setArea(popUpItems.get(item.getItemId()).getName());
            } else {
                addAdViewModel.getAddAdRequest().setGovernmentId(popUpItems.get(item.getItemId()).getId());
                addAdViewModel.getAddAdRequest().setGovernment(popUpItems.get(item.getItemId()).getName());
            }


            return false;
        });
        popupMenu.show();

    }


}
