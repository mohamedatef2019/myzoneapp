package grand.myzone.updatead;

import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import grand.myzone.R;
import grand.myzone.CustomDialogs;
import grand.myzone.addad.request.AddAdRequest;
import grand.myzone.addad.viewmodel.AddAdViewModel;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentEditAd3Binding;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateAd3Fragment extends BaseFragment {

    FragmentEditAd3Binding binding;
    AddAdViewModel addAdViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_ad_3, container, false);
        addAdViewModel = new AddAdViewModel((AddAdRequest) getArguments().getSerializable(Params.BUNDLE_ITEM_AD));
        addAdViewModel.getAttributes();
        binding.setAddAdViewModel(addAdViewModel);
        init();
        return binding.getRoot();
    }

    public void init() {
        addAdViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == Codes.NEXT) {
                Bundle bundle = new Bundle();
                addAdViewModel.getAddAdRequest().setAttributesItems(addAdViewModel.getAttrubtiesMainsAdapter().getAttributesItems());
                bundle.putSerializable(Params.BUNDLE_ITEM_AD, addAdViewModel.getAddAdRequest());
                MovementManager.startActivity(requireActivity(), UpdateAd4Fragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_edit_ad));
            } else if (result == Codes.ADDING_ERROR) {
                CustomDialogs.wariningDialog(requireActivity(), addAdViewModel.getReturnedMessage());
            }
        });
    }


}
