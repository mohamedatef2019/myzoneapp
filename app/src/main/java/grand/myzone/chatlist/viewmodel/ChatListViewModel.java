package grand.myzone.chatlist.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.chatlist.request.ChatRequest;
import grand.myzone.chatlist.response.ChattersResponse;
import grand.myzone.chatlist.view.ChatListAdapter;

public class ChatListViewModel extends BaseViewModel {

    private ChatListAdapter chatListAdapter;

    public ChatListViewModel() {
        getChatters();
    }

    @Bindable
    public ChatListAdapter getChatListAdapter() {
        return this.chatListAdapter == null ? this.chatListAdapter = new ChatListAdapter() : this.chatListAdapter;
    }


    @BindingAdapter("app:chat_adapter")
    public static void bindChatAdapter(RecyclerView recyclerView, ChatListAdapter chatListAdapter) {

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));


        recyclerView.setAdapter(chatListAdapter);
    }


    private void getChatters() {

        ChatRequest chatRequest = new ChatRequest(UserPreferenceHelper.getUserDetails().getId());
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ChattersResponse chatListResponse = (ChattersResponse) response;
                getChatListAdapter().updateData(chatListResponse.getData().getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.CHATTERS, chatRequest, ChattersResponse.class);


    }

}
