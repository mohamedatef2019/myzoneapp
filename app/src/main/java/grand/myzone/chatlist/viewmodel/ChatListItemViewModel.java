
package grand.myzone.chatlist.viewmodel;

import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.chatlist.response.ChattersItem;


public class ChatListItemViewModel extends BaseItemViewModel {
    private ChattersItem chatListItem;


    public ChatListItemViewModel(ChattersItem chatListItem) {
        this.chatListItem = chatListItem;
    }


    @Bindable
    public ChattersItem getChatListItem() {
        return chatListItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(ImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }


}
