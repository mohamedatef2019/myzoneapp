package grand.myzone.chatlist.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.myzone.R;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.chatlist.viewmodel.ChatListViewModel;
import grand.myzone.chatmessages.view.ChatMessagesFragment;
import grand.myzone.databinding.FragmentChatListBinding;


public class ChatListFragment extends BaseFragment {
    private View rootView;
    private ChatListViewModel chatListViewModel;
    private FragmentChatListBinding fragmentChatListBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentChatListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_list, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentChatListBinding.getRoot();
        chatListViewModel = new ChatListViewModel();
        fragmentChatListBinding.setChatListViewModel(chatListViewModel);
    }


    private void liveDataListeners() {
        chatListViewModel.getClicksMutableLiveData().observe(getActivity(), o -> {
            int result = (int) o;
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }
        });

        chatListViewModel.getChatListAdapter().getItemsOperationsLiveListener().observe(getActivity(), chatItem -> {
                Bundle bundle = new Bundle();
                bundle.putInt(Params.BUNDLE_ANY_ID,chatItem.getId());
                bundle.putInt(Params.USER_ID,chatItem.getUser().getId());
                MovementManager.startActivity(requireActivity(), ChatMessagesFragment.class.getName(),bundle, ResourcesManager.getString(R.string.chat));
        });
    }


}