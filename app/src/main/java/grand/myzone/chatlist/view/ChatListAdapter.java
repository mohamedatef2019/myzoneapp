
package grand.myzone.chatlist.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.myzone.R;
import grand.myzone.chatlist.response.ChattersItem;
import grand.myzone.chatlist.viewmodel.ChatListItemViewModel;
import grand.myzone.databinding.ItemChatterBinding;


public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.CompaniesViewHolder> {

    private List<ChattersItem> chatHistoryItems;
    private MutableLiveData<ChattersItem> itemsOperationsLiveListener;


    public ChatListAdapter() {
        this.chatHistoryItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemChatterBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_chatter
                , parent, false);
        return new CompaniesViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {

        ChattersItem chatHistoryItem = chatHistoryItems.get(position);
        ChatListItemViewModel companyItemViewModel = new ChatListItemViewModel(chatHistoryItem);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemsOperationsLiveListener.setValue(chatHistoryItem);
            }
        });
        holder.setViewModel(companyItemViewModel);
    }


    public MutableLiveData<ChattersItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.chatHistoryItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ChattersItem> data) {

        this.chatHistoryItems.clear();

        this.chatHistoryItems.addAll(data);


        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemChatterBinding itemChatCompanyBinding;

        CompaniesViewHolder(ItemChatterBinding binding) {
            super(binding.getRoot());
            this.itemChatCompanyBinding = binding;
        }

        void bind() {
            if (itemChatCompanyBinding == null) {
                itemChatCompanyBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemChatCompanyBinding != null) {
                itemChatCompanyBinding.unbind();
            }
        }

        void setViewModel(ChatListItemViewModel serviceItemViewModel) {
            if (itemChatCompanyBinding != null) {
                itemChatCompanyBinding.setItemChatterViewModel(serviceItemViewModel);
            }
        }


    }


}
