package grand.myzone.chatlist.request;

import com.google.gson.annotations.SerializedName;

public class ChatRequest {

    @SerializedName("user_id")
    int userId;

    public ChatRequest(int userId) {
        this.userId = userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
