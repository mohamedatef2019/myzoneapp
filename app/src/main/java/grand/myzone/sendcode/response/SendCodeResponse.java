package grand.myzone.sendcode.response;


import com.google.gson.annotations.SerializedName;

import grand.myzone.login.response.UserItem;



public class SendCodeResponse {

    @SerializedName("message")
    private String msg;

    @SerializedName("code")
    private int code;

    @SerializedName("data")
    private UserItem data;


    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }


    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setData(UserItem data) {
        this.data = data;
    }

    public UserItem getData() {
        return data;
    }


}