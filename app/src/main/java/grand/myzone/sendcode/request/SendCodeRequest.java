package grand.myzone.sendcode.request;


import com.google.gson.annotations.SerializedName;

public class SendCodeRequest  {

    @SerializedName("code")
    private String code;

    @SerializedName("key")
    private String key;

    @SerializedName("email")
    private String email;


    @SerializedName("phone")
    private String phone;

    @SerializedName("type")
    private String type;



	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}