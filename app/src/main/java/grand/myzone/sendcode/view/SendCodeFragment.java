package grand.myzone.sendcode.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.util.Objects;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentConfirmCodingBinding;
import grand.myzone.home.HomeFragment;
import grand.myzone.register.view.RegisterFragment;
import grand.myzone.sendcode.viewmodel.SendCodeViewModel;


public class SendCodeFragment extends BaseFragment {

    private View rootView;
    private SendCodeViewModel sendCodeViewModel;
    private FragmentConfirmCodingBinding fragmentSendCodeBinding;
    private boolean isForgotPassword=false;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSendCodeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_coding, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSendCodeBinding.getRoot();

        try {
            isForgotPassword = getArguments().getBoolean(Params.IS_FORGOT_PASSWORD,false);
            sendCodeViewModel = new SendCodeViewModel(isForgotPassword,getArguments().getString(Params.BUNDLE_PHONE,""));
        }catch (Exception e){
            sendCodeViewModel = new SendCodeViewModel(false,getArguments().getString(Params.BUNDLE_PHONE,""));
            e.getStackTrace();
        }

        fragmentSendCodeBinding.setSendCodeViewModel(sendCodeViewModel);


    }


    private void liveDataListeners() {
        sendCodeViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.HOME_SCREEN) {
                requireActivity().finishAffinity();
                MovementManager.startMainActivity(getActivity(), HomeFragment.class.getName());
            }else if(result == Codes.SHOW_MESSAGE){
                showMessage(sendCodeViewModel.getReturnedMessage());
            }else if(result == Codes.SET_CODE){
             sendCodeViewModel.getSendCodeRequest().setCode(Objects.requireNonNull(fragmentSendCodeBinding.firstPinView.getText().toString()));
            }else if(result == Codes.REGISTER_SCREEN){

                MovementManager.startActivity(requireActivity(), RegisterFragment.class.getName(),"title");
            }

        });
    }


}