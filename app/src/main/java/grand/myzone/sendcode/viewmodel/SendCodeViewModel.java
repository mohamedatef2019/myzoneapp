package grand.myzone.sendcode.viewmodel;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import com.android.volley.Request;
import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.login.response.UserItem;
import grand.myzone.sendcode.request.SendCodeRequest;
import grand.myzone.sendcode.response.SendCodeResponse;



public class SendCodeViewModel extends BaseViewModel {
    private SendCodeRequest sendCodeRequest;
    private boolean isForgotPassword;
    public String downTimer = "00:59  ";
    public boolean isEnable=false;


    public SendCodeViewModel(boolean isForgotPassword, String phone) {


       // downTimer += getString(R.string.label_resend);
        setCountDownTimer();
        this.isForgotPassword = isForgotPassword;
        sendCodeRequest = new SendCodeRequest();
        sendCodeRequest.setKey(phone);
        sendCodeRequest.setPhone(phone);
    }


    private void setCountDownTimer() {
        isEnable=false;
        new CountDownTimer(59000, 1000) {

            public void onTick(long millisUntilFinished) {
                long time = (millisUntilFinished / 1000);
                downTimer = "00:" + ((time>9)?"":"0")+time;
              //  downTimer += "  " + getString(R.string.label_resend);
                 notifyChange();
            }

            public void onFinish() {
                Log.e("TAGO","request");
                isEnable = true;
                notifyChange();
            }

        }.start();
    }


    public void reSendCodeClick() {

        sendCodeRequest.setType("verify");


        try {
            sendCodeRequest.setEmail(UserPreferenceHelper.getUserDetails().getEmail());
            sendCodeRequest.setPhone(UserPreferenceHelper.getUserDetails().getPhone());
            sendCodeRequest.setKey(UserPreferenceHelper.getUserDetails().getPhone());

        } catch (Exception e) {
            e.getStackTrace();
        }

        setCountDownTimer();
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;

                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);

                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHECK_CODE, sendCodeRequest, SendCodeResponse.class);
    }


    public void sendCodeClick() {
        getClicksMutableLiveData().setValue(Codes.SET_CODE);
        sendCodeRequest.setType("verify");
        if (isForgotPassword) {
            getClicksMutableLiveData().setValue(Codes.SET_CODE);


            sendCodeForgotPassword();
        } else {
            getClicksMutableLiveData().setValue(Codes.SET_CODE);
            sendCode();
        }
    }


    public SendCodeRequest getSendCodeRequest() {
        return sendCodeRequest;
    }


    private void sendCode() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;

                if (sendCodeResponse.getCode() == WebServices.SUCCESS) {
                    UserItem userItem = UserPreferenceHelper.getUserDetails();
                    userItem.setJwt(sendCodeResponse.getData().getJwt());
                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);

                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHECK_CODE, sendCodeRequest, SendCodeResponse.class);
    }


    private void sendCodeForgotPassword() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;
                if (sendCodeResponse.getCode() == WebServices.SUCCESS) {
                    UserItem userItem = new UserItem();
                    userItem.setJwt(sendCodeResponse.getData().getJwt());
                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.CHANGE_PASSWORD_SCREEN);
                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHECK_CODE, sendCodeRequest, SendCodeResponse.class);
    }


    public void confirmCoding() {
        getClicksMutableLiveData().setValue(Codes.SET_CODE);
        sendCodeRequest.setType("verify");
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;
                if (sendCodeResponse.getCode() == WebServices.SUCCESS) {
                    UserItem userItem = new UserItem();
                    userItem.setJwt(sendCodeResponse.getData().getJwt());

                    if(sendCodeResponse.getData().getEmail()!=null && !sendCodeResponse.getData().getEmail().equals("")){
                        userItem = sendCodeResponse.getData();
                    }
                    if(sendCodeResponse.getData().getEmail()!=null && !sendCodeResponse.getData().getEmail().equals("")){
                        getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                    }else {
                        getClicksMutableLiveData().setValue(Codes.REGISTER_SCREEN);

                    }


                    UserPreferenceHelper.saveUserDetails(userItem);

                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);

            }
        }).requestJsonObject(Request.Method.POST, WebServices.CONFIRM_CODE, sendCodeRequest, SendCodeResponse.class);
    }







}
