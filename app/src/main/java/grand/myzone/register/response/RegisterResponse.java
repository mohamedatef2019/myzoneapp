package grand.myzone.register.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import grand.myzone.login.response.UserItem;


public class RegisterResponse{

	@Expose
	@SerializedName("message")
	private String msg;

	@Expose
	@SerializedName("code")
	private int code;

	@Expose
	@SerializedName("data")
	private UserItem userItem;


	public void setUserItem(UserItem userItem) {
		this.userItem = userItem;
	}

	public UserItem getUserItem() {
		return userItem;
	}

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	@Override
 	public String toString(){
		return 
			"RegisterResponse{" + 
			"msg = '" + msg + '\'' +
			"}";
		}
}