package grand.myzone.register.request;


import com.google.gson.annotations.SerializedName;

public class RegisterRequest {




	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("email")
	private String email;

	@SerializedName("firebase_token")
	private String token;

	@SerializedName("device_token")
	private String deviceToken;


	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}


    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }


	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}


	public String getDeviceToken() {
		return deviceToken;
	}



	@Override
 	public String toString(){
		return 
			"RegisterResponse{" +
			",phone = '" + phone + '\'' + 
			",name = '" + name + '\'' + 
			",email = '" + email + '\'' + 

			"}";
		}
}