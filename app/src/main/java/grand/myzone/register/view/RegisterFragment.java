package grand.myzone.register.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.filesutils.FileOperations;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.Validate;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.databinding.FragmentCompleteRegisterBinding;
import grand.myzone.help.HelpFragment;
import grand.myzone.home.HomeFragment;
import grand.myzone.register.viewmodel.RegisterViewModel;
import grand.myzone.sendcode.view.SendCodeFragment;



public class RegisterFragment extends BaseFragment {

    private View rootView;
    private RegisterViewModel registerViewModel;
    private FragmentCompleteRegisterBinding fragmentRegisterBinding;

    private String socialId=null;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_complete_register, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }


    private void binding() {
        rootView = fragmentRegisterBinding.getRoot();
        registerViewModel = new RegisterViewModel();
        fragmentRegisterBinding.setRegisterViewModel(registerViewModel);


        try {
            registerViewModel.getRegisterRequest().setEmail(getArguments().getString("email"));
            registerViewModel.getRegisterRequest().setName(getArguments().getString("name"));
            ConnectionHelper.loadImage(fragmentRegisterBinding.ivRegisterUserPhoto,getArguments().getString("photo"));

        } catch (Exception e) {
            e.getStackTrace();
        }




    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
       // FileOperations.pickImage(getActivity());

        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);


    }

    private void liveDataListeners() {
        registerViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SELECT_PROFILE_IMAGE) {

                if(Validate.isPermissionDone(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                &&Validate.isPermissionDone(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                &&Validate.isPermissionDone(context, Manifest.permission.CAMERA)){
                    CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
                }else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, 199);
                }

            } else if (result == Codes.SHOW_MESSAGE) {
                showMessage(registerViewModel.getReturnedMessage());
            } else if (result == Codes.TERMS_CONDITION_SCREEN) {
                MovementManager.startActivity(getActivity(), HelpFragment.class.getName(), ResourcesManager.getString(R.string.label_enter_code));
            } else {
                    MovementManager.startMainActivity(getActivity(), HomeFragment.class.getName());
            }



        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();
                VolleyFileObject volleyFileObject =  FileOperations.getVolleyFileObject(getActivity(),resultUri,"image",Codes.FILE_TYPE_IMAGE);

                fragmentRegisterBinding.ivRegisterUserPhoto.setImageBitmap(volleyFileObject.getCompressObject().getImage());
                registerViewModel.getVolleyFileObjects().add(volleyFileObject);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


}