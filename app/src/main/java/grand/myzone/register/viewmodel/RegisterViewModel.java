package grand.myzone.register.viewmodel;

import android.provider.Settings;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.utils.Validate;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.login.response.UserItem;
import grand.myzone.register.request.RegisterRequest;
import grand.myzone.register.response.RegisterResponse;


public class RegisterViewModel extends BaseViewModel {

    private RegisterRequest registerRequest;

    private ArrayList<VolleyFileObject> volleyFileObjects;




    public RegisterViewModel() {
        registerRequest = new RegisterRequest();

        registerRequest.setToken(UserPreferenceHelper.getGoogleToken());

        volleyFileObjects = new ArrayList<>();

        //getCountries();
    }


    public void selectImageClick() {
        getClicksMutableLiveData().setValue(Codes.SELECT_PROFILE_IMAGE);
    }


    public RegisterRequest getRegisterRequest() {
        return registerRequest;
    }


    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }


    public void skip() {

        accessLoadingBar(View.VISIBLE);

        int random = 1111;

        try {
            random = new Random().nextInt(9999);
        }catch (Exception e){
            e.getStackTrace();
        }
        registerRequest.setEmail("vistor@visitor"+random+".com");
        registerRequest.setName("زائر"+random);
        registerRequest.setDeviceToken(UserPreferenceHelper.getGoogleToken());
        //registerRequest.setDeviceToken("1235243");
       //registerRequest.setToken("3432");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                RegisterResponse registerResponse = (RegisterResponse) response;
                if (registerResponse.getCode()==WebServices.SUCCESS) {

                    UserItem userItem = new UserItem();
                    userItem.setId(registerResponse.getUserItem().getId());
                    userItem.setEmail(registerRequest.getEmail());
                    userItem.setPhone(registerResponse.getUserItem().getPhone());
                    userItem.setUsersImg(registerResponse.getUserItem().getUsersImg());
                    userItem.setJwt(registerResponse.getUserItem().getJwt());
                    userItem.setUsersName(registerResponse.getUserItem().getUsersName());
                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                    getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);

                } else  {

                    setReturnedMessage(registerResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);

                }
                accessLoadingBar(View.GONE);
            }
        }).multiPartConnect( WebServices.REGISTER, registerRequest,volleyFileObjects, RegisterResponse.class);
    }

    private boolean validate() {

        if(Validate.isEmpty(registerRequest.getName())){
           setReturnedMessage(ResourcesManager.getString(R.string.hint_enter_name));
            getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
            return false;
        }else if(Validate.isEmpty(registerRequest.getEmail())){
            setReturnedMessage(ResourcesManager.getString(R.string.label_enter_email));
            getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
            return false;
        }else if(!Validate.isMail(registerRequest.getEmail())){
            setReturnedMessage(ResourcesManager.getString(R.string.msg_wrong_email));
            getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
            return false;
        }
        return true;
    }


    public void goRegister() {


        if(validate()){
            registerRequest.setDeviceToken(UserPreferenceHelper.getGoogleToken());
        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                RegisterResponse registerResponse = (RegisterResponse) response;
                if (registerResponse.getCode()==WebServices.SUCCESS) {

                    UserItem userItem = new UserItem();
                    userItem.setId(registerResponse.getUserItem().getId());
                    userItem.setEmail(registerRequest.getEmail());
                    userItem.setPhone(registerResponse.getUserItem().getPhone());
                    userItem.setUsersImg(registerResponse.getUserItem().getUsersImg());
                    userItem.setJwt(registerResponse.getUserItem().getJwt());
                    userItem.setUsersName(registerResponse.getUserItem().getUsersName());
                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                    getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);

                } else  {

                    setReturnedMessage(registerResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);

                }
                accessLoadingBar(View.GONE);
            }
        }).multiPartConnect( WebServices.REGISTER, registerRequest,volleyFileObjects, RegisterResponse.class);

        }
    }



    public void setupData(){

        UserItem userItem = UserPreferenceHelper.getUserDetails();

        try {
            Log.e("RRRR", userItem.getPhone());
        }catch (Exception e){
            e.getStackTrace();
        }

        getRegisterRequest().setName(userItem.getUsersName());
        getRegisterRequest().setEmail(userItem.getEmail());
        getRegisterRequest().setPhone(userItem.getPhone());
        notifyChange();
    }


    public void goUpdateProfile() {

        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                RegisterResponse registerResponse = (RegisterResponse) response;

                if (registerResponse.getCode()==WebServices.SUCCESS) {
                    UserPreferenceHelper.saveUserDetails(registerResponse.getUserItem());
                }

                setReturnedMessage(registerResponse.getMsg());
                getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                accessLoadingBar(View.GONE);
            }

        }).multiPartConnect( WebServices.UPDATE, registerRequest,volleyFileObjects, RegisterResponse.class);

    }



}
