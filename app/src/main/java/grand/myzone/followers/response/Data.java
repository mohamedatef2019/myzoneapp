package grand.myzone.followers.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("data")
	private List<FollowersItem> data;

	@SerializedName("meta")
	private Meta meta;

	@SerializedName("links")
	private Links links;

	public void setData(List<FollowersItem> data){
		this.data = data;
	}

	public List<FollowersItem> getData(){
		return data;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	public void setLinks(Links links){
		this.links = links;
	}

	public Links getLinks(){
		return links;
	}
}