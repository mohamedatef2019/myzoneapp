package grand.myzone.followers.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.followers.response.FollowersItem;


public class FollowerItemViewModel extends BaseItemViewModel {

    private FollowersItem followersItem;



    public FollowerItemViewModel(FollowersItem followersItem) {
        this.followersItem = followersItem;

    }

    @Bindable
    public FollowersItem getFollowersItem() {
        return followersItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
