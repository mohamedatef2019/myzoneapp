package grand.myzone.followers.viewmodel;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.followers.response.FollowersResponse;
import grand.myzone.followers.view.FollowersAdapter;


public class FollowersViewModel extends BaseViewModel {

    private FollowersAdapter followersAdapter;


    public FollowersViewModel( ) {

    }


    @BindingAdapter({"app:followers_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, FollowersAdapter followersAdapter) {
        recyclerView.setAdapter(followersAdapter);
    }



    @Bindable
    public FollowersAdapter getFollowersAdapter() {
        return this.followersAdapter == null ? this.followersAdapter = new FollowersAdapter(true) : this.followersAdapter;
    }


    @Bindable
    public FollowersAdapter getFollowingAdapter() {
        return this.followersAdapter == null ? this.followersAdapter = new FollowersAdapter(false) : this.followersAdapter;
    }

    public void getFollowings() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                FollowersResponse followersResponse = (FollowersResponse)response;

                getFollowingAdapter().updateData(followersResponse.getData().getData());
                accessLoadingBar(View.GONE);
                notifyChange();

            }
        }).requestJsonObject(Request.Method.GET, WebServices.FOLLOWING, null, FollowersResponse.class);

    }


    public void getFollowers() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                FollowersResponse followersResponse = (FollowersResponse)response;

                getFollowersAdapter().updateData(followersResponse.getData().getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.FOLLOWERS, null, FollowersResponse.class);

    }
}
