package grand.myzone.followers.request;

import com.google.gson.annotations.SerializedName;

public class FollowersRequest {

    @SerializedName("user_id")
    int userId;

    public FollowersRequest(int userId) {
        this.userId = userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
