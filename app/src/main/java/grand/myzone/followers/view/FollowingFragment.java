package grand.myzone.followers.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.myzone.R;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentFollowersBinding;
import grand.myzone.followers.viewmodel.FollowersViewModel;

public class FollowingFragment extends BaseFragment {
    private View rootView;
    private FollowersViewModel followersViewModel;
    private FragmentFollowersBinding fragmentFollowersBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFollowersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_followers, container, false);
        init();
        return rootView;
    }

    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentFollowersBinding.getRoot();
        followersViewModel = new FollowersViewModel();
        followersViewModel.getFollowings();
        fragmentFollowersBinding.setFollowersViewModel(followersViewModel);

    }


    private void liveDataListeners() {
        followersViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });
    }



}