package grand.myzone.followers.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentEventsBinding;
import grand.myzone.databinding.FragmentMainFollowerBinding;
import grand.myzone.followers.viewmodel.FollowersViewModel;

public class FragmentMainFollowers extends BaseFragment {
    private View rootView;
    private FragmentMainFollowerBinding fragmentMainFollowerBinding;



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMainFollowerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_follower, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentMainFollowerBinding.getRoot();


        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                requireActivity().getSupportFragmentManager(), FragmentPagerItems.with(requireActivity())
                .add(ResourcesManager.getString(R.string.label_followers), FollowersFragment.class)
                .add(ResourcesManager.getString(R.string.label_following), FollowingFragment.class)
                .create());

        ViewPager viewPager = fragmentMainFollowerBinding.viewpager;
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = fragmentMainFollowerBinding.viewpagertab;
        viewPagerTab.setViewPager(viewPager);


    }


    private void liveDataListeners() {
    }



}