
package grand.myzone.followers.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.databinding.ItemFollowersBinding;
import grand.myzone.followers.response.FollowersItem;
import grand.myzone.followers.viewmodel.FollowerItemViewModel;


public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.CategoriesViewHolder> {

    private List<FollowersItem> followersItems;
    private boolean showFollowButton;

    public FollowersAdapter(boolean showFollowButton) {
        this.showFollowButton=showFollowButton;
        this.followersItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_followers,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        FollowersItem followersItem = followersItems.get(position);
        holder.itemFollowersBinding.tvFollowersFollow.setVisibility(showFollowButton?View.VISIBLE:View.GONE);
        FollowerItemViewModel orderItemViewModel = new FollowerItemViewModel(followersItem);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.followersItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<FollowersItem> data) {

            this.followersItems.clear();

            this.followersItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemFollowersBinding itemFollowersBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemFollowersBinding == null) {
                itemFollowersBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemFollowersBinding != null) {
                itemFollowersBinding.unbind();
            }
        }

        void setViewModel(FollowerItemViewModel followerItemViewModel) {
            if (itemFollowersBinding != null) {
                itemFollowersBinding.setFollowerItemViewModel(followerItemViewModel);
            }
        }


    }



}
