package grand.myzone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.login.view.LoginFragment;
import grand.myzone.welcome.IntroScreenActivity;


public class CustomDialogs {


    public static void wariningDialog(Context context, String message) {
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.label_warning))
                .setContentText(message)
                .setConfirmButton(ResourcesManager.getString(R.string.dialog_okey), sweetAlertDialog -> {

                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();

                })
                .show();
    }


    public static void wariningDialog(Context context, String message, SweetAlertDialog.OnSweetClickListener listener) {
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.label_warning))
                .setContentText(message)
                .setConfirmButton(ResourcesManager.getString(R.string.dialog_okey), listener)
                .show();
    }

    public static void successDialog(Context context, String message) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.label_success))
                .setContentText(message)
                .setConfirmButton(ResourcesManager.getString(R.string.dialog_okey), sweetAlertDialog -> {

                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();

                })
                .show();
    }


    public static void successDialog(Context context, String message, SweetAlertDialog.OnSweetClickListener listener) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.label_success))
                .setContentText(message)

                .setConfirmButton(ResourcesManager.getString(R.string.dialog_okey), listener)
                .show();
    }


    public static void loginToContinue(Context context) {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_login, null);
        Button loginBtn = dialogView.findViewById(R.id.btn_login);
        Button cancelBtn = dialogView.findViewById(R.id.btn_cancel);


        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();

        loginBtn.setOnClickListener(view -> {
            alertDialog.cancel();
            alertDialog.dismiss();

            MovementManager.startActivity(context, LoginFragment.class.getName(),context.getString(R.string.label_login));
        });

        cancelBtn.setOnClickListener(view -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });


        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

    }


}
