package grand.myzone;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.activities.SplashScreenActivity;
import grand.myzone.base.views.navigationdrawer.MenuViewModel;


public class SelectLocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    MapView mapView;
    Button submitButton;
    GoogleMap map;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MenuViewModel.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));
        SplashScreenActivity.changeLanguage(this,  UserPreferenceHelper.getCurrentLanguage(this));


        setContentView(R.layout.fragment_select_location);


        context = this;


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        enableLocationDailog();

        init(savedInstanceState);
    }


    private void init(Bundle savedInstanceState) {
        mapView = findViewById(R.id.mapview);
        submitButton = findViewById(R.id.btn_select_location_submit);
        mapView.onCreate(savedInstanceState);


        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        } else {
            mapView.getMapAsync(this);
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setMyLocationEnabled(true);
        if (getIntent().hasExtra(Params.INTENT_LAT)) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(getIntent().getDoubleExtra(Params.INTENT_LAT, 0.0), getIntent().getDoubleExtra(Params.INTENT_LANG, 0.0)), 13));
            addMarker(new LatLng(getIntent().getDoubleExtra(Params.INTENT_LAT, 0.0), getIntent().getDoubleExtra(Params.INTENT_LANG, 0.0)), false);
            submitButton.setVisibility(View.GONE);
        } else {

            setChangeCameraListener();
            submitClick();

        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
        // MUT.changeLanguage(this, SharedPreferenceHelper.getCurrentLanguage(this));
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mapView.getMapAsync(this);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    GoogleApiClient googleApiClient;
    boolean locationDone = false;

    private void enableLocationDailog() {

        if (googleApiClient == null) {
            LocationRequest locationRequest = getLocationRequest();
            LocationSettingsRequest settingsRequest = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest).build();
            SettingsClient client = LocationServices.getSettingsClient(context);
            Task<LocationSettingsResponse> task = client
                    .checkLocationSettings(settingsRequest);

            task.addOnFailureListener((AppCompatActivity) context, e -> {
                int statusCode = ((ApiException) e).getStatusCode();
                if (statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                    try {
                        ResolvableApiException resolvable =
                                (ResolvableApiException) e;
                        resolvable.startResolutionForResult
                                ((AppCompatActivity) context,
                                        1019);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            });


        }


    }

    double lat, lang;

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    public void submitClick() {
        submitButton.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("lat", "" + lat);
            intent.putExtra("lng", "" + lang);
            ((AppCompatActivity) context).setResult(8967, intent);
            ((AppCompatActivity) context).finish();
        });
    }


    private void setChangeCameraListener() {

        map.setOnMyLocationChangeListener(location -> {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(17).bearing(90).tilt(40).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            map.setOnMyLocationChangeListener(null);
        });


        map.setOnCameraMoveStartedListener(i -> {

        });

        map.setOnCameraIdleListener(() -> {
            map.clear();
            addMarker(map.getCameraPosition().target, false);
            lat = map.getCameraPosition().target.latitude;
            lang = map.getCameraPosition().target.longitude;
        });

    }

    protected void addMarker(LatLng position, boolean draggable) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.draggable(draggable);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map));
        markerOptions.position(position);
        Marker pinnedMarker = map.addMarker(markerOptions);
        startDropMarkerAnimation(pinnedMarker);
    }

    private void startDropMarkerAnimation(final Marker marker) {
        final LatLng target = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point targetPoint = proj.toScreenLocation(target);
        final long duration = (long) (200 + (targetPoint.y * 0.6));
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        startPoint.y = 0;
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final Interpolator interpolator = new LinearOutSlowInInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * target.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * target.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later == 60 frames per second
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

}
