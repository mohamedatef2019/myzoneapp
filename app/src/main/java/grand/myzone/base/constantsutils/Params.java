package grand.myzone.base.constantsutils;

public class Params {

    //SharedPreferenceParams


    //BundleParams



    //IntentParams

    public static final String INTENT_PAGE="page";
    public static final String INTENT_BUNDLE="bundle";
    public static final String BUNDLE_PHONE="phone";
    public static final String BUNDLE_SUB_CATEGORY="sub_category";
    public static final String BUNDLE_ITEM_AD="item_ad";
    public static final String BUNDLE_SELECTED_ADS="selected_ads";
    public static final String BUNDLE_CATEGORY="category";
    public static final String BUNDLE_FULL_PAGE="full_page";
    public static final String INTENT_PAGE_TITLE="title";
    public static final String IS_FORGOT_PASSWORD="is_forgot_password";
    public static final String INTENT_FILTER="intent_filter";
    public static final String BUNDLE_ANY_ID="any_id";
    public static final String USER_ID="user_id";
    public static final String SEARCH="search";
    public static final String INTENT_LAT="lat";
    public static final String INTENT_LANG="lng";
    public static final String BUNDLE_TAG="filter_tag";
    public static final String BUNDLE_FROM_EDIT="from_edit";




}
