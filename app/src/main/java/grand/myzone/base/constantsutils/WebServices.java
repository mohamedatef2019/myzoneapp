package grand.myzone.base.constantsutils;

/**
 * Created by sameh on 4/2/18.
 */

public class WebServices {

    //Codes


    //Status
    public static final int SUCCESS = 200;
    public static final int NOT_FOUND = 401;
    public static final int GO_ACTIVIATE = 405;
    public static final String NOT_ACTIVE = "not_active";
    public static final String FAILED="failed";


    //URLS
    public static String  BASE_URL ="https://myzzone.com/api/v1/";
    public static String LOGIN ="login";
    public static String CHECK_CODE = "code_send";
    public static String CONFIRM_CODE = "code_check";
    public static String REGISTER = "register";
    public static String UPDATE="profile";
    public static String TERMS = "terms_condition";
    public static String AD_DETAILS="advertisement/";
    public static String PRIVACY = "privacy_policy";
    public static String HOME="home";
    public static String ADS="advertisements?sub_category_id=";
    public static String ALL_ADS="advertisements?category_id=";
    public static String TAGS_SEARCH="advertisements?category_id=";
    public static String ORDER_BY="order_by?sub_category_id=";
    public static String FAVOURITE_ADS="favorite";
    public static String MY_ADS="profile/advertisements";
    public static String USER_PROFILE="profile?user_id=";
    public static String SEARCH="search?search=";
    public static String ADD_FAVOURITE="favorite";
    public static String NOTIFICATIONS="notifications";
    public static String GET_CATEGORIES="categories";
    public static String GET_COMMENTS="advertisement/";
    public static String ADD_COMMENT="advertisement/comment";
    public static String SUB_CATEGORIES="subcategory?category_id=";
    public static String FILTER_DATA="filter/details?category_id=";
    public static String FILTER_RESULT="filter/result";
    public static String ATTRIBUTES="category/attributes?sub_category_id=";
    public static String CHATTERS="chats";
    public static String FOLLOWERS="profile/followers";
    public static String FOLLOWING="profile/followings";
    public static String HELP = "help";
    public static String ABOUT_US="about_us";
    public static String SOCIAL_MEDIA="socials";
    public static String CONTACT_US="contact_us";
    public static String SUGGESTIONS="complaints_suggestions";
    public static String GOVERNMENTS="governments";
    public static String CITIES="cities?government_id=";
    public static String AREAS="areas?city_id=";
    public static String ADD_ADVERTISE="advertisement";
    public static String UPDATE_ADVERTISE="advertisement/";
    public static String ADD_STORY="story";
    public static String GET_STORIES="story?user_id=";
    public static String SEND_MESSAGE="message";
    public static String CHAT_MESSAGES="chat/messages?user_id=";
    public static String WELCOME="welcome";
    public static String FOLLOW="follow";
    public static String GET_REPORT="report_reasons";
    public static String DELETE_AD="advertisement/";
    public static String REPORT_AD="advertisement/report";
    public static String MAKE_SPECIAL="advertisements/special";
    public static String DELETE_IMAGE="advertisement/image/";



}
