package grand.myzone.base.constantsutils;

public class Codes {

    //RequestsCodes
    public static int FILE_TYPE_IMAGE = 10;
    public static int FILE_TYPE_VIDEO = 11;
    public static int FILE_TYPE_PDF = 12;

    //ACTIONS
    public static int REGISTER_SCREEN = 20;
    public static int SHOW_MESSAGE = 21;
    public static int FORGOT_PASSWORD_SCREEN = 22;
    public static int CHANGE_PASSWORD_SCREEN = 23;
    public static int SEND_CODE_SCREEN = 24;
    public static int HOME_SCREEN = 25;
    public static int SET_CODE = 26;
    public static int SEARCH = 27;
    public static int TERMS_CONDITION_SCREEN=28;
    public static int SKIP=29;
    public static int EMPTY_DATA=30;
    public static int WRONG_EMAIL=31;
    public static int DATA_RECIVED=32;
    public static int NEXT=33;
    public static int CITIES=34;
    public static int GOVERNMENTS =35;
    public static int AREAS=36;
    public static int SELECT_STORY=37;
    public static int FILTER_SCREEN=38;
    public static int SEARCH_USING_FILTER=39;
    public static int ADD_COMMENT_SCREEN=40;
    public static int MY_SELECTED_ADS=41;
    public static int USER_DETAILS_DATA=42;
    public static int NO_ADS_BEFORE=43;
    public static int ADD_STORY_DATA=44;
    public static int SORT_CLICK=45;
    public static int CALL_CLICK=46;
    public static int CHAT_CLICK=47;
    public static int SHARE_CLICK=48;
    public static int MAP_CLICK=49;
    public static int FOLLOW_CLICK=50;
    public static int ON_SELECT_IMAGE_CLICK=51;
    public static int ADDING_ERROR=52;
    public static int SELECT_LOCATION=53;
    public static int REPORT_CLICK=54;
    public static int SOLD_CLICK=55;
    public static int REMOVED=56;
    public static int REMOVE_CLICK=57;
    public static int REPORTED=58;
    public static int AD_IS_SPECIAL=59;
    public static int AD_EDIT_CLICK=60;
    public static int SELECT_CATEGORY_CLICK=61;
    public static int SELECT_SUB_CATEGORY_CLICK=62;
    public static int DELETE_IMAGE=63;
    public static int OPEN_CLOSE_TAB=64;
    public static int ACCEPT_TERMS=65;
    public static int EMPTY_DATA_2=66;
    public static int LOGIN_FIRST=67;
    //Requested Code
    public static int SELECT_PROFILE_IMAGE = 210;

}
