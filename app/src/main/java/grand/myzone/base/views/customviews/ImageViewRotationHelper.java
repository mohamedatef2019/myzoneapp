package grand.myzone.base.views.customviews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;

import androidx.appcompat.widget.AppCompatImageView;

import grand.myzone.base.utils.UserPreferenceHelper;


public class ImageViewRotationHelper extends AppCompatImageView
{


    public ImageViewRotationHelper(Context context, AttributeSet attrs) {
        super(context, attrs);
        setRotation(context);
        try{
            setOnClickListener(view -> ((Activity)context).onBackPressed());
        }catch (Exception e){
            e.getStackTrace();
        }
    }

    public void setRotation(Context context){

        if(UserPreferenceHelper.getCurrentLanguage(context).equals("ar")){

            setRotation(180);
        }


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }


}