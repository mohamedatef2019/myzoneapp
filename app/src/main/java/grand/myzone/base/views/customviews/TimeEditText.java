package grand.myzone.base.views.customviews;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;

import android.util.AttributeSet;

import java.util.Calendar;

import grand.myzone.R;
import grand.myzone.base.utils.ResourcesManager;


public class TimeEditText extends CustomEditText {
    DatePickerDialog datePickerDialog;

    public TimeEditText(Context context) {
        super(context);
        init();
    }

    public TimeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TimeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        setOnClickListener(v -> showDateDialog());
    }

    private void showDateDialog() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), (timePicker, selectedHour, selectedMinute) -> {


            String amPm;

            if(selectedHour < 12) {
                amPm = ResourcesManager.getString(R.string.label_am);
            } else {
                amPm = ResourcesManager.getString(R.string.label_pm);
            }

            String selectedDate = selectedHour + ":" + selectedMinute + amPm;

            setText(selectedDate);


        }, 0, 0, false);
        mTimePicker.setTitle(ResourcesManager.getString(R.string.hint_select_time));
        mTimePicker.show();



    }

    public DatePickerDialog getDatePickerDialog() {
        return datePickerDialog;
    }
}
