package grand.myzone.base.views.customviews;

import android.content.Context;

import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupMenu;

import androidx.appcompat.widget.AppCompatEditText;

import java.util.ArrayList;

import grand.myzone.base.views.customviews.spinner.PopUpItem;


public class SpinnerEditText extends AppCompatEditText {
    PopupMenu popupMenu;
      ArrayList<PopUpItem> popUpItems ;
    public SpinnerEditText(Context context) {
        super(context);

    }

    public SpinnerEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public SpinnerEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }


    private void showPopUp(final ArrayList<PopUpItem> popUpItems){
        this.popUpItems=popUpItems;
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
             //   popupMenu = PopUpMenus.showPopUp(getContext(), SpinnerEditText.this, popUpItems);
            }
        });

    }


    public PopupMenu getPopUpMenu(){
       return popupMenu;
    }

    public ArrayList<PopUpItem> getPopUpItems() {
        return popUpItems;
    }
}
