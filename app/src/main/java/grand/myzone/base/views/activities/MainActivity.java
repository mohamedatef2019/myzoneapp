package grand.myzone.base.views.activities;

import android.Manifest;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import irfaan008.irbottomnavigation.SpaceItem;
import irfaan008.irbottomnavigation.SpaceNavigationView;
import irfaan008.irbottomnavigation.SpaceOnClickListener;

import java.util.ArrayList;

import grand.myzone.CustomDialogs;
import grand.myzone.R;
import grand.myzone.ads.view.SearchFragment;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.navigationdrawer.MenuViewModel;
import grand.myzone.base.views.navigationdrawer.NavigationDrawerView;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.categories.view.CategoriesFragment;
import grand.myzone.chatlist.view.ChatListFragment;
import grand.myzone.databinding.ActivityMainBinding;
import grand.myzone.home.HomeFragment;
import grand.myzone.myaccount.view.ProfileFragment;
import grand.myzone.myaccount.view.SettingsFragment;
import grand.myzone.notifications.view.NotificationsFragment;


public class MainActivity extends ParentActivity {

    public ActivityMainBinding activityMainBinding;
    NavigationDrawerView navigationDrawerView;
    Bundle savedInstanceState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MenuViewModel.changeLanguage(this,  UserPreferenceHelper.getCurrentLanguage(getApplicationContext()));
        SplashScreenActivity.changeLanguage(this,  UserPreferenceHelper.getCurrentLanguage(this));


        this.savedInstanceState = savedInstanceState;


        SplashScreenActivity.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));
        MenuViewModel.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
       // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1001);

        navigationDrawerView = new NavigationDrawerView(this);
        activityMainBinding.llBaseContainer.addView(navigationDrawerView);


        navigationDrawerView.layoutNavigationDrawerBinding.ivNavigationDrawerSearch.setOnClickListener(v -> MovementManager.startActivity(MainActivity.this, SearchFragment.class.getName(), ResourcesManager.getString(R.string.label_search)));

        navigationDrawerView.layoutNavigationDrawerBinding.ivNavigationDrawerNotifications.setOnClickListener(v -> MovementManager.startActivity(MainActivity.this, NotificationsFragment.class.getName(), ResourcesManager.getString(R.string.label_notifications)));


        if (getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getIntExtra(Params.INTENT_PAGE, 0));
        } else {
            MovementManager.addFragment(this, new HomeFragment(), "HomeFragment");
        }

        initMenu();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {
        if (!doubleBackToExitPressedOnce) {
            finish();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                this.doubleBackToExitPressedOnce = false;
                Toast.makeText(this, getResources().getString(R.string.msg_double_click_to_close), Toast.LENGTH_SHORT).show();
            } else {
                super.onBackPressed();
            }

        }

    }

    SpaceNavigationView spaceNavigationView;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        spaceNavigationView.onSaveInstanceState(outState);
    }


    private void addFragment(int page) {
        MovementManager.addFragment(this, new HomeFragment(), "HomeFragment");
    }

    private void initMenu() {

        spaceNavigationView = findViewById(R.id.space);
        spaceNavigationView.setCentreButtonIcon(R.drawable.ic_add_attachment);
        spaceNavigationView.setCentreButtonColor(ResourcesManager.getColor(R.color.colorAccent));
        //  spaceNavigationView.showIconOnly();
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.setActiveCentreButtonIconColor(ContextCompat.getColor(this, R.color.colorWhite));
        spaceNavigationView.setInActiveCentreButtonIconColor(ContextCompat.getColor(this, R.color.colorWhite));

        if(UserPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.my_profile), R.drawable.ic_profile));
            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.label_more), R.drawable.ic_more));
            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.label_home), R.drawable.ic_home));
            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.chat), R.drawable.ic_chat));
            spaceNavigationView.changeCurrentItem(2);
        }else {

            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.label_home), R.drawable.ic_home));
            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.chat), R.drawable.ic_chat));
            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.my_profile), R.drawable.ic_profile));
            spaceNavigationView.addSpaceItem(new SpaceItem(ResourcesManager.getString(R.string.label_more), R.drawable.ic_more));
            spaceNavigationView.changeCurrentItem(0);
        }






        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {

                if (!UserPreferenceHelper.isLogined()) {
                    CustomDialogs.loginToContinue(MainActivity.this);
                } else {
                    MovementManager.addFragment(MainActivity.this, new CategoriesFragment(), "CategoriesFragment");
                }
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {

                if(UserPreferenceHelper.getCurrentLanguage(MainActivity.this).equals("ar")) {

                    if (itemIndex == 0) {

                        if (UserPreferenceHelper.isLogined()) {
                            MovementManager.popAllFragments(MainActivity.this);
                            MovementManager.addFragment(MainActivity.this, new ProfileFragment(), "MyProfileFragment");
                        } else {
                            CustomDialogs.loginToContinue(MainActivity.this);
                        }

                    } else if (itemIndex == 1) {

                        MovementManager.popAllFragments(MainActivity.this);
                        MovementManager.addFragment(MainActivity.this, new SettingsFragment(), "SettingsFragment");

                    } else if (itemIndex == 2) {

                        MovementManager.popAllFragments(MainActivity.this);
                        MovementManager.addFragment(MainActivity.this, new HomeFragment(), "HomeFragment");

                    } else if (itemIndex == 3) {

                        if (UserPreferenceHelper.isLogined()) {
                            MovementManager.popAllFragments(MainActivity.this);
                            MovementManager.addFragment(MainActivity.this, new ChatListFragment(), "ChatListFragment");
                        } else {
                            CustomDialogs.loginToContinue(MainActivity.this);
                        }
                    }
                }else {
                    if (itemIndex == 0) {


                        MovementManager.popAllFragments(MainActivity.this);
                        MovementManager.addFragment(MainActivity.this, new HomeFragment(), "HomeFragment");



                    } else if (itemIndex == 1) {

                        if (UserPreferenceHelper.isLogined()) {
                            MovementManager.popAllFragments(MainActivity.this);
                            MovementManager.addFragment(MainActivity.this, new ChatListFragment(), "ChatListFragment");
                        } else {
                            CustomDialogs.loginToContinue(MainActivity.this);
                        }



                    } else if (itemIndex == 2) {


                        if (UserPreferenceHelper.isLogined()) {
                            MovementManager.popAllFragments(MainActivity.this);
                            MovementManager.addFragment(MainActivity.this, new ProfileFragment(), "MyProfileFragment");
                        } else {
                            CustomDialogs.loginToContinue(MainActivity.this);
                        }


                    } else if (itemIndex == 3) {
                        MovementManager.popAllFragments(MainActivity.this);
                        MovementManager.addFragment(MainActivity.this, new SettingsFragment(), "SettingsFragment");

                    }
                }


            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {

            }
        });
    }

    public NavigationDrawerView getNavigationDrawerView() {
        return navigationDrawerView;
    }


}
