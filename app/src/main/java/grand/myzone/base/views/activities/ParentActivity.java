package grand.myzone.base.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import grand.myzone.R;
import grand.myzone.base.utils.BaseContextWrapper;
import grand.myzone.base.utils.UserPreferenceHelper;


public class ParentActivity extends AppCompatActivity {


    public void showMessage(String message) {
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
            fragment.onActivityResult(requestCode, resultCode, data);
        }catch (Exception e){
            e.getStackTrace();
        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
                super.attachBaseContext(BaseContextWrapper.wrap(newBase, UserPreferenceHelper.getCurrentLanguage(newBase)));
            } else {
                final Configuration override = new Configuration(newBase.getResources().getConfiguration());
                override.fontScale = 1.0f;
                applyOverrideConfiguration(override);
            }

        } catch (
                Exception e) {
            e.getStackTrace();
        }

    }






}
