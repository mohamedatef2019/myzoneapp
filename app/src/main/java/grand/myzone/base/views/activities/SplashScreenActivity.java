package grand.myzone.base.views.activities;



import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import java.util.Locale;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.SplashScreenViewModel;
import grand.myzone.base.views.navigationdrawer.MenuViewModel;
import grand.myzone.databinding.ActivitySplashScreenBinding;
import grand.myzone.home.HomeFragment;
import grand.myzone.login.view.LoginFragment;
import grand.myzone.welcome.IntroScreenActivity;


public class SplashScreenActivity extends ParentActivity {
    ActivitySplashScreenBinding activitySplashScreenBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


            startApp();

            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},342);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        startApp();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void startApp(){



        activitySplashScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);

        if(UserPreferenceHelper.isLogined()){
            MovementManager.startActivity(this, LoginFragment.class.getName(),getString(R.string.label_login));
        }else {
            MovementManager.startMainActivity(this, HomeFragment.class.getName());
        }

    }



    public static void changeLanguage(Context context, String languageToLoad){
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
        UserPreferenceHelper.setLanguage(context, languageToLoad);


        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
        UserPreferenceHelper.setLanguage(context, languageToLoad);
    }

}
