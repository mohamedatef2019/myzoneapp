package grand.myzone.base.views.fragments;


import android.content.Context;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;


import grand.myzone.base.views.activities.BaseActivity;
import grand.myzone.base.views.activities.MainActivity;


public class BaseFragment extends Fragment {
   public Context context;



    public void accessLoadingBar(int visiablity) {
        try {
            ((BaseActivity) context).activityBaseBinding.pbBaseLoadingBar.setVisibility(visiablity);
        }catch (Exception e){
            e.getStackTrace();
        }
        try {
              ((MainActivity) context).activityMainBinding.pbBaseLoadingBar.setVisibility(visiablity);
        }catch (Exception e){
            e.getStackTrace();
        }
    }

    public void showMessage(String message) {
        //beacuse of handling snack bar


        try {
            ((BaseActivity) context).showMessage(message);
        }catch (Exception e){
            e.getStackTrace();
        }
        try {
          //  ((MainActivity) context).showMessage(message);
        }catch (Exception e){
            e.getStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.context = null;

    }



    @Override
    public void onAttach(@NonNull Context context) {

        super.onAttach(context);
        this.context = context;
    }





}
