package grand.myzone.base.views.navigationdrawer;


public class NavigationDrawerItem {

	 private int id,image;
	 private String title;


	 public NavigationDrawerItem(int id, String title, int image){
	 	this.id = id;
	 	this.title=title;
	 	this.image=image;
	 }

	public void setId(int id) {
		this.id = id;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public int getImage() {
		return image;
	}

}