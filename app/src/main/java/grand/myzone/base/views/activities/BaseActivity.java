package grand.myzone.base.views.activities;


import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.myzone.R;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.navigationdrawer.MenuViewModel;
import grand.myzone.databinding.ActivityBaseBinding;
import grand.myzone.login.view.LoginFragment;
import grand.myzone.register.view.RegisterFragment;


public class BaseActivity extends ParentActivity {
    public ActivityBaseBinding activityBaseBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MenuViewModel.changeLanguage(this,  UserPreferenceHelper.getCurrentLanguage(this));
        SplashScreenActivity.changeLanguage(this,  UserPreferenceHelper.getCurrentLanguage(this));


        SplashScreenActivity.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));
        MenuViewModel.changeLanguage(this,UserPreferenceHelper.getCurrentLanguage(this));
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
       // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1001);

        if (getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getStringExtra(Params.INTENT_PAGE));
            activityBaseBinding.tvMainTitle.setText(getIntent().getStringExtra(Params.INTENT_PAGE_TITLE));
        }


    }





    private void addFragment(String page) {

        if(page.equals(LoginFragment.class.getName()) || (page.equals(RegisterFragment.class.getName()))){
            activityBaseBinding.llBaseActionBarContainer.setVisibility(View.GONE);
        }

        try {
            Fragment fragment = (Fragment) (Class.forName(page).newInstance());
            fragment.setArguments(getIntent().getBundleExtra(Params.INTENT_BUNDLE));
            MovementManager.addFragment(this, fragment, "");
        } catch (Exception e) {
            Log.e("ERROR",e.getStackTrace()+"  "+e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
