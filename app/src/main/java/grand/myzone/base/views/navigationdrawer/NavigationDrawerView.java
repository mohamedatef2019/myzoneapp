package grand.myzone.base.views.navigationdrawer;



import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import java.util.Objects;


import grand.myzone.R;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.databinding.LayoutNavigationDrawerBinding;


public class NavigationDrawerView extends RelativeLayout {
    public LayoutNavigationDrawerBinding layoutNavigationDrawerBinding;


    public NavigationDrawerView(Context context) {
        super(context);
        init();
    }

    public NavigationDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public NavigationDrawerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        init();

    }



    private void init() {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true);

    }













}
