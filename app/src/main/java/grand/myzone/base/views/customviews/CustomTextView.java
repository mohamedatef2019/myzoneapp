package grand.myzone.base.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.TextView;

import grand.myzone.base.utils.ViewOperations;


public class CustomTextView extends TextView {


    public CustomTextView(Context context) {
        super(context);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


        init();
    }


    @Override
    public void setText(CharSequence text, BufferType type) {
        try {
            String textString = text.toString();
            if (textString == null) textString = "";
            super.setText(ViewOperations.replaceArabicNumbers(textString), type);
        }catch (Exception e){
            e.getStackTrace();
            super.setText("", type);
        }
    }



    private void init() {

        Typeface font = null;
        font = Typeface.createFromAsset(getContext().getAssets(), "maddina.otf");
         setTypeface(font);

    }


}
