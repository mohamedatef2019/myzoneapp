package grand.myzone.base.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DefaultResponse{

	@Expose
	@SerializedName("message")
	private String msg;

	@Expose
	@SerializedName("code")
	private int code;



	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}




}