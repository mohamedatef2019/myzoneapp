package grand.myzone.base.viewmodel;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import com.android.volley.Request;

import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.login.request.UserRequest;
import grand.myzone.login.response.UserItem;
import grand.myzone.login.response.UserResponse;


public class SplashScreenViewModel extends BaseViewModel {

    public SplashScreenViewModel() {
        startApp();
    }

    private void startApp() {
        Handler handler = new Handler();
        handler.postDelayed(() -> {

            if (UserPreferenceHelper.isLogined()) {
                autoLogin();
            } else {
                 getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);

            }
        }, 2000);

    }


    private void autoLogin(){

    }
}