package grand.myzone.base.baseservice;


import androidx.lifecycle.MutableLiveData;

public class BaseService {
    MutableLiveData<Integer> mutableLiveData;

    public BaseService(){
        mutableLiveData= new MutableLiveData<>();
    }

    public MutableLiveData<Integer> getMutableLiveData() {
        return mutableLiveData;
    }
}

