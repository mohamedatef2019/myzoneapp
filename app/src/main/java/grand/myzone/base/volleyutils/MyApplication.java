package grand.myzone.base.volleyutils;
import android.content.Context;

import androidx.databinding.BindingAdapter;
import androidx.multidex.MultiDexApplication;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.danikula.videocache.HttpProxyCacheServer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import grand.myzone.App;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.views.navigationdrawer.MenuViewModel;


public class MyApplication extends MultiDexApplication {
    public static final String TAG = MyApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
        MenuViewModel.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    @BindingAdapter({"app:adapter"})
    public static void getRecyclerBinding(RecyclerView recyclerView,RecyclerView.Adapter adapter) {
        if (recyclerView.getAdapter() == null) {
            recyclerView.setAdapter(adapter);
        }
    }

    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        MyApplication app = (MyApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer(this);
    }



}