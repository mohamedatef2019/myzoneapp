package grand.myzone.base.volleyutils;

import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONObject;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import grand.myzone.R;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.filesutils.VolleyFileObject;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;


public class ConnectionHelper {

    private static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.img_square_logo)
            .showImageOnLoading(R.drawable.img_square_logo)
            .showImageOnFail(R.drawable.img_square_logo)
            .cacheInMemory(true)
            .cacheOnDisk(true).build();

    private static ImageLoader imageLoader = ImageLoader.getInstance();
    private ConnectionListener connectionListener;
    private RequestQueue queue;
    private static final int TIME_OUT = 10000;
    private Gson gson;

    public ConnectionHelper(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
        queue = MyApplication.getInstance().getRequestQueue();
        gson = new Gson();
    }


    public void requestJsonObject(int method, String url, Object requestData, final Class<?> responseType) {
        final Gson gson = new Gson();
        String link = WebServices.BASE_URL + url;


        Log.e("Request :", link);


        link = link.replaceAll(" ", "%20");
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(gson.toJson(requestData));
        } catch (Exception e) {
            e.getStackTrace();
        }

        Log.e("Url :", url);
        if (jsonObject != null) {
            Log.e("Request :", jsonObject.toString());
        } else {
            Log.e("Request :", "Make sure that you added request correctly");
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(method, link, jsonObject,

                response -> {
                    Log.e("Response Success:", response.toString());
                    parseData(response, responseType);
                }

                , volleyError -> {
            showErrorDetails(volleyError);
            connectionListener.onRequestError(volleyError);
        }) {
            @Override
            public Map getHeaders() {
                return getCustomHeaders();
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);

    }


    public void multiPartConnect(String url, final HashMap<String, String> params, final List<VolleyFileObject> volleyFileObjects, final Class<?> responseType, boolean hashMap) {

        Log.e("DAs", hashMap + "");
        String link = WebServices.BASE_URL + url;
        link = link.replaceAll(" ", "%20");
        Log.e("link", link + "");


        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, link, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse networkResponse) {
                String responseString = new String(networkResponse.data);
                JSONObject response = null;
                try {
                    response = new JSONObject(responseString);
                } catch (Exception e) {

                    e.getStackTrace();
                }
                parseData(response, responseType);
            }
        }, volleyError -> {
            showErrorDetails(volleyError);
            connectionListener.onRequestError(volleyError);
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getCustomHeaders();
            }

            @Override
            protected Map<String, DataPart> getByteData() {

                return getFileParameters(volleyFileObjects);
            }

        };

        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(multipartRequest);
    }


    public void multiPartConnect(String url, final Object requestData, final List<VolleyFileObject> volleyFileObjects, final Class<?> responseType) {

        String link = WebServices.BASE_URL + url;
        link = link.replaceAll(" ", "%20");


        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, link, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse networkResponse) {
                String responseString = new String(networkResponse.data);
                JSONObject response = null;
                try {
                    response = new JSONObject(responseString);
                } catch (Exception e) {

                    e.getStackTrace();
                }
                parseData(response, responseType);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showErrorDetails(volleyError);
                connectionListener.onRequestError(volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return getParameters(requestData);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getCustomHeaders();
            }

            @Override
            protected Map<String, DataPart> getByteData() {

                return getFileParameters(volleyFileObjects);
            }

        };

        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(multipartRequest);
    }

    public HashMap getCustomHeaders() {
        HashMap headers = new HashMap();
        //header
        Log.e("REAr", "Bearer " + UserPreferenceHelper.getUserDetails().getJwt() + "");
        headers.put("Authorization", "Bearer " + UserPreferenceHelper.getUserDetails().getJwt());
        headers.put("language", UserPreferenceHelper.getCurrentLanguage(MyApplication.getInstance().getApplicationContext()));
        return headers;
    }


    public static void loadImage(final ImageView image, String imageUrl) {
        Log.d("Image Url", "Image Url " + imageUrl);

        if (imageUrl == null || imageUrl.length() < 1) {
            image.setImageDrawable(ResourcesManager.getDrawable(R.drawable.img_square_logo));
        } else {
            imageLoader.displayImage(imageUrl, image, options);
        }
    }


    public static void loadImage(String imageUrl, ImageLoadingListener listener) {
        Log.d("Image Url", "Image Url " + imageUrl);
        imageLoader.loadImage(imageUrl, options, listener);
    }


    private void showErrorDetails(VolleyError volleyError) {
        String body;

        try {
            final String statusCode = String.valueOf(volleyError.networkResponse.statusCode);
            body = new String(volleyError.networkResponse.data, StandardCharsets.UTF_8);
            Log.e("TAG", "Error Body " + body + " StatusCode " + statusCode);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    private void parseData(JSONObject response, final Class<?> responseType) {

        try {
            System.out.println("Response " + response.toString());
            if (response.toString().equals("")) {
                System.out.println("Errora " + "Fucking empty response");
                connectionListener.onRequestError(null);
            } else {
                connectionListener.onRequestSuccess(gson.fromJson(response.toString(), responseType));
            }
        } catch (Exception e) {
            System.out.println("Errora " + response.toString());
            connectionListener.onRequestError(null);
        }

    }

    private Map<String, String> getParameters(final Object requestData) {
        Map<String, String> params = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject(gson.toJson(requestData));
            for (int i = 0; i < jsonObject.names().length(); i++) {
                params.put(jsonObject.names().getString(i), jsonObject.get(jsonObject.names().getString(i)) + "");
                Log.e("PARAMS", jsonObject.get(jsonObject.names().getString(i)) + "");
            }
            Log.e("PARAMS", params.size() + "");
        } catch (Exception e) {
            Log.e("PARAMS", e.getStackTrace() + "");
            e.getStackTrace();
        }
        return params;
    }

    private Map<String, VolleyMultipartRequest.DataPart> getFileParameters(List<VolleyFileObject> volleyFileObjects) {
        Map<String, VolleyMultipartRequest.DataPart> filesParams = new HashMap<>();
        if (volleyFileObjects == null) {
            return filesParams;
        }

        for (int i = 0; i < volleyFileObjects.size(); i++) {
            final File filePath = new File(volleyFileObjects.get(i).getFilePath());
            Log.e("PARAMSz", volleyFileObjects.get(i).getParamName() + "  " + volleyFileObjects.get(i).getCompressObject().getBytes());
            filesParams.put(volleyFileObjects.get(i).getParamName(), new VolleyMultipartRequest.DataPart(filePath.getName(), volleyFileObjects.get(i).getCompressObject().getBytes()));
        }

        volleyFileObjects.clear();
        return filesParams;
    }


}