package grand.myzone.base.utils;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import grand.myzone.BuildConfig;
import grand.myzone.R;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.views.activities.BaseActivity;
import grand.myzone.base.views.activities.MainActivity;


public class MovementManager {

    //---------Fragments----------//
    private static final int CONTAINER_ID = R.id.fl_home_container;

    public static void popAllFragments(Context context) {
        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public static void addFragment(Context context, Fragment fragment, String backStackText) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().add(CONTAINER_ID, fragment);
        if (!backStackText.equals("")) {
            fragmentTransaction.addToBackStack(backStackText);
        }
        fragmentTransaction.commit();
    }


    public static void replaceFragment(Context context, Fragment fragment, String backStackText) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(CONTAINER_ID, fragment);
        if (!backStackText.equals("")) {
            fragmentTransaction.addToBackStack(backStackText);
        }
        fragmentTransaction.commit();
    }


    public static void shareContent(Context context,String shareBody) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        shareBody+="\n\n"+ ResourcesManager.getString(R.string.label_download_app) + " \n\n https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    public static void popLastFragment(Context context) {
        ((FragmentActivity) context).getSupportFragmentManager().popBackStack();
    }


    //-----------Activities-----------------//

    public static void startActivity(Context context, Class<?> activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
    }

    public static void startActivityForResult(Context context, Class<?> activity , int requestCode, String page) {
        Intent intent = new Intent(context, activity);
        intent.putExtra(Params.INTENT_PAGE,page);
        ((AppCompatActivity)context).startActivityForResult(intent,requestCode);
    }

    public static void startActivity(Context context,String page,String title) {
        Intent intent = new Intent(context, BaseActivity.class);
        intent.putExtra(Params.INTENT_PAGE,page);
        intent.putExtra(Params.INTENT_PAGE_TITLE,title);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String page, Bundle bundle,String title) {
        Intent intent = new Intent(context, BaseActivity.class);
        intent.putExtra(Params.INTENT_PAGE,page);
        intent.putExtra(Params.INTENT_PAGE_TITLE,title);
        intent.putExtra(Params.INTENT_BUNDLE,bundle);
        context.startActivity(intent);
    }

    public static void startMainActivity(Context context,String page) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(Params.INTENT_PAGE,page);
        context.startActivity(intent);
    }


    public static void startMainActivity(Context context,Bundle bundle) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(Params.INTENT_BUNDLE,bundle);
        context.startActivity(intent);
    }

    public static void startWebPage(Context context, String page) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(page)));
        }
        catch (Exception e){
            e.getStackTrace();
        }
    }
}
