package grand.myzone.base.utils;

import android.graphics.drawable.Drawable;

import grand.myzone.base.volleyutils.MyApplication;


public class ResourcesManager {

    public static Drawable getDrawable(int resId){
        return MyApplication.getInstance().getResources().getDrawable(resId);
    }

    public static int getColor(int resId){
        return MyApplication.getInstance().getResources().getColor(resId);
    }

    public static float getDim(int resId){
        return MyApplication.getInstance().getResources().getDimension(resId);
    }

    public static String getString(int resId){
        return MyApplication.getInstance().getResources().getString(resId);
    }

    public static String[] getStringArr(int resId){
        return MyApplication.getInstance().getResources().getStringArray(resId);
    }
}
