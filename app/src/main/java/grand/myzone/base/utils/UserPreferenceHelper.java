package grand.myzone.base.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import grand.myzone.base.volleyutils.MyApplication;
import grand.myzone.login.response.UserItem;


public class UserPreferenceHelper {

    private static SharedPreferences sharedPreferences = null;

    //here you can find shared preference operations like get saved data for user


    private UserPreferenceHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance( ) {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }

    public static int getUserId( ) {
        return getSharedPreferenceInstance( ).getInt("userId", -1);
    }

    public static void setCurrentReader(  String currentReader) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        prefsEditor.putString("currentReader", currentReader);
        prefsEditor.commit();
    }

    public static String getCurrentReader( ) {
        return getSharedPreferenceInstance( ).getString("currentReader", "ar.abdulbasitmurattal");
    }

    public static void setCurrentMazhab(  String currentReader) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        prefsEditor.putString("currentMazhab", currentReader);
        prefsEditor.commit();
    }

    public static String getMazhab( ) {
        return getSharedPreferenceInstance( ).getString("currentMazhab", "suny");
    }


    public static void setUserId(  int userId) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        prefsEditor.putInt("userId", userId);
        prefsEditor.commit();
    }


    public static void setRememberMe(Context context, boolean rememberMe) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        prefsEditor.putBoolean("rememberMe", rememberMe);
        prefsEditor.commit();
    }

    public static boolean isRemember(Context context) {
        return getSharedPreferenceInstance().getBoolean("rememberMe", false);
    }

    public static void saveUserDetails( UserItem userModel) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        prefsEditor.putString("userDetails", json);
        prefsEditor.putInt("userId", userModel.getId());
        prefsEditor.apply();
    }





    public static void setPrayerAvailable(int index,boolean on){
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        prefsEditor.putBoolean("prayerTimes_"+index, on);
        prefsEditor.apply();
    }


    public static boolean getPrayerAvailable(int index){
        return getSharedPreferenceInstance().getBoolean("prayerTimes_"+index, false);
    }


    public static void clearUserDetails() {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        prefsEditor.putString("userDetails", null);
        prefsEditor.putInt("userId", -1);
        prefsEditor.apply();
    }

    public static UserItem getUserDetails() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("userDetails", "");
        if(json==null||json.equals(""))return new UserItem();
        return gson.fromJson(json, UserItem.class);
    }




    public static boolean isLogined() {
        if(getUserDetails()==null) return false;
        return (getUserDetails().getId()!=0);
    }


    public static void setLanguage(Context context, String language) {
        SharedPreferences userDetails = context.getSharedPreferences("languageData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString("language", language);
        editor.putBoolean("haveLanguage", true);
        editor.apply();
    }

    public static String getCurrentLanguage(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("languageData", Context.MODE_PRIVATE);
        if (!sharedPreferences.getBoolean("haveLanguage", false)) return "en";
        return sharedPreferences.getString("language", "en");
    }

    public static void setAzan(Context context, String azan) {
        SharedPreferences userDetails = context.getSharedPreferences("azanData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString("azan", azan);
        editor.apply();
    }

    public static String getCurrentAzan(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("azanData", Context.MODE_PRIVATE);
        return sharedPreferences.getString("azan", "big");
    }


    public static void setCurrentTime(Context context, String azan) {
        SharedPreferences userDetails = context.getSharedPreferences("timerData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString("timerData", azan);
        editor.apply();
    }

    public static String getCurrentTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("timerData", Context.MODE_PRIVATE);
        return sharedPreferences.getString("timerData", "0");
    }

    public static boolean isFirstTime() {
        boolean isFirstTime = getSharedPreferenceInstance().getBoolean("firstTime", true);
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        prefsEditor.putBoolean("firstTime", false);
        prefsEditor.apply();

        return isFirstTime;
    }


    public static void saveGoogleToken(String token) {
        if (token != null && !token.equals("")) {
            SharedPreferences.Editor editor = getSharedPreferenceInstance().edit();
            editor.putString("googleToken", token);
            editor.apply();
        }
    }

    public static String getGoogleToken() {
        try {
            String token = getSharedPreferenceInstance().getString("googleToken", "");
            return token.length() < 1 ? "EmptyToken" : token;
        }catch (Exception e){
            e.getStackTrace();
        }

        return "EmptyToken";
    }


}
