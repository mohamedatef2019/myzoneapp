
package grand.myzone.comments.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.R;
import grand.myzone.comments.response.CommentItem;
import grand.myzone.comments.viewmodel.CommentItemViewModel;
import grand.myzone.databinding.ItemCommentBinding;


public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CategoriesViewHolder> {

    private List<CommentItem> commentItems;
    private MutableLiveData<CommentItem>valuesItemMutableLiveData;

    public CommentsAdapter() {
        valuesItemMutableLiveData = new MutableLiveData<>();
        this.commentItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        CommentItem categoryItem = commentItems.get(position);
        CommentItemViewModel orderItemViewModel = new CommentItemViewModel(categoryItem);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valuesItemMutableLiveData.setValue(categoryItem);
            }
        });
        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.commentItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    public MutableLiveData<CommentItem> getCommentItemMutableLiveData() {
        return valuesItemMutableLiveData;
    }

    public void updateData(@Nullable List<CommentItem> data) {

            this.commentItems.clear();

            this.commentItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemCommentBinding itemCommentBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemCommentBinding == null) {
                itemCommentBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemCommentBinding != null) {
                itemCommentBinding.unbind();
            }
        }

        void setViewModel(CommentItemViewModel commentItemViewModel) {
            if (itemCommentBinding != null) {
                itemCommentBinding.setCommentItemViewModel(commentItemViewModel);
            }
        }


    }



}
