package grand.myzone.comments.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.myzone.CustomDialogs;
import grand.myzone.R;
import grand.myzone.ads.response.AdsItem;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;

import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.comments.viewmodel.CommentsViewModel;
import grand.myzone.databinding.FragmentCommentsBinding;


public class CommentsFragment extends BaseFragment {
    private View rootView;
    private CommentsViewModel commentsViewModel;
    private FragmentCommentsBinding fragmentCommentsBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentCommentsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_comments, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentCommentsBinding.getRoot();
        commentsViewModel = new CommentsViewModel();

        try{
            commentsViewModel.getComments(((AdsItem) getArguments().getSerializable(Params.BUNDLE_ITEM_AD)).getId(),getArguments().getBoolean(Params.BUNDLE_FULL_PAGE,false));
        }catch (Exception e){
            e.getStackTrace();
        }



        fragmentCommentsBinding.setCommentsViewModel(commentsViewModel);
    }


    private void liveDataListeners() {
        commentsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if(result == Codes.LOGIN_FIRST){
                CustomDialogs.loginToContinue(requireActivity());
            } else if (result == Codes.ADD_COMMENT_SCREEN) {
                AdsItem adsItem = (AdsItem) getArguments().getSerializable(Params.BUNDLE_ITEM_AD);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Params.BUNDLE_ITEM_AD, adsItem);
                MovementManager.startActivity(requireActivity(), CommentsFragment.class.getName(), bundle, ResourcesManager.getString(R.string.label_comments));
            }
        });




    }




}