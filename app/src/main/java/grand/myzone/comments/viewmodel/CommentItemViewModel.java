package grand.myzone.comments.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.makeramen.roundedimageview.RoundedImageView;

 

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.comments.response.CommentItem;


public class CommentItemViewModel extends BaseItemViewModel {

    private CommentItem commentItem;



    public CommentItemViewModel(CommentItem commentItem) {
        this.commentItem = commentItem;

    }

    @Bindable
    public CommentItem getCommentItem() {
        return commentItem;
    }


    @BindingAdapter({"app:imageUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {
            ConnectionHelper.loadImage(view, imageItem);
        }
    }
}
