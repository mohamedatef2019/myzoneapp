package grand.myzone.comments.viewmodel;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import java.util.ArrayList;
import java.util.List;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.model.DefaultResponse;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.comments.request.AddCommentRequest;
import grand.myzone.comments.response.CommentItem;
import grand.myzone.comments.response.CommentsResponse;
import grand.myzone.comments.view.CommentsAdapter;


public class CommentsViewModel extends BaseViewModel {

    private CommentsAdapter commentsAdapter;
    AddCommentRequest addCommentRequest;
    public boolean showAll=false;
    int count=0;
    int adId;
    public CommentsViewModel() {
        addCommentRequest = new AddCommentRequest();
    }


    @BindingAdapter({"app:comments_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, CommentsAdapter commentsAdapter) {
        recyclerView.setAdapter(commentsAdapter);
    }


    @Bindable
    public CommentsAdapter getCommentsAdapter() {
        return this.commentsAdapter == null ? this.commentsAdapter = new CommentsAdapter() : this.commentsAdapter;
    }

    public int comments(){
        return count;
    }

    public void getComments(int adId , boolean showAll) {
        this.showAll = showAll;
        this.adId=adId;

        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                CommentsResponse commentsResponse = (CommentsResponse) response;

                if(showAll) {
                    List<CommentItem> data = commentsResponse.getData().getComments().getData();
                    List<CommentItem> commentItems = new ArrayList<>();
                    for(int i=0; i<data.size(); i++){
                        if(i==5){
                            break;
                        }
                        commentItems.add(data.get(i));
                    }
                    getCommentsAdapter().updateData(commentItems);
                }else {
                    getCommentsAdapter().updateData(commentsResponse.getData().getComments().getData());
                }
                count = commentsResponse.getData().getComments().getData().size();

                accessLoadingBar(View.GONE);
                notifyChange();

            }

        }).requestJsonObject(Request.Method.GET, WebServices.GET_COMMENTS + adId + "/comments", null, CommentsResponse.class);
    }


    public AddCommentRequest getAddCommentRequest() {
        return addCommentRequest;
    }

    public void addCommentClick() {
        if(UserPreferenceHelper.isLogined()) {
        addCommentRequest.setAdsId(adId);
        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                getComments(adId,false);
                accessLoadingBar(View.GONE);
                addCommentRequest.setContent(null);

            }

        }).requestJsonObject(Request.Method.POST, WebServices.ADD_COMMENT , addCommentRequest, DefaultResponse.class);
        }else {
            getClicksMutableLiveData().setValue(Codes.LOGIN_FIRST);
        }
    }


    public void onCommentsClick(){
        getClicksMutableLiveData().setValue(Codes.ADD_COMMENT_SCREEN);

    }


}
