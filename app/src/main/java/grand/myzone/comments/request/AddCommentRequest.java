package grand.myzone.comments.request;

import com.google.gson.annotations.SerializedName;

public class AddCommentRequest {


    @SerializedName("advertisement_id")
    private int adsId;

    @SerializedName("content")
    private String content;


    public void setAdsId(int adsId) {
        this.adsId = adsId;
    }

    public int getAdsId() {
        return adsId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
