package grand.myzone.comments.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Comments{

	@SerializedName("data")
	private List<CommentItem> data;

	@SerializedName("meta")
	private Meta meta;

	@SerializedName("links")
	private Links links;

	public void setData(List<CommentItem> data){
		this.data = data;
	}

	public List<CommentItem> getData(){
		return data;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	public void setLinks(Links links){
		this.links = links;
	}

	public Links getLinks(){
		return links;
	}
}