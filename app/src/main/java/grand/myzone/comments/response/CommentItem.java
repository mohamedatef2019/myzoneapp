package grand.myzone.comments.response;

import com.google.gson.annotations.SerializedName;

import grand.myzone.addetails.response.User;

public class CommentItem {

	@SerializedName("id")
	private int id;

	@SerializedName("content")
	private String content;

	@SerializedName("user")
	private User user;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return content;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}