package grand.myzone.comments.response;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("comments")
	private Comments comments;

	@SerializedName("comments_count")
	private int commentsCount;

	public void setComments(Comments comments){
		this.comments = comments;
	}

	public Comments getComments(){
		return comments;
	}

	public void setCommentsCount(int commentsCount){
		this.commentsCount = commentsCount;
	}

	public int getCommentsCount(){
		return commentsCount;
	}
}