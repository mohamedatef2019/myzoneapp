package grand.myzone.terms;


import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.help.HelpResponse;
import grand.myzone.terms.response.TermsResponse;

public class TermViewModel extends BaseViewModel {

    public TermsResponse termResponse;

    public TermViewModel() {
        getTerms();
    }


    private void getTerms() {
        Log.e("INTERMS","INNNN");
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                Log.e("INTERMS","INNNN"+response);
                accessLoadingBar(View.GONE);
                termResponse = (TermsResponse) response;
                if(termResponse != null){
                    setReturnedMessage(termResponse.getMessage());
                    notifyChange();
                }else{
                    getClicksMutableLiveData().setValue(Constant.ERROR_NETWORK);
                }
            }

            @Override
            public void onRequestError(Object error) {
                Log.e("INTERMS","INNNN"+error);
                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.TERMS, null, TermsResponse.class);
    }

}
