package grand.myzone.terms;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import grand.myzone.R;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentTermsAndConditionBinding;
import grand.myzone.help.HelpViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermFragments extends BaseFragment {

    FragmentTermsAndConditionBinding binding;
    TermViewModel viewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms_and_condition, container, false);

        init();
        return binding.getRoot();
    }

    public void init() {

        viewModel = new TermViewModel();
        binding.setViewModel(viewModel);

        viewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }


        });
    }
}
