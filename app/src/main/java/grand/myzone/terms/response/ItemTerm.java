package grand.myzone.terms.response;

import com.google.gson.annotations.SerializedName;

public class ItemTerm {

	@SerializedName("image")
	private String image;

	@SerializedName("id")
	private int id;

	@SerializedName("value")
	private String value;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}
}