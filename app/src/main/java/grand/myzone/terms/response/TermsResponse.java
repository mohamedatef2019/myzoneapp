package grand.myzone.terms.response;

import com.google.gson.annotations.SerializedName;

public class TermsResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private ItemTerm data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(ItemTerm data){
		this.data = data;
	}

	public ItemTerm getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}