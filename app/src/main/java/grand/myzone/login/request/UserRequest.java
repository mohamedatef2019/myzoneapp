package grand.myzone.login.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class  UserRequest {

    @SerializedName("device_token")
    private String deviceToken;

    @SerializedName("phone")
    private String phone;

    @SerializedName("social_token")
    private String socialId;

    @SerializedName("firebase_token")
    private String token="ddd";


    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public String getPhone() {
        return phone;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialId() {
        return socialId;
    }
}
