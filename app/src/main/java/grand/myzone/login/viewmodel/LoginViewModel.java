package grand.myzone.login.viewmodel;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.utils.UserPreferenceHelper;
import grand.myzone.base.utils.Validate;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.login.request.UserRequest;
import grand.myzone.login.response.UserItem;
import grand.myzone.login.response.UserResponse;


public class LoginViewModel extends BaseViewModel {
    private UserRequest userRequest;
    public boolean isChecked=false;

    public LoginViewModel() {
        userRequest = new UserRequest();
    }

    @Bindable
    public UserRequest getUserDetails() {
        return userRequest;
    }


    public void loginClick2() {


        getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
//        isChecked=true;
//        userRequest.setPhone("01121805383");
//        if(Validate.isEmpty(userRequest.getPhone())){
//            setReturnedMessage(ResourcesManager.getString(R.string.label_enter_data));
//            getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
//        }else {
//            goLogin();
//        }
    }

    public void loginClick() {
        userRequest.setDeviceToken(UserPreferenceHelper.getGoogleToken());
        if(Validate.isEmpty(userRequest.getPhone())){
            setReturnedMessage(ResourcesManager.getString(R.string.label_enter_data));
            getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
        }else {
            goLogin();
        }
    }


    public void onTermsClick(){
        getClicksMutableLiveData().setValue(Codes.TERMS_CONDITION_SCREEN);
    }

    private void goLogin() {

        if(!isChecked){
            setReturnedMessage(ResourcesManager.getString(R.string.accept_terms));
                getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
        }else {
            userRequest.setDeviceToken(UserPreferenceHelper.getGoogleToken());
            if (validate()) {

                userRequest.setToken(UserPreferenceHelper.getGoogleToken());
                accessLoadingBar(View.VISIBLE);
                new ConnectionHelper(new ConnectionListener() {
                    @Override
                    public void onRequestSuccess(Object response) {
                        accessLoadingBar(View.GONE);
                        Log.e("ZWT", "tete");
                        UserResponse userResponse = (UserResponse) response;
                        if (userResponse.getCode() == WebServices.SUCCESS) {
                            Log.e("ZWT", "tete");
                            UserItem userItem = userResponse.getData();
                            Log.e("ZWT", "tete2" + userItem.getPhone());
                            UserPreferenceHelper.saveUserDetails(userItem);

                            getClicksMutableLiveData().setValue(WebServices.GO_ACTIVIATE);
                        } else if (userResponse.getCode() == WebServices.GO_ACTIVIATE) {
                            getClicksMutableLiveData().setValue(WebServices.GO_ACTIVIATE);
                        }
                    }
                }).requestJsonObject(Request.Method.POST, WebServices.LOGIN, userRequest, UserResponse.class);
            }
        }
    }




    private boolean validate() {

        return true;
    }

}
