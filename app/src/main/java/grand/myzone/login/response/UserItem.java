package grand.myzone.login.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserItem {

    @Expose
    @SerializedName("image")
    private String usersImg;

    @Expose
    @SerializedName("token")
    private String jwt;

    @Expose
    @SerializedName("firebase_token")
    private String firebaseToken;

    @Expose
    @SerializedName("name")
    private String usersName;

    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("id")
    private int usersId;

    @Expose
    @SerializedName("is_follow")
    private boolean isFollow;

    @Expose
    @SerializedName("email")
    private String email;


    @Expose
    private String password;

    public void setUsersImg(String usersImg) {
        this.usersImg = usersImg;
    }

    public String getUsersImg() {
        return usersImg;
    }



    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }


    public void setUsersName(String usersName) {
        this.usersName = usersName;
    }

    public String getUsersName() {
        return usersName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setId(int usersId) {
        this.usersId = usersId;
    }

    public int getId() {
        return usersId;
    }



    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }



    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    public int getUsersId() {
        return usersId;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setUsersId(int usersId) {
        this.usersId = usersId;
    }




}