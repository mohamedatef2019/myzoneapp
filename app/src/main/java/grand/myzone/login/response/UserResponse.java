package grand.myzone.login.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

 public class UserResponse {

 	@Expose
	@SerializedName("message")
	private String msg;

 	@Expose
	@SerializedName("data")
	private UserItem data;

 	@Expose
	@SerializedName("code")
	private int code;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(UserItem data){
		this.data = data;
	}

	public UserItem getData(){
		return data;
	}

	 public void setCode(int code) {
		 this.code = code;
	 }

	 public int getCode() {
		 return code;
	 }


}