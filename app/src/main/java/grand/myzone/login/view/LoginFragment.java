package grand.myzone.login.view;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Objects;

import org.json.JSONException;

import grand.myzone.R;
import grand.myzone.base.constantsutils.Codes;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.utils.MovementManager;
import grand.myzone.base.utils.ResourcesManager;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentLoginBinding;
import grand.myzone.home.HomeFragment;
import grand.myzone.login.viewmodel.LoginViewModel;
import grand.myzone.sendcode.view.SendCodeFragment;
import grand.myzone.terms.TermFragments;


public class LoginFragment extends BaseFragment {

    private View rootView;

    private LoginViewModel loginViewModel;
    private FragmentLoginBinding fragmentLoginBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);




        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.getStackTrace();
        }


        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentLoginBinding.getRoot();
        loginViewModel = new LoginViewModel();
        fragmentLoginBinding.setLoginViewModel(loginViewModel);
    }


    private void liveDataListeners() {
        loginViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }  else if (result == Codes.HOME_SCREEN) {
                requireActivity().finish();
                MovementManager.startMainActivity(getActivity(), HomeFragment.class.getName());

            }else if(result == WebServices.GO_ACTIVIATE) {
                Bundle bundle = new Bundle();
                bundle.putString(Params.BUNDLE_PHONE,loginViewModel.getUserDetails().getPhone());
                MovementManager.startActivity(getActivity(), SendCodeFragment.class.getName(),bundle, ResourcesManager.getString(R.string.label_enter_code));
            }else if(result == Codes.TERMS_CONDITION_SCREEN){
                MovementManager.startActivity(requireActivity(), TermFragments.class.getName(),ResourcesManager.getString(R.string.terms_conditions));
            }
            else if (result == Codes.SHOW_MESSAGE) {
                showMessage(loginViewModel.getReturnedMessage());
            }
        });





    }


}