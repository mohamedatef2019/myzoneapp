package grand.myzone.events.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventItem {

	@Expose
	@SerializedName("image")
	private String image;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	@SerializedName("tags")
	private Tags tags;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTags(Tags tags){
		this.tags = tags;
	}

	public Tags getTags(){
		return tags;
	}
}