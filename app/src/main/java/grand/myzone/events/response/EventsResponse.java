package grand.myzone.events.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class EventsResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<EventItem> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<EventItem> data){
		this.data = data;
	}

	public List<EventItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}