package grand.myzone.events.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValuesItem{

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("used_tag")
	private int usedTag;


	@Expose
	boolean isSelected=false;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public int getUsedTag() {
		return usedTag;
	}

	public void setUsedTag(int usedTag) {
		this.usedTag = usedTag;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	public boolean isSelected() {
		return isSelected;
	}
}