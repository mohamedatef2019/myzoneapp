package grand.myzone.events.viewmodel;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import grand.myzone.base.constantsutils.WebServices;
import grand.myzone.base.viewmodel.BaseViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.base.volleyutils.ConnectionListener;
import grand.myzone.events.response.EventsResponse;
import grand.myzone.events.view.EventsAdapter;
import grand.myzone.home.response.CategoriesItem;
import grand.myzone.subcategories.response.SubCategoriesResponse;


public class EventsViewModel extends BaseViewModel {

    private EventsAdapter eventsAdapter;
    private CategoriesItem categoriesItem;
    public EventsViewModel(CategoriesItem categoriesItem) {
        this.categoriesItem=categoriesItem;
        getServices();
    }


    @BindingAdapter({"app:events_adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, EventsAdapter eventsAdapter) {
        recyclerView.setAdapter(eventsAdapter);
    }



    @Bindable
    public EventsAdapter getEventsAdapter() {
        return this.eventsAdapter == null ? this.eventsAdapter = new EventsAdapter() : this.eventsAdapter;
    }

    private void getServices( ) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SubCategoriesResponse eventsResponse = (SubCategoriesResponse)response;
                getEventsAdapter().updateData(eventsResponse.getData().getSubCategoriesItems());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.SUB_CATEGORIES+categoriesItem.getId(), null, SubCategoriesResponse.class);
    }
}
