package grand.myzone.events.viewmodel;


import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import com.joooonho.SelectableRoundedImageView;
import com.makeramen.roundedimageview.RoundedImageView;

import grand.myzone.base.viewmodel.BaseItemViewModel;
import grand.myzone.base.volleyutils.ConnectionHelper;
import grand.myzone.events.view.EventsAdapter;
import grand.myzone.subcategories.response.SubCategoriesItem;


public class EventItemViewModel extends BaseItemViewModel {

    private SubCategoriesItem eventItem;



    public EventItemViewModel(SubCategoriesItem eventItem) {
        this.eventItem = eventItem;

    }

    @Bindable
    public SubCategoriesItem getEventItem() {
        return eventItem;
    }


    @BindingAdapter({"app:youtubeVideoUrl"})
    public static void setItemImage(RoundedImageView view, String imageItem){
        if(imageItem!=null && imageItem.length()>0) {


            ConnectionHelper.loadImage(view, "https://img.youtube.com/vi/" + EventsAdapter.getVideoCode(imageItem)+ "/mqdefault.jpg");
        }
    }
}
