package grand.myzone.events.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.myzone.R;
import grand.myzone.base.constantsutils.Params;
import grand.myzone.base.views.fragments.BaseFragment;
import grand.myzone.databinding.FragmentEventsBinding;
import grand.myzone.events.viewmodel.EventsViewModel;
import grand.myzone.home.response.CategoriesItem;

public class EventsFragment extends BaseFragment {
    private View rootView;
    private EventsViewModel notificationsViewModel;
    private FragmentEventsBinding fragmentEventsBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentEventsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_events, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentEventsBinding.getRoot();
        notificationsViewModel = new EventsViewModel((CategoriesItem) getArguments().getSerializable(Params.BUNDLE_CATEGORY));
        fragmentEventsBinding.setEventViewModel(notificationsViewModel);
    }


    private void liveDataListeners() {
        notificationsViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });
    }




}