
package grand.myzone.events.view;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.flipkart.youtubeview.activity.YouTubeActivity;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import grand.myzone.R;
import grand.myzone.base.utils.Constant;
import grand.myzone.base.views.activities.MainActivity;
import grand.myzone.databinding.ItemEventBinding;
import grand.myzone.events.response.EventItem;
import grand.myzone.events.viewmodel.EventItemViewModel;
import grand.myzone.subcategories.response.SubCategoriesItem;


public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.CategoriesViewHolder> {

    private List<SubCategoriesItem> notificationItems;


    public EventsAdapter() {
        this.notificationItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        SubCategoriesItem eventItem = notificationItems.get(position);


        EventItemViewModel orderItemViewModel = new EventItemViewModel(eventItem);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(orderItemViewModel);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(v.getContext(), YouTubeActivity.class);
//                intent.putExtra("apiKey", Constant.API_KEY);
//                intent.putExtra("videoId", getVideoCode(eventItem.getLink()));
//                intent.putExtra("webUrl",  eventItem.getLink());
//
//                v.getContext().startActivity(intent);

                Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) holder.itemView.getContext(), Constant.API_KEY, getVideoCode(eventItem.getLink()));
                v.getContext().startActivity(intent);
            }
        });
    }

    public static String getVideoCode(String url){

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url); //url is youtube url for which you want to extract the id.
        if (matcher.find()) {
            return matcher.group();
        }
        return  url;
    }

    @Override
    public int getItemCount() {
        return this.notificationItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<SubCategoriesItem> data) {

            this.notificationItems.clear();

            this.notificationItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemEventBinding itemEventBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemEventBinding == null) {
                itemEventBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemEventBinding != null) {
                itemEventBinding.unbind();
            }
        }

        void setViewModel(EventItemViewModel notificationItemViewModel) {
            if (itemEventBinding != null) {
                itemEventBinding.setEventItemViewModel(notificationItemViewModel);
            }
        }


    }



}
