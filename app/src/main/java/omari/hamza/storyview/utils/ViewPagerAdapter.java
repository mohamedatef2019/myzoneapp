package omari.hamza.storyview.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.danikula.videocache.HttpProxyCacheServer;

import java.util.ArrayList;


import grand.myzone.App;
import grand.myzone.R;
import grand.myzone.base.volleyutils.MyApplication;
import omari.hamza.storyview.callback.StoryCallbacks;
import omari.hamza.storyview.model.MyStory;

public class ViewPagerAdapter extends PagerAdapter {

    private ArrayList<MyStory> images;
    private Context context;
    private StoryCallbacks storyCallbacks;
    private boolean storiesStarted = false;

    public ViewPagerAdapter(ArrayList<MyStory> images, Context context, StoryCallbacks storyCallbacks) {
        this.images = images;
        this.context = context;
        this.storyCallbacks = storyCallbacks;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, final int position) {

        LayoutInflater inflater = LayoutInflater.from(context);

        MyStory currentStory = images.get(position);
          View view = inflater.inflate(R.layout.layout_story_item, collection, false);


        if(!currentStory.getUrl().contains("mp4")){
            final ImageView mImageView = view.findViewById(R.id.mImageView);
            View finalView = view;
            Glide.with(context)
                    .load(currentStory.getUrl())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            storyCallbacks.nextStory();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            if (resource != null) {
                                PaletteExtraction pe = new PaletteExtraction(finalView.findViewById(R.id.relativeLayout),
                                        ((BitmapDrawable) resource).getBitmap());
                                pe.execute();
                            }
                            if (!storiesStarted) {
                                storiesStarted = true;
                                storyCallbacks.startStories();
                            }
                            return false;
                        }
                    })
                    .into(mImageView);
        }else {
            view = inflater.inflate(R.layout.layout_story_video_item, collection, false);
            final VideoView mImageView = view.findViewById(R.id.mImageView);
            HttpProxyCacheServer proxy = MyApplication.getProxy(context);
            String proxyUrl = proxy.getProxyUrl(currentStory.getUrl());
            mImageView.setVideoPath(proxyUrl);




                final ViewTreeObserver vto = mImageView.getViewTreeObserver();
                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (isVisible(mImageView)) {
                            mImageView.start();
                        }else {
                            mImageView.pause();
                        }
                    }
                });
            }




        if (!TextUtils.isEmpty(currentStory.getDescription())) {
            TextView textView = view.findViewById(R.id.descriptionTextView);
            textView.setVisibility(View.VISIBLE);
            textView.setText(currentStory.getDescription());
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storyCallbacks.onDescriptionClickListener(position);
                }
            });
        }

        collection.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        (container).removeView((View) object);
    }

    public static boolean isVisible(final View view) {
        if (view == null) {
            return false;
        }
        if (!view.isShown()) {
            return false;
        }
        final Rect actualPosition = new Rect();
        view.getGlobalVisibleRect(actualPosition);
        final Rect screen = new Rect(0, 0, Resources.getSystem().getDisplayMetrics().widthPixels, Resources.getSystem().getDisplayMetrics().heightPixels);
        return actualPosition.intersect(screen);
    }

}
