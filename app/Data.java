public class Data{

	@SerializedName("area")
	private String area;

	@SerializedName("images")
	private List<ImagesItem> images;

	@SerializedName("is_favorite")
	private boolean isFavorite;

	@SerializedName("comments")
	private List<CommentsItem> comments;

	@SerializedName("city")
	private String city;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("title")
	private String title;

	@SerializedName("negotiable")
	private int negotiable;

	@SerializedName("government")
	private String government;

	@SerializedName("price")
	private int price;

	@SerializedName("comments_count")
	private int commentsCount;

	@SerializedName("id")
	private int id;

	@SerializedName("tag")
	private Object tag;

	@SerializedName("user")
	private User user;

	@SerializedName("longitude")
	private String longitude;

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setImages(List<ImagesItem> images){
		this.images = images;
	}

	public List<ImagesItem> getImages(){
		return images;
	}

	public void setIsFavorite(boolean isFavorite){
		this.isFavorite = isFavorite;
	}

	public boolean isIsFavorite(){
		return isFavorite;
	}

	public void setComments(List<CommentsItem> comments){
		this.comments = comments;
	}

	public List<CommentsItem> getComments(){
		return comments;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setNegotiable(int negotiable){
		this.negotiable = negotiable;
	}

	public int getNegotiable(){
		return negotiable;
	}

	public void setGovernment(String government){
		this.government = government;
	}

	public String getGovernment(){
		return government;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setCommentsCount(int commentsCount){
		this.commentsCount = commentsCount;
	}

	public int getCommentsCount(){
		return commentsCount;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTag(Object tag){
		this.tag = tag;
	}

	public Object getTag(){
		return tag;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}
}
